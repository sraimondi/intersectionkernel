#ifndef INTERSECTIONKERNEL_TEXTURE_HPP
#define INTERSECTIONKERNEL_TEXTURE_HPP

#include "image.hpp"
#include "intersection.hpp"

// Base texture interface
template <typename T>
class TextureInterface {
public:
    // Member types
    using value_type = T;

    // Evaluate texture at given coordinates
    virtual value_type Evaluate(const Intersection& isect) const CXX_NOEXCEPT = 0;
};

// Template Texture2D class, inherits from Image and add interpolation
template <typename T>
class Texture2D : public Image<T>, public TextureInterface<T> {
public:
    using value_type = typename Image<T>::value_type;
    using size_type = typename Image<T>::size_type;
    using reference = typename Image<T>::reference;
    using const_reference = typename Image<T>::const_reference;

    // Create new Texture2D of given size
    Texture2D(size_type w, size_type h, const value_type& def_value = value_type());

    // Move constructors
    Texture2D(const Texture2D& other);

    Texture2D(Texture2D&& other) CXX_NOEXCEPT;

    Texture2D& operator=(const Texture2D& rhs);

    Texture2D& operator=(Texture2D&& rhs) CXX_NOEXCEPT;

    value_type Evaluate(const Intersection& isect) const CXX_NOEXCEPT override;
};

template <typename T>
Texture2D<T>::Texture2D(Texture2D::size_type w, Texture2D::size_type h, const value_type& def_value)
        : Image<T>(w, h, def_value) {}

template <typename T>
Texture2D<T>::Texture2D(const Texture2D& other)
        : Image<T>(other) {}

template <typename T>
Texture2D<T>::Texture2D(Texture2D&& other) CXX_NOEXCEPT
        : Image<T>(std::move(other)) {}

template <typename T>
Texture2D<T>& Texture2D<T>::operator=(const Texture2D& rhs) {
    Image<T>::operator=(rhs);
    return *this;
}

template <typename T>
Texture2D<T>& Texture2D<T>::operator=(Texture2D&& rhs) CXX_NOEXCEPT {
    Image<T>::operator=(rhs);
    return *this;
}

template <typename T>
typename Texture2D<T>::value_type Texture2D<T>::Evaluate(const Intersection& isect) const CXX_NOEXCEPT {
    // Compute pixel size
    const Vector2<Real> pixel_size(1 / Image<T>::GetWidth(), 1 / Image<T>::GetHeight());

    // Map uv coordinates to image space
    const Vector2<Real> uv_t_flip(Clamp(isect.uv.s(), Real(0), Real(1)),
                                  Clamp(Real(1) - isect.uv.t(), Real(0), Real(1)));
    const Vector2<Real> local_uv = (uv_t_flip - 0.5 * pixel_size) /
                                   (Vector2<Real>(Image<T>::GetWidth() - 1, Image<T>::GetHeight() - 1) * pixel_size);

    // Get coordinate of the top left pixel for the interpolation
    const size_type top_left_row = Clamp(
            static_cast<size_type>((Image<T>::GetHeight() - 1) * Clamp(local_uv.t(), Real(0), Real(1))), size_type(0),
            Image<T>::GetHeight() - 1);
    const size_type top_left_col = Clamp(
            static_cast<size_type>((Image<T>::GetWidth() - 1) * Clamp(local_uv.s(), Real(0), Real(1))), size_type(0),
            Image<T>::GetWidth() - 1);

    // Check if we are on the border of the texture
    if (local_uv.s() <= 0 || local_uv.s() >= 1 || local_uv.t() <= 0 || local_uv.t() >= 1) {
        return Image<T>::operator()(top_left_row, top_left_col);
    } else {
        // Compute interpolation value along column and row
        const Real t_col = (local_uv.t() - top_left_row * pixel_size.t());
        const Real t_row = (local_uv.s() - top_left_col * pixel_size.s());
        const value_type e0 = Lerp(t_col, Image<T>::operator()(top_left_row, top_left_col),
                                   Image<T>::operator()(top_left_row + 1, top_left_col));
        const value_type e1 = Lerp(t_col, Image<T>::operator()(top_left_row, top_left_col + 1),
                                   Image<T>::operator()(top_left_row + 1, top_left_col + 1));

        return Lerp(t_row, e0, e1);
    }
}

#endif //INTERSECTIONKERNEL_TEXTURE_HPP
