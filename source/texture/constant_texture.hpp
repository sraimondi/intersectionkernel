#ifndef INTERSECTIONKERNEL_CONSTANT_TEXTURE_HPP
#define INTERSECTIONKERNEL_CONSTANT_TEXTURE_HPP

#include "texture.hpp"

// Constant texture class
template <typename T>
class ConstantTexture : public TextureInterface<T> {
public:
    // Memeber types
    using value_type = typename TextureInterface<T>::value_type;

    CXX_CONSTEXPR explicit ConstantTexture(const value_type& value) CXX_NOEXCEPT
            : texture_value(value) {}

    value_type Evaluate(const Intersection&) const CXX_NOEXCEPT override {
        return texture_value;
    }

private:
    // Constant value of the texture
    const value_type texture_value;
};

#endif //INTERSECTIONKERNEL_CONSTANT_TEXTURE_HPP
