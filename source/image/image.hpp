#ifndef INTERSECTION_KERNEL_IMAGE_HPP
#define INTERSECTION_KERNEL_IMAGE_HPP

#include "lin_algebra.hpp"

#include <vector>
#include <stdexcept>
#include <functional>

// Template Image class, storage is column-major
template <typename T>
class Image {
public:
    using value_type = T;
    using size_type = std::size_t;
    using reference = value_type&;
    using const_reference = const value_type&;
    using const_pointer = const value_type*;

    // Create empty image
    Image();

    // Generate default constructors and operators
    Image(const Image& other);

    Image(Image&& other) CXX_NOEXCEPT;

    Image& operator=(const Image& rhs);

    Image& operator=(Image&& rhs) CXX_NOEXCEPT;

    // Create image with given size and possible default value
    Image(size_type w, size_type h, const value_type& def_value = value_type());

    // Resize image, deletes the content
    void Resize(size_type new_w, size_type new_h, const value_type& def_value = value_type());

    CXX_INLINE size_type GetWidth() const CXX_NOEXCEPT {
        return width;
    }

    CXX_INLINE size_type GetHeight() const CXX_NOEXCEPT {
        return height;
    }

    // Access element with range check
#if CXX_VERSION >= CXX_14

    decltype(auto)
#else

    typename std::conditional<std::is_arithmetic<value_type>::value, value_type, const_reference>::type
#endif
    At(size_type row, size_type col) const;

    reference At(size_type row, size_type col);

    // Access element operator
#if CXX_VERSION >= CXX_14

    decltype(auto)
#else

    typename std::conditional<std::is_arithmetic<value_type>::value, value_type, const_reference>::type
#endif
    operator()(size_type row, size_type col) const CXX_NOEXCEPT;

    reference operator()(size_type row, size_type col) CXX_NOEXCEPT;

    // Convert image to another one of different type
    template <typename NewT, typename ConversionFunction>
    Image<NewT>
    Convert(ConversionFunction&& conversion_function = [](const value_type& e) -> NewT {
        return static_cast<NewT>(e);
    }) const;

    // Access pointer to data
    const_pointer Data() const CXX_NOEXCEPT;

    // Apply reduction function to image elements in the given region with the offset
    template <typename ReduceFunction>
    value_type Reduce(const Vector2<size_type>& offset, const Vector2<size_type>& size, value_type init,
                      ReduceFunction&& function) const;

    // Apply reduction function on whole image
    template <typename ReduceFunction>
    value_type Reduce(value_type init, ReduceFunction&& function) const;

protected:
    // Column-wise content of the image
    std::vector<value_type> content;
    // Size of the image
    size_type height, width;

    // Small inline utility to convert to linear index
    CXX_INLINE size_type LinearIndex(size_type row, size_type col) const CXX_NOEXCEPT {
        return row + col * height;
    }

    // Check indices
    CXX_INLINE void CheckIndices(size_type row, size_type col) const {
        if (row >= height || col >= width) {
            throw std::out_of_range("INVALID INDEX in Image::At(row, col)");
        }
    }
};

template <typename T>
Image<T>::Image()
        : content(), height(0), width(0) {}

template <typename T>
Image<T>::Image(const Image& other)
        : content(other.content), height(other.height), width(other.width) {}

template <typename T>
Image<T>::Image(Image&& other) CXX_NOEXCEPT
        : content(std::move(other.content)), height(std::move(other.height)), width(std::move(other.width)) {}

template <typename T>
Image<T>& Image<T>::operator=(const Image& rhs) {
    if (this != &rhs) {
        content = rhs.content;
        height = rhs.height;
        width = rhs.width;
    }
    return *this;
}

template <typename T>
Image<T>& Image<T>::operator=(Image&& rhs) CXX_NOEXCEPT {
    if (this != &rhs) {
        content = std::move(rhs.content);
        height = std::move(rhs.height);
        width = std::move(rhs.width);
    }
    return *this;
}

template <typename T>
Image<T>::Image(Image::size_type w, Image::size_type h, const value_type& def_value)
        : content(w * h, def_value), height(h), width(w) {}

template <typename T>
void Image<T>::Resize(Image::size_type new_w, Image::size_type new_h, const value_type& def_value) {
    height = new_h;
    width = new_w;
    content.resize(height * width, def_value);
}

template <typename T>
#if CXX_VERSION >= CXX_14
decltype(auto)
#else
typename std::conditional<
        std::is_arithmetic<typename Image<T>::value_type>::value,
        typename Image<T>::value_type, typename Image<T>::const_reference>::type
#endif
Image<T>::At(Image::size_type row, Image::size_type col) const {
    CheckIndices(row, col);
    return content[LinearIndex(row, col)];
}

template <typename T>
typename Image<T>::reference Image<T>::At(Image::size_type row, Image::size_type col) {
    CheckIndices(row, col);
    return content[LinearIndex(row, col)];
}

template <typename T>
#if CXX_VERSION >= CXX_14
decltype(auto)
#else
typename std::conditional<
        std::is_arithmetic<typename Image<T>::value_type>::value,
        typename Image<T>::value_type, typename Image<T>::const_reference>::type
#endif
Image<T>::operator()(Image::size_type row, Image::size_type col) const CXX_NOEXCEPT {
    return content[LinearIndex(row, col)];
}

template <typename T>
typename Image<T>::reference Image<T>::operator()(Image::size_type row, Image::size_type col) CXX_NOEXCEPT {
    return content[LinearIndex(row, col)];
}

template <typename T>
template <typename NewT, typename ConversionFunction>
Image<NewT> Image<T>::Convert(ConversionFunction&& conversion_function) const {
    Image<NewT> converted_image(GetWidth(), GetHeight());
    // Loop over image and convert using the given function
    for (size_type col = 0; col < GetWidth(); ++col) {
        for (size_type row = 0; row < GetWidth(); ++row) {
            converted_image(row, col) = conversion_function(this->operator()(row, col));
        }
    }

    return converted_image;
}

template <typename T>
typename Image<T>::const_pointer Image<T>::Data() const CXX_NOEXCEPT {
    return content.data();
}

template <typename T>
template <typename ReduceFunction>
typename Image<T>::value_type
Image<T>::Reduce(const Vector2<Image::size_type>& offset, const Vector2<Image::size_type>& size,
                 value_type init, ReduceFunction&& function) const {
    for (size_type col = offset.s(); col < std::min(offset.s() + size.s(), width); ++col) {
        for (size_type row = offset.t(); row < std::min(offset.t() + size.t(), height); ++row) {
            init = function(init, this->operator()(row, col));
        }
    }

    return init;
}

template <typename T>
template <typename ReduceFunction>
T Image<T>::Reduce(value_type init, ReduceFunction&& function) const {
    return Reduce(Vector2<size_type>(0, 0), Vector2<size_type>(width, height), init,
                  std::forward<ReduceFunction>(function));
}

#endif //INTERSECTION_KERNEL_IMAGE_HPP
