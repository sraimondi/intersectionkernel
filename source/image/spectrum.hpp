#ifndef INTERSECTIONKERNEL_SPECTRUM_HPP
#define INTERSECTIONKERNEL_SPECTRUM_HPP

#include "common.hpp"

#include <array>
#include <cassert>

// Spectrum class
template <typename T>
class Spectrum {
public:
    // Member types
    using value_type = T;
    using size_type = int;
    using reference = value_type&;
    using const_reference = const value_type&;

    // Constructors
    CXX_CONSTEXPR Spectrum() CXX_NOEXCEPT
            : Spectrum(0) {}

    CXX_CONSTEXPR explicit Spectrum(value_type v) CXX_NOEXCEPT
            : Spectrum(v, v, v) {}

    CXX_CONSTEXPR Spectrum(value_type r, value_type g, value_type b) CXX_NOEXCEPT
            : elements({r, g, b}) {}

    CXX14_CONSTEXPR value_type operator[](size_type i) const CXX_NOEXCEPT {
        assert(i >= 0 && i < 3);
        return elements[i];
    }

    CXX17_CONSTEXPR reference operator[](size_type i) CXX_NOEXCEPT {
        assert(i >= 0 && i < 3);
        return elements[i];
    }

    CXX_CONSTEXPR value_type r() const CXX_NOEXCEPT {
        return elements[0];
    }

    CXX_CONSTEXPR value_type g() const CXX_NOEXCEPT {
        return elements[1];
    }

    CXX_CONSTEXPR value_type b() const CXX_NOEXCEPT {
        return elements[2];
    }

    CXX17_CONSTEXPR reference r() CXX_NOEXCEPT {
        return elements[0];
    }

    CXX17_CONSTEXPR reference g() CXX_NOEXCEPT {
        return elements[1];
    }

    CXX17_CONSTEXPR reference b() CXX_NOEXCEPT {
        return elements[2];
    }

    // Math operator
    CXX_CONSTEXPR Spectrum operator+(const Spectrum& s) const CXX_NOEXCEPT {
        return {r() + s.r(), g() + s.g(), b() + s.b()};
    }

    CXX14_CONSTEXPR Spectrum& operator+=(const Spectrum& s) CXX_NOEXCEPT {
        r() += s.r();
        g() += s.g();
        b() += s.b();
        return *this;
    }

    CXX_CONSTEXPR Spectrum operator-() const CXX_NOEXCEPT {
        return {-r(), -g(), -b()};
    }

    CXX_CONSTEXPR Spectrum operator-(const Spectrum& s) const CXX_NOEXCEPT {
        return {r() - s.r(), g() - s.g(), b() - s.b()};
    }

    CXX14_CONSTEXPR Spectrum& operator-=(const Spectrum& s) CXX_NOEXCEPT {
        r() -= s.r();
        g() -= s.g();
        b() -= s.b();
        return *this;
    }

    CXX_CONSTEXPR Spectrum operator*(const Spectrum& s) const CXX_NOEXCEPT {
        return {r() * s.r(), g() * s.g(), b() * s.b()};
    }

    CXX_CONSTEXPR Spectrum operator*(value_type t) const CXX_NOEXCEPT {
        return {t * r(), t * g(), t * b()};
    }

    CXX14_CONSTEXPR Spectrum& operator*=(value_type t) CXX_NOEXCEPT {
        r() *= t;
        g() *= t;
        b() *= t;
        return *this;
    }

    CXX14_CONSTEXPR Spectrum& operator*=(const Spectrum& s) CXX_NOEXCEPT {
        r() *= s.r();
        g() *= s.g();
        b() *= s.b();
        return *this;
    }

    CXX_CONSTEXPR Spectrum operator/(const Spectrum& s) const CXX_NOEXCEPT {
        return {r() / s.r(), g() / s.g(), b() / s.b()};
    }

    CXX14_CONSTEXPR Spectrum operator/(value_type t) const CXX_NOEXCEPT {
        const value_type inv_t = value_type(1) / t;
        return *this * inv_t;
    }

    CXX14_CONSTEXPR Spectrum& operator/=(value_type t) CXX_NOEXCEPT {
        const value_type inv_t = value_type(1) / t;
        return (*this *= inv_t);
    }

    // Check if color is black
    CXX_INLINE bool IsBlack() const CXX_NOEXCEPT {
        return ApproxEqual(r(), value_type(0)) &&
               ApproxEqual(g(), value_type(0)) &&
               ApproxEqual(b(), value_type(0));
    }

private:
    // Elements
    std::array<value_type, 3> elements;
};

template <typename T>
CXX_CONSTEXPR Spectrum<T> operator*(typename Spectrum<T>::value_type t, const Spectrum<T>& s) CXX_NOEXCEPT {
    return {t * s.r(), t * s.g(), t * s.b()};
}

// Clamp color
template <typename T>
CXX_CONSTEXPR Spectrum<T>
Clamp(const Spectrum<T>& s, typename Spectrum<T>::value_type min, typename Spectrum<T>::value_type max) CXX_NOEXCEPT {
    return {Clamp(s.r(), min, max), Clamp(s.g(), min, max), Clamp(s.b(), min, max)};
}

// Define common types
using Spectrumf = Spectrum<float>;
using Spectrumd = Spectrum<double>;

#endif //INTERSECTIONKERNEL_SPECTRUM_HPP
