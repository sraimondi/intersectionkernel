#ifndef INTERSECTIONKERNEL_IMAGE_WRITER_HPP
#define INTERSECTIONKERNEL_IMAGE_WRITER_HPP

#include "image.hpp"
#include "spectrum.hpp"

// Generate PNG from image made of Vector3
void GeneratePNG(const std::string& file_name, const Image<Spectrum<Real>>& image);

// Gray-scale image
void GeneratePNG(const std::string& file_name, const Image<Real>& image);

#endif //INTERSECTIONKERNEL_IMAGE_WRITER_HPP
