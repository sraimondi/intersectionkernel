#include "image_writer.hpp"
#include "common.hpp"

#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "stb_image_write.hpp"

#include <iostream>

void GeneratePNG(const std::string& file_name, const Image<Spectrum<Real>>& image) {
    // Raster converted to bytes
    std::vector<unsigned char> raster(image.GetHeight() * image.GetWidth() * 3);
    for (Image<Spectrum<Real>>::size_type col = 0; col < image.GetWidth(); ++col) {
        for (Image<Spectrum<Real>>::size_type row = 0; row < image.GetHeight(); ++row) {
            const auto c = Clamp(image(row, col), Real(0), Real(1));
            // The storage for the function to write is row major, we need to swap the indices
            const Image<Spectrum<Real>>::size_type linear_index_row_major = row * image.GetWidth() + col;
            raster[3 * linear_index_row_major] = static_cast<unsigned char>(255 * c.r());
            raster[3 * linear_index_row_major + 1] = static_cast<unsigned char>(255 * c.g());
            raster[3 * linear_index_row_major + 2] = static_cast<unsigned char>(255 * c.b());
        }
    }
    // Write image
    if (!stbi_write_png(file_name.c_str(), image.GetWidth(), image.GetHeight(), 3, raster.data(), 0)) {
        std::cerr << "Error during writing of image " << file_name << "\n";
    }
}

void GeneratePNG(const std::string& file_name, const Image<Real>& image) {
    // Raster converted to bytes
    std::vector<unsigned char> raster(image.GetHeight() * image.GetWidth());
    for (Image<Spectrum<Real>>::size_type col = 0; col < image.GetWidth(); ++col) {
        for (Image<Spectrum<Real>>::size_type row = 0; row < image.GetHeight(); ++row) {
            const auto& c = image(row, col);
            // The storage for the function to write is row major, we need to swap the indices
            const Image<Spectrum<Real>>::size_type linear_index_row_major = row * image.GetWidth() + col;
            raster[linear_index_row_major] = static_cast<unsigned char>(255 * Clamp(c, Real(0), Real(1)));
        }
    }
    // Write image
    if (!stbi_write_png(file_name.c_str(), image.GetWidth(), image.GetHeight(), 1, raster.data(), 0)) {
        std::cerr << "Error during writing of image " << file_name << "\n";
    }
}
