#include "triangle.hpp"
#include "mesh.hpp"
#include "montecarlo.hpp"

Triangle::Triangle(const int* index, const Mesh& m) CXX_NOEXCEPT
        : triangle_index(index), mesh(m) {}

bool Triangle::Intersect(const Ray& ray, Intersection& isect) const CXX_NOEXCEPT {
    // Get reference to the three vertices
    const auto& v0 = mesh.vertices[triangle_index[0]];
    const auto& v1 = mesh.vertices[triangle_index[1]];
    const auto& v2 = mesh.vertices[triangle_index[2]];

    // Translate vertices based on ray origin
    auto v0t = v0 - ray.Origin();
    auto v1t = v1 - ray.Origin();
    auto v2t = v2 - ray.Origin();

    // Permute components of triangle vertices and ray direction
    const auto kz = static_cast<Vector3<Real>::size_type>(MaxDimension(Abs(ray.Direction())));
    auto kx = kz + 1;
    if (kx == 3) { kx = 0; }
    auto ky = kx + 1;
    if (ky == 3) { ky = 0; }
    const auto d = Permute(ray.Direction(), kx, ky, kz);
    v0t = Permute(v0t, kx, ky, kz);
    v1t = Permute(v1t, kx, ky, kz);
    v2t = Permute(v2t, kx, ky, kz);

    // Apply shear transformation to translated vertex positions
    const Real sx = -d.x() / d.z();
    const Real sy = -d.y() / d.z();
    const Real sz = 1 / d.z();
    v0t.x() += sx * v0t.z();
    v0t.y() += sy * v0t.z();
    v1t.x() += sx * v1t.z();
    v1t.y() += sy * v1t.z();
    v2t.x() += sx * v2t.z();
    v2t.y() += sy * v2t.z();

    // Compute edge function coefficients e0, e1 and e2
    const Real e0 = v1t.x() * v2t.y() - v1t.y() * v2t.x();
    const Real e1 = v2t.x() * v0t.y() - v2t.y() * v0t.x();
    const Real e2 = v0t.x() * v1t.y() - v0t.y() * v1t.x();

    // Perform triangle edge and determinant tests
    if ((e0 < 0 || e1 < 0 || e2 < 0) && (e0 > 0 || e1 > 0 || e2 > 0)) { return false; }
    const Real det = e0 + e1 + e2;
    if (ApproxEqual(det, Real(0))) { return false; }

    // Compute scaled hit distance to triangle and test against ray range
    v0t.z() *= sz;
    v1t.z() *= sz;
    v2t.z() *= sz;
    const Real t_scaled = e0 * v0t.z() + e1 * v1t.z() + e2 * v2t.z();
    if (det < 0 && (t_scaled >= 0 || t_scaled < isect.t * det)) { return false; }
    else if (det > 0 && (t_scaled <= 0 || t_scaled > isect.t * det)) { return false; }

    // Compute barycentric coordinates and t value for triangle intersection
    const Real inv_det = 1 / det;
    const Real b0 = e0 * inv_det;
    const Real b1 = e1 * inv_det;
    const Real b2 = e2 * inv_det;
    const Real t = t_scaled * inv_det;

    // Compute intersection point
    isect.hit_p = b0 * v0 + b1 * v1 + b2 * v2;

    // If we don't have shading normals use the geometric one
    if (mesh.normals.empty()) {
        isect.geometry_frame = Frame(Normalize(Cross(v1 - v0, v2 - v0)));
    } else {
        isect.geometry_frame = Frame(Normalize(b0 * mesh.normals[triangle_index[0]] +
                                               b1 * mesh.normals[triangle_index[1]] +
                                               b2 * mesh.normals[triangle_index[2]]));
    }

    // If UVs are not empty, interpolate them, otherwise leave it to zero
    if (!mesh.uvs.empty()) {
        isect.uv = b0 * mesh.uvs[triangle_index[0]] +
                   b1 * mesh.uvs[triangle_index[1]] +
                   b2 * mesh.uvs[triangle_index[2]];
    } else {
        isect.uv = Vector2<Real>(0);
    }

    // Store outgoing direction
    isect.wo = -Normalize(ray.Direction());

    // Set the value in the intersection parameter + pointer to triangle
    isect.t = t;

    return true;
}

bool Triangle::IntersectP(const Ray& ray, Real max_t) const CXX_NOEXCEPT {
    // Get reference to the three vertices
    const auto& v0 = mesh.vertices[triangle_index[0]];
    const auto& v1 = mesh.vertices[triangle_index[1]];
    const auto& v2 = mesh.vertices[triangle_index[2]];

    // Translate vertices based on ray origin
    auto v0t = v0 - ray.Origin();
    auto v1t = v1 - ray.Origin();
    auto v2t = v2 - ray.Origin();

    // Permute components of triangle vertices and ray direction
    const auto kz = static_cast<Vector3<Real>::size_type>(MaxDimension(Abs(ray.Direction())));
    auto kx = kz + 1;
    if (kx == 3) { kx = 0; }
    auto ky = kx + 1;
    if (ky == 3) { ky = 0; }
    const auto d = Permute(ray.Direction(), kx, ky, kz);
    v0t = Permute(v0t, kx, ky, kz);
    v1t = Permute(v1t, kx, ky, kz);
    v2t = Permute(v2t, kx, ky, kz);

    // Apply shear transformation to translated vertex positions
    const Real sx = -d.x() / d.z();
    const Real sy = -d.y() / d.z();
    const Real sz = 1 / d.z();
    v0t.x() += sx * v0t.z();
    v0t.y() += sy * v0t.z();
    v1t.x() += sx * v1t.z();
    v1t.y() += sy * v1t.z();
    v2t.x() += sx * v2t.z();
    v2t.y() += sy * v2t.z();

    // Compute edge function coefficients e0, e1 and e2
    const Real e0 = v1t.x() * v2t.y() - v1t.y() * v2t.x();
    const Real e1 = v2t.x() * v0t.y() - v2t.y() * v0t.x();
    const Real e2 = v0t.x() * v1t.y() - v0t.y() * v1t.x();

    // Perform triangle edge and determinant tests
    if ((e0 < 0 || e1 < 0 || e2 < 0) && (e0 > 0 || e1 > 0 || e2 > 0)) { return false; }
    const Real det = e0 + e1 + e2;
    if (ApproxEqual(det, Real(0))) { return false; }

    // Compute scaled hit distance to triangle and test against ray range
    v0t.z() *= sz;
    v1t.z() *= sz;
    v2t.z() *= sz;
    const Real t_scaled = e0 * v0t.z() + e1 * v1t.z() + e2 * v2t.z();
    if (det < 0 && (t_scaled >= 0 || t_scaled < max_t * det)) { return false; }
    else if (det > 0 && (t_scaled <= 0 || t_scaled > max_t * det)) { return false; }

    return true;
}

Real Triangle::Area() const CXX_NOEXCEPT {
    // Get reference to the three vertices
    const auto& v0 = mesh.vertices[triangle_index[0]];
    const auto& v1 = mesh.vertices[triangle_index[1]];
    const auto& v2 = mesh.vertices[triangle_index[2]];

    return Real(0.5) * Length(Cross(v1 - v0, v2 - v0));
}

Bounds Triangle::LocalBounds() const CXX_NOEXCEPT {
    // Get reference to the three vertices
    const auto& v0 = mesh.vertices[triangle_index[0]];
    const auto& v1 = mesh.vertices[triangle_index[1]];
    const auto& v2 = mesh.vertices[triangle_index[2]];

    Bounds b(v0, v1);
    b.Extend(v2);

    return b;
}

Bounds Triangle::WorldBounds(const Matrix4x4<Real>& l2w) const CXX_NOEXCEPT {
    // Get reference to the three vertices
    const auto& v0 = mesh.vertices[triangle_index[0]];
    const auto& v1 = mesh.vertices[triangle_index[1]];
    const auto& v2 = mesh.vertices[triangle_index[2]];

    Bounds b(l2w.TransformPosition(v0), l2w.TransformPosition(v1));
    b.Extend(l2w.TransformPosition(v2));

    return b;
}

Intersection Triangle::Sample(const Vector2<Real>& u) const CXX_NOEXCEPT {
    Intersection sample_intersection;
    // Sample barycentric coordinate uniformly
    const Vector2<Real> b = UniformSampleTriangle(u);
    const Real b2 = 1 - b[0] - b[1];

    // Get reference to the three vertices
    const auto& v0 = mesh.vertices[triangle_index[0]];
    const auto& v1 = mesh.vertices[triangle_index[1]];
    const auto& v2 = mesh.vertices[triangle_index[2]];

    // Sample point
    sample_intersection.hit_p = b[0] * v0 + b[1] * v1 + b2 * v2;

    // Compute normal
    if (mesh.normals.empty()) {
        sample_intersection.geometry_frame = Frame(Normalize(Cross(v1 - v0, v2 - v0)));
    } else {
        sample_intersection.geometry_frame = Frame(Normalize(b[0] * mesh.normals[triangle_index[0]] +
                                                             b[1] * mesh.normals[triangle_index[1]] +
                                                             b2 * mesh.normals[triangle_index[2]]));
    }

    // If UVs are not empty, interpolate them, otherwise leave it to zero
    if (!mesh.uvs.empty()) {
        sample_intersection.uv = b[0] * mesh.uvs[triangle_index[0]] +
                                 b[1] * mesh.uvs[triangle_index[1]] +
                                 b2 * mesh.uvs[triangle_index[2]];
    } else {
        sample_intersection.uv = Vector2<Real>(0);
    }

    return sample_intersection;
}


