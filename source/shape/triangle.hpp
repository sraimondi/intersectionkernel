#ifndef INTERSECTIONKERNEL_TRIANGLE_HPP
#define INTERSECTIONKERNEL_TRIANGLE_HPP

#include "shape.hpp"

// Forward declare Mesh class
class Mesh;

// Triangle class
class Triangle : public ShapeInterface {
public:
    Triangle(const int* index, const Mesh& m) CXX_NOEXCEPT;

    bool Intersect(const Ray& ray, Intersection& isect) const CXX_NOEXCEPT override;

    bool IntersectP(const Ray& ray, Real max_t) const CXX_NOEXCEPT override;

    Real Area() const CXX_NOEXCEPT override;

    Bounds LocalBounds() const CXX_NOEXCEPT override;

    Bounds WorldBounds(const Matrix4x4<Real>& l2w) const CXX_NOEXCEPT override;

    Intersection Sample(const Vector2<Real>& u) const CXX_NOEXCEPT override;

private:
    // Index of the starting indices in the mesh array for this triangle
    const int* triangle_index;
    // Reference to the mesh that holds the data of this triangle
    const Mesh& mesh;
};

#endif //INTERSECTIONKERNEL_TRIANGLE_HPP
