#include "sphere.hpp"
#include "montecarlo.hpp"

bool Sphere::Intersect(const Ray& ray, Intersection& isect) const CXX_NOEXCEPT {
    // Compute quadratic terms
    const Real a = LengthSquared(ray.Direction());
    const Real b = 2 * Dot(ray.Direction(), ray.Origin());
    const Real c = LengthSquared(ray.Origin()) - radius * radius;

    // Solve quadratic equation
    Real t0, t1;
    if (!SolveQuadratic(a, b, c, t0, t1)) {
        return false;
    }

    // Check if we have a hit
    if (t0 > isect.t || t1 <= 0) {
        return false;
    }
    Real t_hit = t0;
    if (t_hit <= 0) {
        t_hit = t1;
        if (t_hit > isect.t) {
            return false;
        }
    }

    // Set intersection parameters
    isect.t = t_hit;
    isect.wo = -Normalize(ray.Direction());

    // Compute hit point
    isect.hit_p = ray(isect.t);

    // Check if we are at top or bottom of the sphere
    Real theta, phi;
    if (ApproxEqual(isect.hit_p.x(), Real(0)) && ApproxEqual(isect.hit_p.z(), Real(0))) {
        phi = 0;
        if (isect.hit_p.y() > 0) {
            theta = 0;
        } else {
            theta = PI;
        }
    } else {
        phi = std::atan2(isect.hit_p.z(), isect.hit_p.x());
        if (phi < 0) {
            phi += TWO_PI;
        }
        theta = std::acos(isect.hit_p.y() / radius);
    }

    // Compute UV coordinates
    isect.uv = Vector2<Real>(phi / TWO_PI, theta / PI);

    // Compute local geometry frame
    const Real sin_theta = std::sin(theta);
    const Real cos_theta = std::cos(theta);
    isect.geometry_frame = Frame(Normalize(Vector3<Real>(std::sin(phi) * sin_theta, 0, -std::cos(phi) * sin_theta)),
                                 Normalize(isect.hit_p),
                                 Normalize(Vector3<Real>(cos_theta * std::cos(phi), -sin_theta,
                                                         cos_theta * std::sin(phi))));

    return true;
}

bool Sphere::IntersectP(const Ray& ray, Real max_t) const CXX_NOEXCEPT {
    // Compute quadratic terms
    const Real a = LengthSquared(ray.Direction());
    const Real b = 2 * Dot(ray.Direction(), ray.Origin());
    const Real c = LengthSquared(ray.Origin()) - radius * radius;

    // Solve quadratic equation
    Real t0, t1;
    if (!SolveQuadratic(a, b, c, t0, t1)) {
        return false;
    }

    // Check if we have a hit
    if (t0 > max_t || t1 <= 0) {
        return false;
    }
    if (t0 <= 0) {
        if (t1 > max_t) {
            return false;
        }
    }

    return true;
}

Real Sphere::Area() const CXX_NOEXCEPT {
    return 4 * PI * radius * radius;
}

Bounds Sphere::LocalBounds() const CXX_NOEXCEPT {
    return {Vector3<Real>(-radius), Vector3<Real>(radius)};
}

Intersection Sphere::Sample(const Vector2<Real>& u) const CXX_NOEXCEPT {
    Intersection sample_intersection;

    // Sample normal
    sample_intersection.geometry_frame = Frame(UniformSampleSphere(u));
    // Intersection point is normal scaled by radius in local space
    sample_intersection.hit_p = radius * sample_intersection.geometry_frame.n();

    // Compute uv coordinates of sampled point
    Real theta, phi;
    if (ApproxEqual(sample_intersection.hit_p.x(), Real(0)) && ApproxEqual(sample_intersection.hit_p.z(), Real(0))) {
        phi = 0;
        if (sample_intersection.hit_p.y() > 0) {
            theta = 0;
        } else {
            theta = PI;
        }
    } else {
        phi = std::atan2(sample_intersection.hit_p.z(), sample_intersection.hit_p.x());
        if (phi < 0) {
            phi += TWO_PI;
        }
        theta = std::acos(sample_intersection.hit_p.y() / radius);
    }
    sample_intersection.uv = Vector2<Real>(phi / TWO_PI, theta / PI);

    return sample_intersection;
}
