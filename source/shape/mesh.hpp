#ifndef INTERSECTION_KERNEL_MESH_HPP
#define INTERSECTION_KERNEL_MESH_HPP

#include "triangle.hpp"

#include <vector>

// Loaded mesh class
class Mesh {
public:
    // Create mesh with given data
    Mesh(const std::vector<Vector3<Real>>& v,
         const std::vector<Vector3<Real>>& n,
         const std::vector<Vector2<Real>>& uv,
         const std::vector<int>& i);

    Mesh(std::vector<Vector3<Real>>&& v,
         std::vector<Vector3<Real>>&& n,
         std::vector<Vector2<Real>>&& uv,
         std::vector<int>&& i);

    Mesh(Mesh&& other) CXX_NOEXCEPT = default;

    Mesh& operator=(Mesh&& rhs) CXX_NOEXCEPT = default;

    // Access Mesh information
    CXX_INLINE std::size_t GetNumVertices() const CXX_NOEXCEPT {
        return vertices.size();
    }

    CXX_INLINE std::size_t GetNumTriangles() const CXX_NOEXCEPT {
        return triangles.size();
    }

    // Create list of triangles for the mesh given the transformation for them
    void GenerateTrianglesInstances(const Matrix4x4<Real>& l2w, const Matrix4x4<Real>& w2l,
                                    const MaterialInterface& material,
                                    std::vector<ShapeInstance>& shape_instances);

private:
    // Make Triangle and TriangleInstance a friend class so we can access the data directly
    friend class Triangle;

    // Generate local list of triangles
    void GenerateTriangles();

    // Description of the mesh
    std::vector<Vector3<Real>> vertices;
    std::vector<Vector3<Real>> normals;
    std::vector<Vector2<Real>> uvs;
    std::vector<int> indices;
    // Generate triangles
    std::vector<Triangle> triangles;
};

#endif // INTERSECTION_KERNEL_MESH_HPP
