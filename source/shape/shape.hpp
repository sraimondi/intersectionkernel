#ifndef INTERSECTIONKERNEL_SHAPE_HPP
#define INTERSECTIONKERNEL_SHAPE_HPP

#include "bounds.hpp"
#include "ray.hpp"
#include "material.hpp"

// Base Shape interface
class ShapeInterface {
public:
    virtual ~ShapeInterface() = default;

    // Intersect ray and fill the intersection
    virtual bool Intersect(const Ray& ray, Intersection& isect) const CXX_NOEXCEPT = 0;

    // Check if ray intersects the Shape
    virtual bool IntersectP(const Ray& ray, Real max_t) const CXX_NOEXCEPT = 0;

    // Compute area of the shape in local space
    virtual Real Area() const CXX_NOEXCEPT = 0;

    // Compute Bounds of the Shape in local space
    virtual Bounds LocalBounds() const CXX_NOEXCEPT = 0;

    // Compute Bounds subject to transformation
    virtual Bounds WorldBounds(const Matrix4x4<Real>& l2w) const CXX_NOEXCEPT;

    // Sample the Shape with respect to area
    virtual Intersection Sample(const Vector2<Real>& u) const CXX_NOEXCEPT = 0;

    // Compute PDF with respect to area
    virtual Real AreaPdf() const CXX_NOEXCEPT;
};

// Instance of a Shape in space with a given material
class ShapeInstance {
public:
    ShapeInstance(const Matrix4x4<Real>& l2w, const Matrix4x4<Real>& w2l,
                  const ShapeInterface& s,
                  const MaterialInterface& m);

    // Compute Bounds of the instance
    CXX_INLINE Bounds GetBounds() const CXX_NOEXCEPT {
        return shape.WorldBounds(local2world);
    }

    // Intersect ray with instance
    void Intersect(const Ray& ray, Intersection& isect) const CXX_NOEXCEPT;

    // Check for intersection with instance
    bool IntersectP(const Ray& ray, Real max_t) const CXX_NOEXCEPT;

    // Sample the ShapeInstace with respect to area
    Intersection Sample(const Vector2<Real>& u) const CXX_NOEXCEPT;

    // Compute PDF with respect to area
    Real AreaPdf() const CXX_NOEXCEPT;

    // Compute PDF with respect to solid angle
    Real SAPdf(const Intersection& isect, const Vector3<Real>& w) const CXX_NOEXCEPT;

    // Get reference to material
    CXX_INLINE const MaterialInterface& GetMaterial() const CXX_NOEXCEPT {
        return material;
    }

private:
    // Pointer to the local to world matrix and world to local matrix
    const Matrix4x4<Real>& local2world;
    const Matrix4x4<Real>& world2local;
    // Referenced triangle geometry
    const ShapeInterface& shape;
    // Material
    const MaterialInterface& material;
};

#endif //INTERSECTIONKERNEL_SHAPE_HPP
