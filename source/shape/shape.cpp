#include "shape.hpp"

Bounds ShapeInterface::WorldBounds(const Matrix4x4<Real>& l2w) const CXX_NOEXCEPT {
    const auto local_bounds = LocalBounds();
    // Compute vertices of the local bounds
    std::array<Vector3<Real>, 8> vertices;
    vertices[0] = local_bounds[0];
    vertices[1] = Vector3<Real>(local_bounds[1].x(), local_bounds[0].y(), local_bounds[0].z());
    vertices[2] = Vector3<Real>(local_bounds[1].x(), local_bounds[0].y(), local_bounds[1].z());
    vertices[3] = Vector3<Real>(local_bounds[0].x(), local_bounds[0].y(), local_bounds[1].z());
    vertices[4] = local_bounds[1];
    vertices[5] = Vector3<Real>(local_bounds[1].x(), local_bounds[1].y(), local_bounds[0].z());
    vertices[6] = Vector3<Real>(local_bounds[0].x(), local_bounds[1].y(), local_bounds[0].z());
    vertices[7] = Vector3<Real>(local_bounds[0].x(), local_bounds[1].y(), local_bounds[1].z());

    Bounds world_bounds;
    for (const auto& v : vertices) {
        world_bounds.Extend(l2w.TransformPosition(v));
    }

    return world_bounds;
}

Real ShapeInterface::AreaPdf() const CXX_NOEXCEPT {
    return 1 / Area();
}

ShapeInstance::ShapeInstance(const Matrix4x4<Real>& l2w, const Matrix4x4<Real>& w2l,
                             const ShapeInterface& s,
                             const MaterialInterface& m)
        : local2world(l2w), world2local(w2l), shape(s), material(m) {}

void ShapeInstance::Intersect(const Ray& ray, Intersection& isect) const CXX_NOEXCEPT {
    // Transform ray to local space
    const auto local_ray = ray.Transform(world2local);
    // Check for intersection
    if (shape.Intersect(local_ray, isect)) {
        isect.hit_shape = this;
        isect.TransformInPlace(local2world, world2local);
    }
}

bool ShapeInstance::IntersectP(const Ray& ray, Real max_t) const CXX_NOEXCEPT {
    // Transform ray to local space and check for hit
    const auto local_ray = ray.Transform(world2local);
    return shape.IntersectP(local_ray, max_t);
}

Intersection ShapeInstance::Sample(const Vector2<Real>& u) const CXX_NOEXCEPT {
    // Sample ths shape, transform to world space and return
    auto shape_isect = shape.Sample(u);
    shape_isect.hit_shape = this;
    shape_isect.TransformInPlace(local2world, world2local);
    return shape_isect;
}

Real ShapeInstance::AreaPdf() const CXX_NOEXCEPT {
    return shape.AreaPdf();
}

Real ShapeInstance::SAPdf(const Intersection& isect, const Vector3<Real>& w) const CXX_NOEXCEPT {
    // Generate ray from intersection in the given direction
    const auto ray = isect.SpawnRay(w);
    // Try to intersect the ray with this shape instance
    Intersection local_isect;
    Intersect(ray, local_isect);
    // Check if we hit
    if (local_isect.Hit()) {
        // Check if we are facing towards the normal
        const Real n_dot_w = Dot(local_isect.geometry_frame.n(), -w);
        if (n_dot_w > 0) {
            // Convert area pdf to solid angle for the given direction
            const Real pdf = DistanceSquared(isect.hit_p, local_isect.hit_p) / (n_dot_w * shape.Area());
            if (!std::isinf(pdf)) {
                return pdf;
            }
        }
    }

    return 0;
}
