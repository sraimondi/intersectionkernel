#ifndef INTERSECTIONKERNEL_SPHERE_HPP
#define INTERSECTIONKERNEL_SPHERE_HPP

#include "shape.hpp"

// Perfect Sphere class
class Sphere : public ShapeInterface {
public:
    CXX_CONSTEXPR explicit Sphere(Real r) CXX_NOEXCEPT
            : radius(r) {}

    bool Intersect(const Ray& ray, Intersection& isect) const CXX_NOEXCEPT override;

    bool IntersectP(const Ray& ray, Real max_t) const CXX_NOEXCEPT override;

    Real Area() const CXX_NOEXCEPT override;

    Bounds LocalBounds() const CXX_NOEXCEPT override;

    Intersection Sample(const Vector2<Real>& u) const CXX_NOEXCEPT override;

private:
    // Sphere radius
    const Real radius;
};

#endif //INTERSECTIONKERNEL_SPHERE_HPP
