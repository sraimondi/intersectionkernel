#include "mesh.hpp"

Mesh::Mesh(const std::vector<Vector3<Real>>& v,
           const std::vector<Vector3<Real>>& n,
           const std::vector<Vector2<Real>>& uv,
           const std::vector<int>& i)
        : vertices(v), normals(n), uvs(uv), indices(i) {
    GenerateTriangles();
}

Mesh::Mesh(std::vector<Vector3<Real>>&& v,
           std::vector<Vector3<Real>>&& n,
           std::vector<Vector2<Real>>&& uv,
           std::vector<int>&& i)
        : vertices(std::move(v)), normals(std::move(n)), uvs(std::move(uv)), indices(std::move(i)) {
    GenerateTriangles();
}

void Mesh::GenerateTrianglesInstances(const Matrix4x4<Real>& l2w, const Matrix4x4<Real>& w2l,
                                      const MaterialInterface& material,
                                      std::vector<ShapeInstance>& shape_instances) {
    // Increase storage capacity
    shape_instances.reserve(shape_instances.size() + triangles.size());
    // Add triangles
    for (const auto& triangle : triangles) {
        shape_instances.emplace_back(l2w, w2l, triangle, material);
    }
}

void Mesh::GenerateTriangles() {
    const std::size_t num_triangles = indices.size() / 3;
    triangles.reserve(num_triangles);
    for (std::size_t t = 0; t < num_triangles; ++t) {
        triangles.emplace_back(&indices[3 * t], *this);
    }
}

