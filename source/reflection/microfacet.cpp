//#include "microfacet.hpp"
//
//TrowbridgeReitzDistribution::TrowbridgeReitzDistribution(Real ax, Real ay)
//        : alpha_x(ax), alpha_y(ay) {}
//
//Real TrowbridgeReitzDistribution::D(const Vector3<Real>& wh) const CXX_NOEXCEPT {
//    const Real tan_2_theta = Frame::Tan2Theta(wh);
//    if (std::isinf(tan_2_theta)) { return 0; }
//    const Real cos_4_theta = Frame::Cos2Theta(wh) * Frame::Cos2Theta(wh);
//    const Real t = (Frame::Cos2Phi(wh) / (alpha_x * alpha_x) + Frame::Sin2Phi(wh) / (alpha_y * alpha_y)) * tan_2_theta;
//
//    return 1 / (PI * alpha_x * alpha_y * cos_4_theta * (1 + t) * (1 + t));
//}
//
//Real TrowbridgeReitzDistribution::G(const Vector3<Real>& wo, const Vector3<Real>& wi) const CXX_NOEXCEPT {
//    return 1 / (1 + Lambda(wo) + Lambda(wi));
//}
//
//Real TrowbridgeReitzDistribution::Lambda(const Vector3<Real>& w) const CXX_NOEXCEPT {
//    const Real abs_tan_theta = std::abs(Frame::TanTheta(w));
//    if (std::isinf(abs_tan_theta)) { return 0; }
//    // Compute alpha for direction w
//    const Real alpha = std::sqrt(Frame::Cos2Phi(w) * alpha_x * alpha_x + Frame::Sin2Phi(w) * alpha_y * alpha_y);
//    const Real alpha_2_tan_2_theta = (alpha * abs_tan_theta) * (alpha * abs_tan_theta);
//
//    return (-1 + std::sqrt(1 + alpha_2_tan_2_theta)) / 2;
//}
