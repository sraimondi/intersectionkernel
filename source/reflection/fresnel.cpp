#include "fresnel.hpp"

#include <algorithm>

Real FrDielectric(Real cos_theta_i, Real eta_i, Real eta_t) CXX_NOEXCEPT {
    cos_theta_i = Clamp(cos_theta_i, Real(-1), Real(1));

    // Compute cos_theta_t using Snell's law
    Real sin_theta_i = std::sqrt(std::max(Real(0), 1 - cos_theta_i * cos_theta_i));
    Real sin_theta_t = eta_i / eta_t * sin_theta_i;
    // Handle total internal reflection
    if (sin_theta_t >= 1) {
        return 1;
    }
    Real cos_theta_t = std::sqrt(std::max(Real(0), 1 - sin_theta_t * sin_theta_t));

    // Compute parallel light component
    const Real r_parallel = ((eta_t * cos_theta_i) - (eta_t * cos_theta_t)) /
                            ((eta_t * cos_theta_i) + (eta_i * cos_theta_t));
    const Real r_perp = ((eta_i * cos_theta_i) - (eta_t * cos_theta_t)) /
                        ((eta_i * cos_theta_i) + (eta_t * cos_theta_t));

    return (r_parallel * r_parallel + r_perp * r_perp) / 2;
}

Spectrum<Real> FresnelDielectric::Evaluate(Real cos_theta_i, Real eta_i, Real eta_t) const CXX_NOEXCEPT {
    return Spectrum<Real>(FrDielectric(cos_theta_i, eta_i, eta_t));
}

Spectrum<Real> FresnelFullReflection::Evaluate(Real, Real, Real) const CXX_NOEXCEPT {
    return Spectrum<Real>(1);
}
