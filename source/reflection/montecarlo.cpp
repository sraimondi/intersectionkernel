#include "montecarlo.hpp"

Vector3<Real> UniformSampleHemisphere(const Vector2<Real>& u) CXX_NOEXCEPT {
    const Real y = u[0];
    const Real r = std::sqrt(std::max(Real(0), 1 - y * y));
    const Real phi = TWO_PI * u[1];

    return {r * std::cos(phi), y, r * std::sin(phi)};
}

Vector3<Real> UniformSampleSphere(const Vector2<Real>& u) CXX_NOEXCEPT {
    const Real y = 1 - 2 * u[0];
    const Real r = std::sqrt(std::max(Real(0), 1 - y * y));
    const Real phi = TWO_PI * u[1];

    return {r * std::cos(phi), y, r * std::sin(phi)};
}

Vector2<Real> ConcentricSampleDisk(const Vector2<Real>& u) CXX_NOEXCEPT {
    // Bring point in [-1, 1] range
    const auto offset = 2 * u - Vector2<Real>(1);
    // Handle origin case
    if (ApproxEqual(offset[0], Real(0)) && ApproxEqual(offset[1], Real(1))) {
        return {0, 0};
    }

    // Apply concentric mapping
    Real theta, r;
    if (std::abs(offset[0]) > std::abs(offset[1])) {
        r = offset[0];
        theta = PI_OVER_4 * (offset[1] / offset[0]);
    } else {
        r = offset[1];
        theta = PI_OVER_2 - PI_OVER_4 * (offset[0] / offset[1]);
    }

    return r * Vector2<Real>(std::cos(theta), std::sin(theta));
}

Vector3<Real> CosineSampleHemisphere(const Vector2<Real>& u) CXX_NOEXCEPT {
    const auto p_circle = ConcentricSampleDisk(u);
    const Real y = std::sqrt(std::max(Real(0), 1 - p_circle.x() * p_circle.x() - p_circle.y() * p_circle.y()));

    return {p_circle.x(), y, p_circle.y()};
}

Vector2<Real> UniformSampleTriangle(const Vector2<Real>& u) CXX_NOEXCEPT {
    const Real u0 = std::sqrt(u[0]);

    return {1 - u0, u[1] * u0};
}
