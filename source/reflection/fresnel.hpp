#ifndef INTERSECTIONKERNEL_FRESNEL_HPP
#define INTERSECTIONKERNEL_FRESNEL_HPP

#include "spectrum.hpp"

// Compute Fresnel reflection for dielectric material
Real FrDielectric(Real cos_theta_i, Real eta_i, Real eta_t) CXX_NOEXCEPT;

// Fresnel interface
class FresnelInterface {
public:
    // Evaluate Fresnel value
    virtual Spectrum<Real> Evaluate(Real cos_theta_i, Real eta_i, Real eta_t) const CXX_NOEXCEPT = 0;
};

// Dielectrics material interface
class FresnelDielectric : public FresnelInterface {
public:
    Spectrum<Real> Evaluate(Real cos_theta_i, Real eta_i, Real eta_t) const CXX_NOEXCEPT override;
};

// Ideal 100% reflection Fresnel
class FresnelFullReflection : public FresnelInterface {
public:
    Spectrum<Real> Evaluate(Real cos_theta_i, Real eta_i, Real eta_t) const CXX_NOEXCEPT override;
};

#endif //INTERSECTIONKERNEL_FRESNEL_HPP
