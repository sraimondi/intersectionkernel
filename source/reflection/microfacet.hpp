//#ifndef INTERSECTIONKERNEL_MICROFACET_HPP
//#define INTERSECTIONKERNEL_MICROFACET_HPP
//
//#include "spectrum.hpp"
//#include "frame.hpp"
//#include "fresnel.hpp"
//
//// Trowbridge-Reitz microfacet distribution, all methods assume local space vectors
//class TrowbridgeReitzDistribution {
//public:
//    TrowbridgeReitzDistribution(Real ax, Real ay);
//
//    // Evaluate D term for distribution
//    Real D(const Vector3<Real>& wh) const CXX_NOEXCEPT;
//
//    // Evaluate masking term
//    Real G(const Vector3<Real>& wo, const Vector3<Real>& wi) const CXX_NOEXCEPT;
//
//    // Convert roughness value
//    static CXX_INLINE Real RoughnessToAlpha(Real roughness) CXX_NOEXCEPT {
//        roughness = std::max(roughness, EPS);
//        const Real x = std::log(roughness);
//
//        return Real(1.62142) + Real(0.819955) * x + Real(0.1734) * x * x + Real(0.0171201) * x * x * x +
//               Real(0.000640711) * x * x * x * x;
//    }
//
//private:
//    // Evaluate lambda function
//    Real Lambda(const Vector3<Real>& w) const CXX_NOEXCEPT;
//
//    // Alpha in the x and y directions
//    const Real alpha_x, alpha_y;
//};
//
//// Evaluate Microfacet reflection for a given distribution, directions are expected to be in local space
//template <typename Distribution>
//Spectrum<Real> EvaluateMicrofacet(const Spectrum<Real>& reflectivity,
//                                  const Distribution& distribution,
//                                  const FresnelInterface& fresnel,
//                                  Real ei, Real et,
//                                  const Vector3<Real>& wo,
//                                  const Vector3<Real>& wi) CXX_NOEXCEPT {
//    const Real cos_theta_o = Frame::CosTheta(wo);
//    const Real cos_theta_i = Frame::CosTheta(wi);
//    // Compute halfway vector
//    Vector3<Real> wh = wi + wo;
//    // Handle degenerate case
//    if (ApproxEqual(cos_theta_o, Real(0)) || ApproxEqual(cos_theta_i, Real(0))) {
//        return Spectrum<Real>(0);
//    }
//    if (ApproxEqual(wh.x(), Real(0)) && ApproxEqual(wh.y(), Real(0)) && ApproxEqual(wh.z(), Real(0))) {
//        return Spectrum<Real>(0);
//    }
//    wh = Normalize(wh);
//    // Evaluate Fresnel term
//    const Spectrum<Real> fr = fresnel.Evaluate(Dot(wi, wh), ei, et);
//
//    return reflectivity * distribution.D(wh) * distribution.G(wo, wi) * fr / (4 * cos_theta_i * cos_theta_o);
//}
//
//#endif //INTERSECTIONKERNEL_MICROFACET_HPP
