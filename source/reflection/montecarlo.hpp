#ifndef INTERSECTIONKERNEL_MONTECARLO_HPP
#define INTERSECTIONKERNEL_MONTECARLO_HPP

#include "lin_algebra.hpp"

// Uniform hemisphere sampling
Vector3<Real> UniformSampleHemisphere(const Vector2<Real>& u) CXX_NOEXCEPT;

CXX_CONSTEXPR Real UniformHemispherePDF() CXX_NOEXCEPT {
    return INV_2PI;
}

// Uniform sphere sampling
Vector3<Real> UniformSampleSphere(const Vector2<Real>& u) CXX_NOEXCEPT;

CXX_CONSTEXPR Real UniformSpherePDF() CXX_NOEXCEPT {
    return INV_4PI;
}

// Concentric disk sampling
Vector2<Real> ConcentricSampleDisk(const Vector2<Real>& u) CXX_NOEXCEPT;

// Cosine hemisphere sampling
Vector3<Real> CosineSampleHemisphere(const Vector2<Real>& u) CXX_NOEXCEPT;

CXX_INLINE Real CosineHemispherePDF(Real cos_theta) CXX_NOEXCEPT {
    return cos_theta * INV_PI;
}

// Sample triangle uniformly with respect to area, returns 2 barycentric coordinates
Vector2<Real> UniformSampleTriangle(const Vector2<Real>& u) CXX_NOEXCEPT;

CXX_INLINE Real UniformTrianglePDF(Real tr_area) CXX_NOEXCEPT {
    return 1 / tr_area;
}

#endif //INTERSECTIONKERNEL_MONTECARLO_HPP
