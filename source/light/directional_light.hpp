#ifndef INTERSECTIONKERNEL_DIRECTIONAL_LIGHT_HPP
#define INTERSECTIONKERNEL_DIRECTIONAL_LIGHT_HPP

#include "light.hpp"

// Directional light class
class DirectionalLight : public LightInterface {
public:
    CXX_INLINE DirectionalLight(const Vector3<Real> dir, const Spectrum<Real>& i) CXX_NOEXCEPT
            : direction(Normalize(dir)), intensity(i) {}

    bool IsDeltaLight() const CXX_NOEXCEPT override;

    Spectrum<Real> SampleLi(const Intersection& ref_isect, const Vector2<Real>& u,
                            LightSample& light_sample, OcclusionTester* occlusion) const CXX_NOEXCEPT override;

    Real PdfLi(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

private:
    // Light direction
    const Vector3<Real> direction;
    // Light intensity
    const Spectrum<Real> intensity;
};

#endif //INTERSECTIONKERNEL_DIRECTIONAL_LIGHT_HPP
