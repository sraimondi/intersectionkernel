#include "point_light.hpp"

bool PointLight::IsDeltaLight() const CXX_NOEXCEPT {
    return true;
}

Spectrum<Real> PointLight::SampleLi(const Intersection& ref_isect, const Vector2<Real>&,
                                    LightSample& light_sample, OcclusionTester* occlusion) const CXX_NOEXCEPT {
    // Point light only has one direction we can sample
    light_sample.wi = Normalize(position - ref_isect.hit_p);
    light_sample.pdf = 1;
    // Setup occlusion tester
    occlusion->TwoPoints(ref_isect.hit_p, position);

    return intensity / DistanceSquared(ref_isect.hit_p, position);
}

Real PointLight::PdfLi(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return 0;
}
