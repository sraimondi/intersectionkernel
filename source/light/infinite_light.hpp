#ifndef INTERSECTIONKERNEL_INFINITE_LIGHT_HPP
#define INTERSECTIONKERNEL_INFINITE_LIGHT_HPP

#include "light.hpp"

#include <functional>

// Type of the function that the InfiniteLight class expects and is used to return the radiance of an infinite Ray
using SphericalFunction = std::function<Spectrum<Real>(const Vector3<Real>& ray)>;

// Infinite light class
class InfiniteLight : public LightInterface {
public:
    InfiniteLight(SphericalFunction f, std::size_t ns) CXX_NOEXCEPT;

    bool IsDeltaLight() const CXX_NOEXCEPT override;

    Spectrum<Real> SampleLi(const Intersection& ref_isect, const Vector2<Real>& u,
                            LightSample& light_sample, OcclusionTester* occlusion) const CXX_NOEXCEPT override;

    Real PdfLi(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

    Spectrum<Real> Le(const Ray& ray) const CXX_NOEXCEPT override;

private:
    // Function used to evaluate the incoming radiance
    const SphericalFunction spherical_function;
};

#endif //INTERSECTIONKERNEL_INFINITE_LIGHT_HPP
