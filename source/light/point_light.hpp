#ifndef INTERSECTIONKERNEL_POINT_LIGHT_HPP
#define INTERSECTIONKERNEL_POINT_LIGHT_HPP

#include "light.hpp"

// Point light class
class PointLight : public LightInterface {
public:
    CXX_CONSTEXPR PointLight(const Vector3<Real>& p, const Spectrum<Real>& i) CXX_NOEXCEPT
            : position(p), intensity(i) {}

    bool IsDeltaLight() const CXX_NOEXCEPT override;

    Spectrum<Real> SampleLi(const Intersection& ref_isect, const Vector2<Real>& u,
                            LightSample& light_sample, OcclusionTester* occlusion) const CXX_NOEXCEPT override;

    Real PdfLi(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

private:
    // Light position
    const Vector3<Real> position;
    // Light intensity
    const Spectrum<Real> intensity;
};

#endif //INTERSECTIONKERNEL_POINT_LIGHT_HPP
