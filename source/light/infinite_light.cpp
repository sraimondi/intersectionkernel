#include "infinite_light.hpp"
#include "montecarlo.hpp"

InfiniteLight::InfiniteLight(SphericalFunction f, std::size_t ns) CXX_NOEXCEPT
        : LightInterface(ns), spherical_function(std::move(f)) {}

bool InfiniteLight::IsDeltaLight() const CXX_NOEXCEPT {
    return false;
}

Spectrum<Real> InfiniteLight::SampleLi(const Intersection& ref_isect, const Vector2<Real>& u,
                                       LightSample& light_sample, OcclusionTester* occlusion) const CXX_NOEXCEPT {
    // Sample direction uniformly on the hemisphere
    light_sample.wi = UniformSampleHemisphere(u);
    light_sample.pdf = UniformHemispherePDF();

    // Setup occlusion tester
    occlusion->PointDirection(ref_isect.hit_p, light_sample.wi);

    return spherical_function(light_sample.wi);
}

Real InfiniteLight::PdfLi(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return UniformHemispherePDF();
}

Spectrum<Real> InfiniteLight::Le(const Ray& ray) const CXX_NOEXCEPT {
    return spherical_function(Normalize(ray.Direction()));
}

