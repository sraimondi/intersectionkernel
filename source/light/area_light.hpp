#ifndef INTERSECTIONKERNEL_AREA_LIGHT_HPP
#define INTERSECTIONKERNEL_AREA_LIGHT_HPP

#include "light.hpp"

// Area light class
class AreaLight : public LightInterface {
public:
    AreaLight(const ShapeInstance& s, std::size_t ns) CXX_NOEXCEPT;

    bool IsDeltaLight() const CXX_NOEXCEPT override;

    Spectrum<Real> SampleLi(const Intersection& ref_isect, const Vector2<Real>& u,
                            LightSample& light_sample, OcclusionTester* occlusion) const CXX_NOEXCEPT override;

    Real PdfLi(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

    const ShapeInstance* GetRepresentingShape() const CXX_NOEXCEPT override;

private:
    // ShapeInstance representing the area
    const ShapeInstance& shape;
};

#endif //INTERSECTIONKERNEL_AREA_LIGHT_HPP
