#include "light.hpp"
#include "scene.hpp"

void OcclusionTester::TwoPoints(const Vector3<Real>& start, const Vector3<Real>& end) CXX_NOEXCEPT {
    const Vector3<Real> dir = end - start;
    occlusion_ray = {start + EPS * Normalize(dir), dir};
    extent = 1 - 2 * EPS;
}

void OcclusionTester::PointDirection(const Vector3<Real>& start, const Vector3<Real>& dir) CXX_NOEXCEPT {
    occlusion_ray = {start + EPS * Normalize(dir), dir};
}

bool OcclusionTester::Occluded(const Scene& scene) const CXX_NOEXCEPT {
    return scene.IntersectP(occlusion_ray, extent);
}

Spectrum<Real> LightInterface::Le(const Ray&) const CXX_NOEXCEPT {
    return Spectrum<Real>(0);
}

const ShapeInstance* LightInterface::GetRepresentingShape() const CXX_NOEXCEPT {
    return nullptr;
}
