#ifndef INTERSECTIONKERNEL_LIGHT_HPP
#define INTERSECTIONKERNEL_LIGHT_HPP

#include "spectrum.hpp"
#include "intersection.hpp"

class Scene;

// Light sample class
struct LightSample {
    CXX_CONSTEXPR LightSample() CXX_NOEXCEPT
            : pdf(0) {}

    // Direction to light
    Vector3<Real> wi;
    // Solid angle PDF
    Real pdf;
};

// Visibility tester class
class OcclusionTester {
public:
    CXX_CONSTEXPR OcclusionTester() CXX_NOEXCEPT
            : extent(INF) {}

    // Setup from two points
    void TwoPoints(const Vector3<Real>& start, const Vector3<Real>& end) CXX_NOEXCEPT;

    // Setup from point and direction
    void PointDirection(const Vector3<Real>& start, const Vector3<Real>& dir) CXX_NOEXCEPT;

    // Check if the ray between the two points is occluded
    bool Occluded(const Scene& scene) const CXX_NOEXCEPT;

private:
    // Ray to test
    Ray occlusion_ray;
    // Extent to test
    Real extent;
};

// Light base interface
class LightInterface {
public:
    CXX_CONSTEXPR explicit LightInterface(std::size_t ns = 1) CXX_NOEXCEPT
            : num_samples(ns) {}

    virtual ~LightInterface() = default;

    // Get number of samples
    CXX_CONSTEXPR std::size_t GetNumSamples() const CXX_NOEXCEPT {
        return num_samples;
    }

    // Return true or false depending if the light is a delta light
    virtual bool IsDeltaLight() const CXX_NOEXCEPT = 0;

    // Sample incoming light at a given intersection
    virtual Spectrum<Real> SampleLi(const Intersection& ref_isect, const Vector2<Real>& u,
                                    LightSample& light_sample, OcclusionTester* occlusion) const CXX_NOEXCEPT = 0;

    // Compute probability with respect to solid angle of a certain direction for this light
    virtual Real PdfLi(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT = 0;

    // Compute radiance coming from a ray that did not intersect, defaults to black
    virtual Spectrum<Real> Le(const Ray& ray) const CXX_NOEXCEPT;

    // Return pointer to a shape, if the light is represented by it, defaults to nullptr
    virtual const ShapeInstance* GetRepresentingShape() const CXX_NOEXCEPT;

protected:
    // Number of samples for the light
    const std::size_t num_samples;
};

#endif //INTERSECTIONKERNEL_LIGHT_HPP
