#include "directional_light.hpp"

bool DirectionalLight::IsDeltaLight() const CXX_NOEXCEPT {
    return true;
}

Spectrum<Real> DirectionalLight::SampleLi(const Intersection& ref_isect, const Vector2<Real>&,
                                          LightSample& light_sample, OcclusionTester* occlusion) const CXX_NOEXCEPT {
    // Sampling direction is the inverse of light direction
    light_sample.wi = direction;
    light_sample.pdf = 1;
    // Setup occlusion tester
    occlusion->PointDirection(ref_isect.hit_p, direction);

    return intensity;
}

Real DirectionalLight::PdfLi(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return 0;
}
