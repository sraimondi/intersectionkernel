#include "area_light.hpp"
#include "shape.hpp"
#include "material.hpp"

AreaLight::AreaLight(const ShapeInstance& s, std::size_t ns) CXX_NOEXCEPT
        : LightInterface(ns), shape(s) {}

bool AreaLight::IsDeltaLight() const CXX_NOEXCEPT {
    return false;
}

Spectrum<Real> AreaLight::SampleLi(const Intersection& ref_isect, const Vector2<Real>& u,
                                   LightSample& light_sample, OcclusionTester* occlusion) const CXX_NOEXCEPT {
    // Sample point on the shape
    const auto shape_sample = shape.Sample(u);

    // Fill light sample
    light_sample.wi = Normalize(shape_sample.hit_p - ref_isect.hit_p);
    light_sample.pdf = shape.SAPdf(ref_isect, light_sample.wi);

    // Setup occlusion tester
    occlusion->TwoPoints(ref_isect.hit_p, shape_sample.hit_p);

    return shape.GetMaterial().Emission(shape_sample, -light_sample.wi);
}

Real AreaLight::PdfLi(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT {
    return shape.SAPdf(isect, wi);
}

const ShapeInstance* AreaLight::GetRepresentingShape() const CXX_NOEXCEPT {
    return &shape;
}
