#ifndef INTERSECTIONKERNEL_SPECULAR_MATERIAL_HPP
#define INTERSECTIONKERNEL_SPECULAR_MATERIAL_HPP

#include "material.hpp"
#include "texture.hpp"
#include "fresnel.hpp"

// Specular reflection material
class SpecularReflectionMaterial : public MaterialInterface {
public:
    CXX_CONSTEXPR explicit SpecularReflectionMaterial(const TextureInterface<Spectrum<Real>>& ref) CXX_NOEXCEPT
            : MaterialInterface(MaterialType(M_REFLECTION | M_SPECULAR)), reflection(ref), fresnel() {}

    Spectrum<Real> F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

    Spectrum<Real> SampleF(const Intersection& isect, const Vector2<Real>& u,
                           MaterialSample& material_sample) const CXX_NOEXCEPT override;

    Real Pdf(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

private:
    // Reflection value, leave to 1 for physically based
    const TextureInterface<Spectrum<Real>>& reflection;
    // Fresnel term evaluator
    const FresnelFullReflection fresnel;
};

// Specular transmission material
class SpecularTransmissionMaterial : public MaterialInterface {
public:
    CXX_CONSTEXPR SpecularTransmissionMaterial(const TextureInterface<Spectrum<Real>>& tr,
                                               const TextureInterface<Real>& ea,
                                               const TextureInterface<Real>& eb) CXX_NOEXCEPT
            : MaterialInterface(MaterialType(M_TRANSMISSION | M_SPECULAR)),
              transmission(tr), eta_a(ea), eta_b(eb), fresnel() {}

    Spectrum<Real> F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

    Spectrum<Real> SampleF(const Intersection& isect, const Vector2<Real>& u,
                           MaterialSample& material_sample) const CXX_NOEXCEPT override;

    Real Pdf(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

private:
    // Transmission value, leave to 1 for physically based
    const TextureInterface<Spectrum<Real>>& transmission;
    // Refraction coefficients outside and inside of the material
    const TextureInterface<Real>& eta_a;
    const TextureInterface<Real>& eta_b;
    // Fresnel term evaluator
    const FresnelDielectric fresnel;
};

// Dielectric material
class DielectricMaterial : public MaterialInterface {
public:
    CXX_CONSTEXPR DielectricMaterial(const TextureInterface<Spectrum<Real>>& tr,
                                     const TextureInterface<Spectrum<Real>>& ref,
                                     const TextureInterface<Real>& ea,
                                     const TextureInterface<Real>& eb) CXX_NOEXCEPT
            : MaterialInterface(MaterialType(M_REFLECTION | M_TRANSMISSION | M_SPECULAR)),
              transmission(tr), reflection(ref), eta_a(ea), eta_b(eb), fresnel() {}

    Spectrum<Real> F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

    Spectrum<Real> SampleF(const Intersection& isect, const Vector2<Real>& u,
                           MaterialSample& material_sample) const CXX_NOEXCEPT override;

    Real Pdf(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

private:
    // Transmission and reflection value, leave both to 1 for realistic Fresnel blending
    const TextureInterface<Spectrum<Real>>& transmission;
    const TextureInterface<Spectrum<Real>>& reflection;
    // Refraction coefficients outside and inside of the material
    const TextureInterface<Real>& eta_a;
    const TextureInterface<Real>& eta_b;
    // Fresnel term evaluator
    const FresnelDielectric fresnel;
};

#endif //INTERSECTIONKERNEL_SPECULAR_MATERIAL_HPP
