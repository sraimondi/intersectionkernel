#ifndef INTERSECTIONKERNEL_DIFFUSE_MATERIAL_HPP
#define INTERSECTIONKERNEL_DIFFUSE_MATERIAL_HPP

#include "material.hpp"
#include "texture.hpp"

// Diffuse lambert material
class DiffuseLambertMaterial : public MaterialInterface {
public:
    CXX_CONSTEXPR explicit DiffuseLambertMaterial(const TextureInterface<Spectrum<Real>>& r) CXX_NOEXCEPT
            : MaterialInterface(MaterialType(M_REFLECTION | M_DIFFUSE)), rho(r) {}

    Spectrum<Real> F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

private:
    // Reflectance value
    const TextureInterface<Spectrum<Real>>& rho;
};

// Diffuse material based on Oren-Nayar model
class DiffuseOrenNayarMaterial : public MaterialInterface {
public:
    CXX_CONSTEXPR DiffuseOrenNayarMaterial(const TextureInterface<Spectrum<Real>>& r,
                                           const TextureInterface<Real>& rough) CXX_NOEXCEPT
            : MaterialInterface(MaterialType(M_REFLECTION | M_DIFFUSE)), rho(r), roughness(rough) {}

    Spectrum<Real> F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

private:
    // Reflectance value
    const TextureInterface<Spectrum<Real>>& rho;
    // Surface roughness
    const TextureInterface<Real>& roughness;
};

#endif //INTERSECTIONKERNEL_DIFFUSE_MATERIAL_HPP
