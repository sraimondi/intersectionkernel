//#include "plastic_material.hpp"
//#include "microfacet.hpp"
//
//Spectrum<Real> PlasticMaterial::F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT {
//    // Since the BRDF is reflecting, we only evaluate it if both directions are in the same hemisphere
//    if (DirectionsSameHemisphere(isect, wi)) {
//        // Evaluate diffuse part
//        const auto diff_component = INV_PI * Kd.Evaluate(isect);
//
//        // Evaluate the glossy part of the BRDF
//        Spectrum<Real> spec_component;
//        const auto ks = Ks.Evaluate(isect);
//        if (!ks.IsBlack()) {
//            Real rough = roughness.Evaluate(isect);
//            if (remap_roughness) {
//                rough = TrowbridgeReitzDistribution::RoughnessToAlpha(rough);
//            }
//            // Create microfacet distribution
//            TrowbridgeReitzDistribution mf_distribution(rough, rough);
//            // Transform vectors to local space
//            const auto local_wo = Normalize(isect.geometry_frame.ToLocal(isect.wo));
//            const auto local_wi = Normalize(isect.geometry_frame.ToLocal(wi));
//
//            spec_component = EvaluateMicrofacet(ks, mf_distribution, fresnel, 1, 1.5, local_wo, local_wi);
//        }
//
//        return diff_component + spec_component;
//    }
//
//    return Spectrum<Real>(0);
//}
//
//Spectrum<Real> PlasticMaterial::SampleF(const Intersection& isect, const Vector2<Real>& u,
//                                        MaterialSample& material_sample) const CXX_NOEXCEPT {
//    return MaterialInterface::SampleF(isect, u, material_sample);
//}
//
//Real PlasticMaterial::Pdf(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT {
//    return MaterialInterface::Pdf(isect, wi);
//}
