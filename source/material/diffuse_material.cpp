#include "diffuse_material.hpp"

Spectrum<Real> DiffuseLambertMaterial::F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT {
    if (DirectionsSameHemisphere(isect, wi)) {
        const auto rho_at_isect = rho.Evaluate(isect);
        if (!rho_at_isect.IsBlack()) {
            return INV_PI * rho_at_isect;
        }
    }

    return Spectrum<Real>(0);
}

Spectrum<Real> DiffuseOrenNayarMaterial::F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT {
    if (DirectionsSameHemisphere(isect, wi)) {
        // Compute values for Oren-Nayar micorfacet model
        const Real sigma = DegToRad(roughness.Evaluate(isect));
        const Real sigma2 = sigma * sigma;
        const Real A = 1 - (sigma2 / (2 * (sigma2 + Real(0.33))));
        const Real B = Real(0.45) * sigma2 / (sigma2 + Real(0.09));

        // Transform to local space
        const auto local_wo = isect.geometry_frame.ToLocal(isect.wo);
        const auto local_wi = isect.geometry_frame.ToLocal(wi);
        // Compute sine of both direction
        const Real sin_theta_i = Frame::SinTheta(local_wi);
        const Real sin_theta_o = Frame::SinTheta(local_wo);

        // Compute cosine term of Oren-Nayar term
        Real max_cos = 0;
        if (sin_theta_i > 1e-4 && sin_theta_o > 1e-4) {
            const Real sin_phi_i = Frame::SinPhi(local_wi);
            const Real cos_phi_i = Frame::CosPhi(local_wi);
            const Real sin_phi_o = Frame::SinPhi(local_wo);
            const Real cos_phi_o = Frame::CosPhi(local_wo);
            const Real d_cos = cos_phi_i * cos_phi_o + sin_phi_i * sin_phi_o;
            max_cos = std::max(Real(0), d_cos);
        }

        // Compute sine and tangent terms of Oren-Nayar term
        Real sin_alpha, tan_beta;
        if (Frame::CosTheta(local_wi) > Frame::CosTheta(local_wo)) {
            sin_alpha = sin_theta_o;
            tan_beta = sin_theta_i / Frame::CosTheta(local_wi);
        } else {
            sin_alpha = sin_theta_i;
            tan_beta = sin_theta_o / Frame::CosTheta(local_wo);
        }

        const auto rho_at_isect = rho.Evaluate(isect);
        if (!rho_at_isect.IsBlack()) {
            return INV_PI * rho_at_isect * (A + B * max_cos * sin_alpha * tan_beta);
        }
    }

    return Spectrum<Real>(0);
}
