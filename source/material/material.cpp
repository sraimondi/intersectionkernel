#include "montecarlo.hpp"
#include "material.hpp"

bool MaterialInterface::IsEmitting() const CXX_NOEXCEPT {
    return MaterialTypeMatch(M_EMISSION, material_type);
}

Spectrum<Real> MaterialInterface::Emission(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return Spectrum<Real>(0);
}

Spectrum<Real> MaterialInterface::F(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return Spectrum<Real>(0);
}

Spectrum<Real> MaterialInterface::SampleF(const Intersection& isect, const Vector2<Real>& u,
                                          MaterialSample& material_sample) const CXX_NOEXCEPT {
    // Cosine sample the hemisphere
    const auto wi = CosineSampleHemisphere(u);
    material_sample.wi = isect.geometry_frame.ToWorld(wi);
    // Evaluate PDF
    material_sample.pdf = Pdf(isect, material_sample.wi);
    // Store type
    material_sample.sampled_type = material_type;

    return F(isect, material_sample.wi);
}

Real MaterialInterface::Pdf(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT {
    // Check if the two vector are in the same hemisphere and return cosine probability
    return DirectionsSameHemisphere(isect, wi) ? INV_PI * Dot(isect.geometry_frame.n(), wi) : 0;
}

