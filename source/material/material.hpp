#ifndef INTERSECTIONKERNEL_MATERIAL_HPP
#define INTERSECTIONKERNEL_MATERIAL_HPP

#include "intersection.hpp"
#include "spectrum.hpp"

// Material types flag
enum MaterialType {
    M_REFLECTION = 1 << 0,
    M_TRANSMISSION = 1 << 1,
    M_EMISSION = 1 << 2,
    M_DIFFUSE = 1 << 3,
    M_GLOSSY = 1 << 4,
    M_SPECULAR = 1 << 5,
};

// Check if the first given MaterialType is contained in the second
CXX_INLINE bool MaterialTypeMatch(const MaterialType type, const MaterialType match) CXX_NOEXCEPT {
    return (type & match) == type;
}

// Material sample
struct MaterialSample {
    CXX_CONSTEXPR MaterialSample() CXX_NOEXCEPT
            : pdf(0), sampled_type(MaterialType(0)) {}

    // Sampled incoming light direction
    Vector3<Real> wi;
    // PDF
    Real pdf;
    // Material type
    MaterialType sampled_type;
};

// Base material interface
class MaterialInterface {
public:
    CXX_CONSTEXPR explicit MaterialInterface(const MaterialType type) CXX_NOEXCEPT
            : material_type(type) {}

    // Checker for emitting material
    bool IsEmitting() const CXX_NOEXCEPT;

    // Check if material matches certain constraint on the type
    CXX_INLINE bool MatchesType(const MaterialType type) const CXX_NOEXCEPT {
        return MaterialTypeMatch(type, material_type);
    }

    // Compute emission in a given direction
    virtual Spectrum<Real> Emission(const Intersection& isect, const Vector3<Real>& w) const CXX_NOEXCEPT;

    // Evaluate BRDF
    virtual Spectrum<Real> F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT;

    // Sample BRDF, default is cosine sampling
    virtual Spectrum<Real> SampleF(const Intersection& isect, const Vector2<Real>& u,
                                   MaterialSample& material_sample) const CXX_NOEXCEPT;

    // Compute PDF of the BRDF for the two given direction in, default is cosine
    virtual Real Pdf(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT;

protected:
    // Check that incoming and outgoing light direction are in the same hemisphere with respect to the geometric normal
    CXX_CONSTEXPR bool DirectionsAreConsistent(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT {
        return Dot(isect.wo, isect.geometry_frame.n()) * Dot(isect.geometry_frame.n(), wi) > 0;
    }

    // Checks that both incoming and outgoing light directions are in the same hemisphere with the normal
    CXX_CONSTEXPR bool DirectionsSameHemisphere(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT {
        return (SameHemisphere(isect.wo, isect.geometry_frame.n()) && SameHemisphere(wi, isect.geometry_frame.n()));
    }

    // Material type
    const MaterialType material_type;
};

#endif //INTERSECTIONKERNEL_MATERIAL_HPP
