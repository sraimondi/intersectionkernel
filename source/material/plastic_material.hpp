//#ifndef INTERSECTIONKERNEL_PLASTIC_MATERIAL_HPP
//#define INTERSECTIONKERNEL_PLASTIC_MATERIAL_HPP
//
//#include "material.hpp"
//#include "microfacet.hpp"
//#include "texture.hpp"
//
//// Plastic material modeled with a diffuse part and a glossy part based on Trowbridge-Reitz distribution
//class PlasticMaterial : public MaterialInterface {
//public:
//    CXX_CONSTEXPR PlasticMaterial(const TextureInterface<Spectrum<Real>>& kd,
//                                  const TextureInterface<Spectrum<Real>>& ks,
//                                  const TextureInterface<Real>& rough,
//                                  bool rr)
//            : MaterialInterface(MaterialType(M_REFLECTION | M_DIFFUSE | M_GLOSSY)),
//              Kd(kd), Ks(ks), roughness(rough), remap_roughness(rr) {}
//
//    Spectrum<Real> F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;
//
//    Spectrum<Real> SampleF(const Intersection& isect, const Vector2<Real>& u,
//                           MaterialSample& material_sample) const CXX_NOEXCEPT override;
//
//    Real Pdf(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;
//
//private:
//    // Diffuse and specular values
//    const TextureInterface<Spectrum<Real>>& Kd;
//    const TextureInterface<Spectrum<Real>>& Ks;
//    // Roughness of the surface
//    const TextureInterface<Real>& roughness;
//    // Fresnel term evaluator passed to the Microfacet model evaluator
//    const FresnelDielectric fresnel;
//    // Flag that tells us if we need to remap the roughness parameter
//    const bool remap_roughness;
//};
//
//#endif //INTERSECTIONKERNEL_PLASTIC_MATERIAL_HPP
