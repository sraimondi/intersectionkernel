#include "specular_material.hpp"

Spectrum<Real> SpecularReflectionMaterial::F(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return Spectrum<Real>(0);
}

Spectrum<Real> SpecularReflectionMaterial::SampleF(const Intersection& isect, const Vector2<Real>&,
                                                   MaterialSample& material_sample) const CXX_NOEXCEPT {
    // Sample with perfect reflection
    material_sample.wi = Reflect(isect.wo, isect.geometry_frame.n());
    material_sample.pdf = 1;
    material_sample.sampled_type = material_type;

    return fresnel.Evaluate(Dot(material_sample.wi, isect.geometry_frame.n()), 0, 0) * reflection.Evaluate(isect);
}

Real SpecularReflectionMaterial::Pdf(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return 0;
}

Spectrum<Real> SpecularTransmissionMaterial::F(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return Spectrum<Real>(0);
}

Spectrum<Real> SpecularTransmissionMaterial::SampleF(const Intersection& isect, const Vector2<Real>&,
                                                     MaterialSample& material_sample) const CXX_NOEXCEPT {
    // Set material type
    material_sample.sampled_type = material_type;

    // Figure out if we are entering or exiting
    const bool entering = Dot(isect.geometry_frame.n(), isect.wo) > 0;
    // Set normal accordingly
    const auto normal = entering ? isect.geometry_frame.n() : -isect.geometry_frame.n();
    // Set refraction coefficients
    const Real eta_i = entering ? eta_a.Evaluate(isect) : eta_b.Evaluate(isect);
    const Real eta_t = entering ? eta_b.Evaluate(isect) : eta_a.Evaluate(isect);

    // Compute transmission direction, if any
    if (!Refract(isect.wo, normal, eta_i / eta_t, material_sample.wi)) {
        material_sample.pdf = 0;
        return Spectrum<Real>(0);
    }

    // Set pdf value to 1 and return BRDF value
    material_sample.pdf = 1;

    return (eta_i * eta_i) / (eta_t * eta_t) * transmission.Evaluate(isect) *
           (Spectrum<Real>(1) - fresnel.Evaluate(Dot(material_sample.wi, -normal), eta_i, eta_t));
}

Real SpecularTransmissionMaterial::Pdf(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return 0;
}

Spectrum<Real> DielectricMaterial::F(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return Spectrum<Real>(0);
}

Spectrum<Real> DielectricMaterial::SampleF(const Intersection& isect, const Vector2<Real>& u,
                                           MaterialSample& material_sample) const CXX_NOEXCEPT {
    // Figure out if we are entering or exiting
    const bool entering = Dot(isect.geometry_frame.n(), isect.wo) > 0;
    // Set normal accordingly
    const Vector3<Real> normal = entering ? isect.geometry_frame.n() : -isect.geometry_frame.n();
    // Set refraction coefficients
    const Real eta_i = entering ? eta_a.Evaluate(isect) : eta_b.Evaluate(isect);
    const Real eta_t = entering ? eta_b.Evaluate(isect) : eta_a.Evaluate(isect);

    // Check if there is refraction
    Vector3<Real> refraction_dir;
    const bool refraction = Refract(isect.wo, normal, eta_i / eta_t, refraction_dir);

    // If we have no refraction , we just sample specular direction
    if (!refraction) {
        material_sample.wi = Reflect(isect.wo, normal);
        material_sample.pdf = 1;
        material_sample.sampled_type = MaterialType(M_REFLECTION | M_SPECULAR);

        return reflection.Evaluate(isect);
    }

    // Sample reflected direction
    const Vector3<Real> reflected_dir = Reflect(isect.wo, normal);
    // Compute Fresnel value for reflected direction
    const Spectrum<Real> fr_value = fresnel.Evaluate(Dot(reflected_dir, normal), eta_i, eta_t);
    // Depending on the value of Fresnel, either sample reflection or refraction
    const Real fr_avg = (fr_value.r() + fr_value.g() + fr_value.b()) / 3;
    if (u[0] < fr_avg) {
        // Sample reflected direction
        material_sample.wi = reflected_dir;
        material_sample.pdf = fr_avg;
        material_sample.sampled_type = MaterialType(M_REFLECTION | M_SPECULAR);

        return reflection.Evaluate(isect) * fr_value;
    } else {
        // Sample refraction direction
        material_sample.wi = refraction_dir;
        material_sample.pdf = 1 - fr_avg;
        material_sample.sampled_type = MaterialType(M_TRANSMISSION | M_SPECULAR);

        return (eta_i * eta_i) / (eta_t * eta_t) * transmission.Evaluate(isect) *
               (Spectrum<Real>(1) - fresnel.Evaluate(Dot(material_sample.wi, -normal), eta_i, eta_t));
    }
}

Real DielectricMaterial::Pdf(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return 0;
}
