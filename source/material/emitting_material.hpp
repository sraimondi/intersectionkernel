#ifndef INTERSECTIONKERNEL_EMITTING_MATERIAL_HPP
#define INTERSECTIONKERNEL_EMITTING_MATERIAL_HPP

#include "material.hpp"
#include "texture.hpp"

// Diffuse emitting material
class DiffuseEmittingMaterial : public MaterialInterface {
public:
    CXX_CONSTEXPR explicit DiffuseEmittingMaterial(const TextureInterface<Spectrum<Real>>& e) CXX_NOEXCEPT
            : MaterialInterface(M_EMISSION), emission_per_area(e) {}

    Spectrum<Real> Emission(const Intersection& isect, const Vector3<Real>& wo) const CXX_NOEXCEPT override;

    Spectrum<Real> F(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

    Spectrum<Real> SampleF(const Intersection& isect, const Vector2<Real>& u,
                           MaterialSample& material_sample) const CXX_NOEXCEPT override;

    Real Pdf(const Intersection& isect, const Vector3<Real>& wi) const CXX_NOEXCEPT override;

private:
    // Emission value per-area element
    const TextureInterface<Spectrum<Real>>& emission_per_area;
};

#endif //INTERSECTIONKERNEL_EMITTING_MATERIAL_HPP
