#include "emitting_material.hpp"

Spectrum<Real> DiffuseEmittingMaterial::Emission(const Intersection& isect, const Vector3<Real>& w) const CXX_NOEXCEPT {
    if (SameHemisphere(isect.geometry_frame.n(), w)) {
        const auto e_at_isect = emission_per_area.Evaluate(isect);
        if (!e_at_isect.IsBlack()) {
            return INV_PI * e_at_isect;
        }
    }

    return Spectrum<Real>(0);
}

Spectrum<Real> DiffuseEmittingMaterial::F(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return Spectrum<Real>(0);
}

Spectrum<Real> DiffuseEmittingMaterial::SampleF(const Intersection&, const Vector2<Real>&,
                                                MaterialSample& material_sample) const CXX_NOEXCEPT {
    material_sample.pdf = 0;
    return Spectrum<Real>(0);
}

Real DiffuseEmittingMaterial::Pdf(const Intersection&, const Vector3<Real>&) const CXX_NOEXCEPT {
    return 0;
}
