#include "mesh_loader.hpp"

#define TINYPLY_IMPLEMENTATION
#define TINYOBJLOADER_IMPLEMENTATION

#include "tinyply.hpp"
#include "tiny_obj_loader.hpp"

#include <fstream>

MeshLoaderException::MeshLoaderException(const std::string& err)
        : std::runtime_error(err) {}

Mesh MeshLoader::LoadFromFile(const std::string& file_name, bool load_normals, bool load_uvs) {
    const std::string extension = file_name.substr(file_name.find_last_of('.'));
    if (extension == ".ply") {
        return LoadFromPLY(file_name, load_normals, load_uvs);
    } else if (extension == ".obj") {
        return LoadFromOBJ(file_name, load_normals, load_uvs);
    } else {
        throw MeshLoaderException("Unrecognized file format: " + extension);
    }
}

Mesh MeshLoader::LoadFromPLY(const std::string& file_name, bool load_normals, bool load_uvs) {
    using namespace tinyply;
    // Open file stream
    std::ifstream file(file_name, std::ios::binary);
    if (!file.is_open()) {
        throw MeshLoaderException("Could not open file " + file_name);
    }

    try {
        PlyFile ply_file;
        ply_file.parse_header(file);

        // Print header of the file
        std::cout << "........................................................................\n";
        for (const auto& c : ply_file.get_comments()) { std::cout << "Comment: " << c << "\n"; }
        for (const auto& e : ply_file.get_elements()) {
            std::cout << "element - " << e.name << " (" << e.size << ")\n";
            for (const auto& p : e.properties) {
                std::cout << "\tproperty - " << p.name << " (" << tinyply::PropertyTable[p.propertyType].str << ")\n";
            }
        }
        std::cout << "........................................................................\n";

        // Loaded data pointers
        std::shared_ptr<PlyData> ply_vertices, ply_normals, ply_uvs, ply_faces;

        // Set data for reading
        ply_vertices = ply_file.request_properties_from_element("vertex", {"x", "y", "z"});
        if (load_normals) {
            ply_normals = ply_file.request_properties_from_element("vertex", {"nx", "ny", "nz"});
        }
        if (load_uvs) {
            ply_uvs = ply_file.request_properties_from_element("vertex", {"s", "t"});
        }
        ply_faces = ply_file.request_properties_from_element("face", {"vertex_indices"}, 3);

        // Do the reading
        ply_file.read(file);

        std::cout << "Loaded " << ply_vertices->count << " vertices and " << ply_faces->count << " faces from file "
                  << file_name << "\n";

        std::vector<Vector3<Real>> vertices(ply_vertices->count);
        std::vector<Vector3<Real>> normals;
        std::vector<Vector2<Real>> uvs;
        std::vector<int> indices(3 * ply_faces->count);

        // Resize and copy data into vectors
        std::memcpy(vertices.data(), ply_vertices->buffer.get(), ply_vertices->buffer.size_bytes());
        std::memcpy(indices.data(), ply_faces->buffer.get(), ply_faces->buffer.size_bytes());

        if (load_normals) {
            normals.resize(ply_normals->count);
            std::memcpy(normals.data(), ply_normals->buffer.get(), ply_normals->buffer.size_bytes());
        } else {
            normals = ComputeSmoothNormals(vertices, indices);
        }
        if (load_uvs) {
            uvs.resize(ply_uvs->count);
            std::memcpy(uvs.data(), ply_uvs->buffer.get(), ply_uvs->buffer.size_bytes());
        }

        return {std::move(vertices), std::move(normals), std::move(uvs), std::move(indices)};
    } catch (const std::exception& ex) {
        throw MeshLoaderException(std::string("Caught tinyply exception: ") + ex.what());
    }
}

Mesh MeshLoader::LoadFromOBJ(const std::string& file_name, bool load_normals, bool load_uvs) {
    // Data for TinyObj
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;

    // Read mesh
    std::string err;
    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, &err, file_name.c_str());
    if (!ret || !err.empty()) {
        throw MeshLoaderException(std::string("Error while reading " + file_name + ": " + err));
    }

    if (shapes.size() > 1) {
        std::cerr << "Multiple shapes in Mesh\n";
    }

    // Loaded mesh description
    std::vector<Vector3<Real>> vertices(attrib.vertices.size() / 3);
    std::vector<Vector3<Real>> normals;
    if (load_normals) {
        normals.resize(vertices.size());
    }
    std::vector<Vector2<Real>> uvs;
    if (load_uvs) {
        uvs.resize(vertices.size());
    }
    std::vector<int> indices;

    // Copy vertices
    for (std::size_t v = 0; v < vertices.size(); ++v) {
        vertices[v] = Vector3<Real>(attrib.vertices[3 * v], attrib.vertices[3 * v + 1], attrib.vertices[3 * v + 2]);
    }

    // Now copy indices and normals + uvs, if required
    std::size_t index_offset = 0;
    for (std::size_t f = 0; f < shapes[0].mesh.num_face_vertices.size(); ++f) {
        std::size_t fv = shapes[0].mesh.num_face_vertices[f];
        // Loop over vertices of the face
        for (std::size_t v = 0; v < fv; ++v) {
            const tinyobj::index_t& idx = shapes[0].mesh.indices[index_offset + v];
            // Add indices
            indices.push_back(idx.vertex_index);
            // Add normal
            if (load_normals) {
                normals[idx.vertex_index] = Vector3<Real>(attrib.normals[3 * idx.normal_index],
                                                          attrib.normals[3 * idx.normal_index + 1],
                                                          attrib.normals[3 * idx.normal_index + 2]);
            }
            if (load_uvs) {
                uvs[idx.vertex_index] = Vector2<Real>(attrib.texcoords[2 * idx.texcoord_index],
                                                      attrib.texcoords[2 * idx.texcoord_index + 1]);
            }
        }
        index_offset += fv;
    }

    std::cout << "Loaded " << vertices.size() << " vertices and " << indices.size() / 3 << " faces from file "
              << file_name << "\n";

    // If normals were not loaded, compute them
    if (!load_normals) {
        normals = ComputeSmoothNormals(vertices, indices);
    }

    return {std::move(vertices), std::move(normals), std::move(uvs), std::move(indices)};
}

std::vector<Vector3<Real>>
MeshLoader::ComputeSmoothNormals(const std::vector<Vector3<Real>>& vertices, const std::vector<int>& indices) {
    std::vector<Vector3<Real>> normals(vertices.size());
    // Loop over all faces, compute normal for the face and add it to the normal for that vertex
    for (std::size_t f = 0; f < indices.size() / 3; ++f) {
        const std::size_t face_index = 3 * f;
        // Get reference to the vertices of the face
        const auto& v0 = vertices[indices[face_index]];
        const auto& v1 = vertices[indices[face_index + 1]];
        const auto& v2 = vertices[indices[face_index + 2]];
        const auto normal = Normalize(Cross(v1 - v0, v2 - v0));

        // Accumulate normal per-vertex
        normals[indices[face_index]] += normal;
        normals[indices[face_index + 1]] += normal;
        normals[indices[face_index + 2]] += normal;
    }

    // Normalize
    std::for_each(normals.begin(), normals.end(), [](Vector3<Real>& n) {
        n = Normalize(n);
    });

    return normals;
}
