#ifndef INTERSECTIONKERNEL_RAY_HPP
#define INTERSECTIONKERNEL_RAY_HPP

#include "lin_algebra.hpp"

// Ray class
class Ray {
public:
    // Constructors
    CXX_CONSTEXPR Ray() = default;

    CXX_CONSTEXPR Ray(const Vector3<Real>& o, const Vector3<Real>& d) CXX_NOEXCEPT
            : origin(o), direction(d) {}

    CXX_CONSTEXPR Vector3<Real> operator()(Real t) const CXX_NOEXCEPT {
        return origin + t * direction;
    }

    CXX17_CONSTEXPR const Vector3<Real>& Origin() const CXX_NOEXCEPT {
        return origin;
    }

    CXX17_CONSTEXPR const Vector3<Real>& Direction() const CXX_NOEXCEPT {
        return direction;
    }

    // Transform ray by given matrix
    CXX17_CONSTEXPR Ray Transform(const Matrix4x4<Real>& m) const CXX_NOEXCEPT {
        return {m.TransformPosition(origin), m.TransformDirection(direction)};
    }

private:
    // Ray components
    Vector3<Real> origin, direction;
};

CXX_INLINE std::ostream& operator<<(std::ostream& os, const Ray ray) {
    os << "[origin] " << ray.Origin() << " [direction] " << ray.Direction();
    return os;
}

#endif //INTERSECTIONKERNEL_RAY_HPP
