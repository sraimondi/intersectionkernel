#ifndef INTERSECTION_KERNEL_BOUNDS_HPP
#define INTERSECTION_KERNEL_BOUNDS_HPP

#include "lin_algebra.hpp"

#include <limits>
#include <cassert>
#include <cstring>
#include <array>

// Bounds class
class Bounds {
public:
    // Constructors
    CXX17_CONSTEXPR Bounds() CXX_NOEXCEPT {
        bounds[0] = Vector3<Real>(std::numeric_limits<Real>::max());
        bounds[1] = Vector3<Real>(std::numeric_limits<Real>::lowest());
    }

    CXX_CONSTEXPR explicit Bounds(const Vector3<Real>& p) CXX_NOEXCEPT
            : bounds({p, p}) {}

    CXX14_CONSTEXPR Bounds(const Vector3<Real>& p1, const Vector3<Real>& p2) CXX_NOEXCEPT
            : bounds({Min(p1, p2), Max(p1, p2)}) {}

    CXX17_CONSTEXPR Bounds(const Bounds& b1, const Bounds& b2) CXX_NOEXCEPT
            : bounds({Min(b1.bounds[0], b2.bounds[0]), Max(b1.bounds[1], b2.bounds[1])}) {}

    CXX_INLINE const Vector3<Real>& operator[](std::size_t i) const CXX_NOEXCEPT {
        assert(i == 0 || i == 1);
        return bounds[i];
    }

    CXX17_CONSTEXPR Vector3<Real> Diagonal() const CXX_NOEXCEPT {
        return bounds[1] - bounds[0];
    }

    CXX17_CONSTEXPR Axis MaxExtent() const CXX_NOEXCEPT {
        return MaxDimension(Diagonal());
    }

    CXX17_CONSTEXPR Vector3<Real> Centroid() const CXX_NOEXCEPT {
        return 0.5 * (bounds[0] + bounds[1]);
    }

    CXX17_CONSTEXPR Real area() const CXX_NOEXCEPT {
        const auto diagonal = Diagonal();
        return 2 * (diagonal.x() * diagonal.y() + diagonal.x() * diagonal.z() + diagonal.y() * diagonal.z());
    }

    CXX17_CONSTEXPR void Extend(const Vector3<Real>& p) CXX_NOEXCEPT {
        bounds[0] = Min(bounds[0], p);
        bounds[1] = Max(bounds[1], p);
    }

    CXX17_CONSTEXPR void Extend(const Bounds& b) CXX_NOEXCEPT {
        bounds[0] = Min(bounds[0], b.bounds[0]);
        bounds[1] = Max(bounds[1], b.bounds[1]);
    }

    // Intersect ray with Bounds
    CXX17_CONSTEXPR bool
    Intersect(const Vector3<Real>& origin, const Vector3<Real>& inv_dir, const std::array<bool, 3>& dir_is_neg,
              Real max_t) const CXX_NOEXCEPT {
        // Compute intersection with the x and y slab
        Real t_min = (bounds[dir_is_neg[0]].x() - origin.x()) * inv_dir.x();
        Real t_max = (bounds[1 - dir_is_neg[0]].x() - origin.x()) * inv_dir.x();
        const Real ty_min = (bounds[dir_is_neg[1]].y() - origin.y()) * inv_dir.y();
        const Real ty_max = (bounds[1 - dir_is_neg[1]].y() - origin.y()) * inv_dir.y();

        // Check for early out
        if (t_min > ty_max || ty_min > t_max) { return false; }
        // Update interval
        if (ty_min > t_min) { t_min = ty_min; }
        if (ty_max < t_max) { t_max = ty_max; }

        // Compute intersection with z slab
        const Real tz_min = (bounds[dir_is_neg[2]].z() - origin.z()) * inv_dir.z();
        const Real tz_max = (bounds[1 - dir_is_neg[2]].z() - origin.z()) * inv_dir.z();

        // Check for intersection condition
        if (t_min > tz_max || tz_min > t_max) { return false; }
        // Update interval
        if (tz_min > t_min) { t_min = tz_min; }
        if (tz_max < t_max) { t_max = tz_max; }

        return (t_min < max_t) && (t_max > 0);
    }

private:
    // Bounds limits
    std::array<Vector3<Real>, 2> bounds;
};

#endif // INTERSECTION_KERNEL_BOUNDS_HPP

