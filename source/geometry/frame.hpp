#ifndef INTERSECTIONKERNEL_FRAME_HPP
#define INTERSECTIONKERNEL_FRAME_HPP

#include "lin_algebra.hpp"

// Local frame orientation description class
class Frame {
public:
    // Constructors
    CXX_CONSTEXPR Frame() CXX_NOEXCEPT
            : s_(1, 0, 0), n_(0, 1, 0), t_(0, 0, 1) {}

    CXX_INLINE explicit Frame(const Vector3<Real>& n) CXX_NOEXCEPT
            : n_(n) {
        if (std::abs(n.x()) > std::abs(n.y())) {
            s_ = Vector3<Real>(-n.z(), 0, n.x()) / std::sqrt(n.x() * n.x() + n.z() * n.z());
        } else {
            s_ = Vector3<Real>(0, n.z(), -n.y()) / std::sqrt(n.y() * n.y() + n.z() * n.z());
        }

        t_ = Cross(s_, n_);
    }

    CXX_CONSTEXPR Frame(const Vector3<Real>& s, const Vector3<Real>& n, const Vector3<Real>& t) CXX_NOEXCEPT
            : s_(s), n_(n), t_(t) {}

    // Access basis vector
    CXX_CONSTEXPR const Vector3<Real>& s() const CXX_NOEXCEPT {
        return s_;
    }

    CXX_CONSTEXPR const Vector3<Real>& n() const CXX_NOEXCEPT {
        return n_;
    }

    CXX_CONSTEXPR const Vector3<Real>& t() const CXX_NOEXCEPT {
        return t_;
    }

    // Transform to local space
    CXX_CONSTEXPR Vector3<Real> ToLocal(const Vector3<Real>& v) const CXX_NOEXCEPT {
        return {Dot(v, s()), Dot(v, n()), Dot(v, t())};
    }

    CXX_CONSTEXPR Vector3<Real> ToWorld(const Vector3<Real>& v) const CXX_NOEXCEPT {
        return v.x() * s() + v.y() * n() + v.z() * t();
    }

    // Transform frame given matrices
    CXX_INLINE Frame Transform(const Matrix4x4<Real>& m, const Matrix4x4<Real>& m_inv) const CXX_NOEXCEPT {
        return {Normalize(m.TransformDirection(s())),
                Normalize(m_inv.TransformNormal(n())),
                Normalize(m.TransformDirection(t()))};
    }

    // Transform in place
    CXX_INLINE void TransformInPlace(const Matrix4x4<Real>& m, const Matrix4x4<Real>& m_inv) CXX_NOEXCEPT {
        s_ = Normalize(m.TransformDirection(s()));
        n_ = Normalize(m_inv.TransformNormal(n()));
        t_ = Normalize(m.TransformDirection(t()));
    }

    // Static utility to compute some common values in local space
    static CXX_CONSTEXPR Real CosTheta(const Vector3<Real>& v) CXX_NOEXCEPT {
        return v.y();
    }

    static CXX_CONSTEXPR Real Cos2Theta(const Vector3<Real>& v) CXX_NOEXCEPT {
        return v.y() * v.y();
    }

    static CXX_INLINE Real AbsCosTheta(const Vector3<Real>& v) CXX_NOEXCEPT {
        return std::abs(CosTheta(v));
    }

    static
#if CXX_VERSION >= CXX_14
    CXX_CONSTEXPR
#else
    CXX_INLINE
#endif
    Real Sin2Theta(const Vector3<Real>& v) CXX_NOEXCEPT {
        return std::max(Real(0), 1 - Frame::Cos2Theta(v));
    }

    static CXX_INLINE Real SinTheta(const Vector3<Real>& v) CXX_NOEXCEPT {
        return std::sqrt(Sin2Theta(v));
    }

    static CXX_INLINE Real TanTheta(const Vector3<Real>& v) CXX_NOEXCEPT {
        return SinTheta(v) / CosTheta(v);
    }

    static CXX_INLINE Real Tan2Theta(const Vector3<Real>& v) CXX_NOEXCEPT {
        return Sin2Theta(v) / Cos2Theta(v);
    }

    static CXX_INLINE Real CosPhi(const Vector3<Real>& v) CXX_NOEXCEPT {
        const Real sin_theta = SinTheta(v);
        return (ApproxEqual(sin_theta, Real(0)) ? 1 : Clamp(v.x() / sin_theta, Real(-1), Real(1)));
    }

    static CXX_INLINE Real SinPhi(const Vector3<Real>& v) CXX_NOEXCEPT {
        const Real sin_theta = SinTheta(v);
        return (ApproxEqual(sin_theta, Real(0)) ? 0 : Clamp(v.z() / sin_theta, Real(-1), Real(1)));
    }

    static CXX_INLINE Real Cos2Phi(const Vector3<Real>& v) CXX_NOEXCEPT {
        const Real cos_phi = CosPhi(v);
        return cos_phi * cos_phi;
    }

    static CXX_INLINE Real Sin2Phi(const Vector3<Real>& v) CXX_NOEXCEPT {
        const Real sin_phi = SinPhi(v);
        return sin_phi * sin_phi;
    }

    static CXX_CONSTEXPR bool SameHemisphere(const Vector3<Real>& v, const Vector3<Real>& w) CXX_NOEXCEPT {
        return v.y() * w.y() > Real(0);
    }

    static CXX_CONSTEXPR Vector3<Real> Reflect(const Vector3<Real>& v) CXX_NOEXCEPT {
        return {-v.x(), v.y(), -v.z()};
    }

private:
    // Local base
    Vector3<Real> s_, n_, t_;
};

#endif //INTERSECTIONKERNEL_FRAME_HPP
