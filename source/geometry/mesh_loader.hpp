#ifndef INTERSECTIONKERNEL_MESH_LOADER_HPP
#define INTERSECTIONKERNEL_MESH_LOADER_HPP

#include "mesh.hpp"

#include <exception>

// Custom exception for Mesh errors
class MeshLoaderException : public std::runtime_error {
public:
    explicit MeshLoaderException(const std::string& err);
};

class MeshLoader {
public:
    // Load mesh from a given file
    static Mesh LoadFromFile(const std::string& file_name, bool load_normals = false, bool load_uvs = false);

private:
    static Mesh LoadFromPLY(const std::string& file_name, bool load_normals = false, bool load_uvs = false);

    static Mesh LoadFromOBJ(const std::string& file_name, bool load_normals = false, bool load_uvs = false);

    // Compute smoothed normals for a given set of
    static std::vector<Vector3<Real>>
    ComputeSmoothNormals(const std::vector<Vector3<Real>>& vertices, const std::vector<int>& indices);
};

#endif //INTERSECTIONKERNEL_MESH_LOADER_HPP
