#ifndef INTERSECTION_KERNEL_INTERSECTION_HPP
#define INTERSECTION_KERNEL_INTERSECTION_HPP

#include "frame.hpp"
#include "ray.hpp"
#include "common.hpp"

// Forward declare triangle class and AreaLight classes
class ShapeInstance;

struct Intersection {
    // Default constructor
    CXX_CONSTEXPR Intersection() CXX_NOEXCEPT
            : t(INF), hit_shape(nullptr) {}

    CXX_INLINE Intersection(const Vector3<Real>& p,
                            const Vector3<Real>& n,
                            const Vector2<Real>& uv,
                            const Vector3<Real>& wo, Real t,
                            const ShapeInstance* s = nullptr) CXX_NOEXCEPT
            : hit_p(p), geometry_frame(n), uv(uv), wo(wo), t(t), hit_shape(s) {}

    CXX_CONSTEXPR Intersection(const Vector3<Real>& p,
                               const Frame& frame,
                               const Vector2<Real>& uv,
                               const Vector3<Real>& wo, Real t,
                               const ShapeInstance* s = nullptr) CXX_NOEXCEPT
            : hit_p(p), geometry_frame(frame), uv(uv), wo(wo), t(t), hit_shape(s) {}

    // Returns true if the Intersection is a valid hit
    CXX_CONSTEXPR bool Hit() const CXX_NOEXCEPT {
        return hit_shape != nullptr;
    }

    CXX_INLINE Intersection Transform(const Matrix4x4<Real>& m, const Matrix4x4<Real>& m_inv) const CXX_NOEXCEPT {
        return {m.TransformPosition(hit_p),
                geometry_frame.Transform(m, m_inv),
                uv,
                Normalize(m.TransformDirection(wo)),
                t,
                hit_shape};
    }

    CXX_INLINE void TransformInPlace(const Matrix4x4<Real>& m, const Matrix4x4<Real>& m_inv) CXX_NOEXCEPT {
        hit_p = m.TransformPosition(hit_p);
        geometry_frame.TransformInPlace(m, m_inv);
        wo = Normalize(m.TransformDirection(wo));
    }

    // Spawn ray in given direction
    CXX_CONSTEXPR Ray SpawnRay(const Vector3<Real>& dir) const CXX_NOEXCEPT {
        return {hit_p + EPS * dir, dir};
    }

    // Intersection point
    Vector3<Real> hit_p;
    // Geometric reference frame
    Frame geometry_frame;
    // UV coordinates
    Vector2<Real> uv;
    // Outgoing direction
    Vector3<Real> wo;
    // Intersection parameter
    Real t;
    // Pointer to Triangle hit
    const ShapeInstance* hit_shape;
};

#endif // INTERSECTION_KERNEL_INTERSECTION_HPP

