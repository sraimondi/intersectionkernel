#ifndef INTERSECTIONKERNEL_TRANSFORM_HPP
#define INTERSECTIONKERNEL_TRANSFORM_HPP

#include "intersection.hpp"
#include "ray.hpp"

// Define a set of function to create common transformation matrices
template <typename T>
CXX17_CONSTEXPR Matrix4x4<T> Translate(const T& tx, const T& ty, const T& tz) CXX_NOEXCEPT {
    return {1, 0, 0, tx,
            0, 1, 0, ty,
            0, 0, 1, tz,
            0, 0, 0, 1};
}

template <typename T>
CXX17_CONSTEXPR Matrix4x4<T> Translate(const Vector3<T>& t) CXX_NOEXCEPT {
    return Translate(t.x(), t.y(), t.z());
}

template <typename T>
CXX17_CONSTEXPR Matrix4x4<T> Scale(const T& sx, const T& sy, const T& sz) CXX_NOEXCEPT {
    return {sx, 0, 0, 0,
            0, sy, 0, 0,
            0, 0, sz, 0,
            0, 0, 0, 1};
}

template <typename T>
CXX17_CONSTEXPR Matrix4x4<T> Scale(const T& s) CXX_NOEXCEPT {
    return Scale(s, s, s);
}

template <typename T>
CXX17_CONSTEXPR Matrix4x4<T> Scale(const Vector3<T>& s) CXX_NOEXCEPT {
    return Scale(s.x(), s.y(), s.z());
}

template <typename T>
CXX_INLINE Matrix4x4<T> RotateX(const T& angle) CXX_NOEXCEPT {
    const T sin_angle = std::sin(DegToRad(angle));
    const T cos_angle = std::cos(DegToRad(angle));

    return {1, 0, 0, 0,
            0, cos_angle, -sin_angle, 0,
            0, sin_angle, cos_angle, 0,
            0, 0, 0, 1};
}

template <typename T>
CXX_INLINE Matrix4x4<T> RotateY(const T& angle) CXX_NOEXCEPT {
    const T sin_angle = std::sin(DegToRad(angle));
    const T cos_angle = std::cos(DegToRad(angle));

    return {cos_angle, 0, sin_angle, 0,
            0, 1, 0, 0,
            -sin_angle, 0, cos_angle, 0,
            0, 0, 0, 1};
}

template <typename T>
CXX_INLINE Matrix4x4<T> RotateZ(const T& angle) CXX_NOEXCEPT {
    const T sin_angle = std::sin(DegToRad(angle));
    const T cos_angle = std::cos(DegToRad(angle));

    return {cos_angle, -sin_angle, 0, 0,
            sin_angle, cos_angle, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1};
}

template <typename T>
CXX_INLINE Matrix4x4<T> Rotate(const Vector3<T>& axis, const T& angle) {
    if (!ApproxEqual(Length(axis), T(1))) {
        std::cerr << "Axis given to rotation matrix is not normalized\n";
        return Matrix4x4<T>::Identity();
    } else {
        const T sin_angle = std::sin(DegToRad(angle));
        const T cos_angle = std::cos(DegToRad(angle));
        const T one_minus_cos = 1 - cos_angle;

        return {cos_angle + axis.x() * axis.x() * one_minus_cos,
                axis.x() * axis.y() * one_minus_cos - axis.z() * sin_angle,
                axis.x() * axis.z() * one_minus_cos + axis.y() * sin_angle,
                0, // First row
                axis.y() * axis.x() * one_minus_cos + axis.z() * sin_angle,
                cos_angle + axis.y() * axis.y() * one_minus_cos,
                axis.y() * axis.z() * one_minus_cos - axis.x() * sin_angle,
                0, // Second row
                axis.z() * axis.x() * one_minus_cos - axis.y() * sin_angle,
                axis.z() * axis.y() * one_minus_cos + axis.x() * sin_angle,
                cos_angle + axis.z() * axis.z() * one_minus_cos,
                0, // Third row
                0, 0, 0, 1};
    }
}

template <typename T>
CXX_INLINE Matrix4x4<T> LookAt(const Vector3<T>& eye, const Vector3<T>& at, const Vector3<T>& up) CXX_NOEXCEPT {
    const auto w = Normalize(eye - at);
    const auto u = Normalize(Cross(up, w));
    const auto v = Cross(w, u);

    return {u.x(), v.x(), w.x(), eye.x(),
            u.y(), v.y(), w.y(), eye.y(),
            u.z(), v.z(), w.z(), eye.z(),
            0, 0, 0, 1};
}

#endif //INTERSECTIONKERNEL_TRANSFORM_HPP
