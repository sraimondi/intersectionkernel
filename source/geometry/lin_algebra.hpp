#ifndef INTERSECTION_KERNEL_LIN_ALGEBRA_HPP
#define INTERSECTION_KERNEL_LIN_ALGEBRA_HPP

#include "common.hpp"

#include <algorithm>
#include <ostream>
#include <array>
#include <cmath>
#include <cassert>
#include <cstring>
#include <iostream>

// Axis class
enum class Axis {
    X = 0,
    Y = 1,
    Z = 2
};

// Small 2D Vector class
template <typename T>
class Vector2 {
public:
    // Member types
    using value_type = T;
    using size_type = int;
    using reference = value_type&;
    using const_reference = const value_type&;

    // Constructors
    CXX_CONSTEXPR Vector2() CXX_NOEXCEPT
            : Vector2(0) {}

    CXX_CONSTEXPR explicit Vector2(const value_type& v) CXX_NOEXCEPT
            : Vector2(v, v) {}

    CXX_CONSTEXPR Vector2(const value_type& x, const value_type& y) CXX_NOEXCEPT
            : elements({x, y}) {}

    // Access operator
    CXX14_CONSTEXPR value_type operator[](size_type i) const CXX_NOEXCEPT {
        assert(i >= 0 && i < 2);
        return elements[i];
    }

    CXX17_CONSTEXPR reference operator[](size_type i) CXX_NOEXCEPT {
        assert(i >= 0 && i < 2);
        return elements[i];
    }

    // Access elements by name
    CXX_CONSTEXPR value_type x() const CXX_NOEXCEPT {
        return elements[0];
    }

    CXX_CONSTEXPR value_type y() const CXX_NOEXCEPT {
        return elements[1];
    }

    CXX_CONSTEXPR value_type s() const CXX_NOEXCEPT {
        return elements[0];
    }

    CXX_CONSTEXPR value_type t() const CXX_NOEXCEPT {
        return elements[1];
    }

    CXX17_CONSTEXPR reference x() CXX_NOEXCEPT {
        return elements[0];
    }

    CXX17_CONSTEXPR reference y() CXX_NOEXCEPT {
        return elements[1];
    }

    CXX17_CONSTEXPR reference s() CXX_NOEXCEPT {
        return elements[0];
    }

    CXX17_CONSTEXPR reference t() CXX_NOEXCEPT {
        return elements[1];
    }

    // Math operators
    CXX_CONSTEXPR Vector2 operator+(const Vector2& v) const CXX_NOEXCEPT {
        return {x() + v.x(), y() + v.y()};
    }

    CXX14_CONSTEXPR Vector2& operator+=(const Vector2& v) CXX_NOEXCEPT {
        x() += v.x;
        y() += v.y;
        return *this;
    }

    CXX_CONSTEXPR Vector2 operator-() const CXX_NOEXCEPT {
        return {-x(), -y()};
    }

    CXX_CONSTEXPR Vector2 operator-(const Vector2& v) const CXX_NOEXCEPT {
        return {x() - v.x(), y() - v.y()};
    }

    CXX14_CONSTEXPR Vector2& operator-=(const Vector2& v) CXX_NOEXCEPT {
        x() -= v.x();
        y() -= v.y();
        return *this;
    }

    CXX_CONSTEXPR Vector2 operator*(const Vector2& v) const CXX_NOEXCEPT {
        return {x() * v.x(), y() * v.y()};
    }

    CXX_CONSTEXPR Vector2 operator*(const value_type& t) const CXX_NOEXCEPT {
        return {t * x(), t * y()};
    }

    CXX14_CONSTEXPR Vector2& operator*=(const value_type& t) CXX_NOEXCEPT {
        x() *= t;
        y() *= t;
        return *this;
    }

    CXX_CONSTEXPR Vector2 operator/(const Vector2& v) const CXX_NOEXCEPT {
        return {x() / v.x(), y() / v.y()};
    }

    CXX14_CONSTEXPR Vector2 operator/(const value_type& t) const CXX_NOEXCEPT {
        const value_type inv_t = value_type(1) / t;
        return *this * inv_t;
    }

    CXX14_CONSTEXPR Vector2& operator/=(const value_type& t) CXX_NOEXCEPT {
        const value_type inv_t = value_type(1) / t;
        return (*this *= inv_t);
    }

private:
    // Components
    std::array<value_type, 2> elements;
};

template <typename T>
CXX_CONSTEXPR Vector2<T> operator*(const typename Vector2<T>::value_type& t, const Vector2<T>& v) CXX_NOEXCEPT {
    return {t * v.x(), t * v.y()};
}

// Comparison operators
template <typename T>
CXX_CONSTEXPR bool operator<(const Vector2<T>& v1, const Vector2<T>& v2) CXX_NOEXCEPT {
    return v1.x() < v2.x() && v1.y() < v2.y();
}

template <typename T>
CXX_CONSTEXPR bool operator<=(const Vector2<T>& v1, const Vector2<T>& v2) CXX_NOEXCEPT {
    return v1.x() <= v2.x() && v1.y() <= v2.y();
}

template <typename T>
CXX_CONSTEXPR bool operator>(const Vector2<T>& v1, const Vector2<T>& v2) CXX_NOEXCEPT {
    return v1.x() > v2.x() && v1.y() > v2.y();
}

template <typename T>
CXX_CONSTEXPR bool operator>=(const Vector2<T>& v1, const Vector2<T>& v2) CXX_NOEXCEPT {
    return v1.x() >= v2.x() && v1.y() >= v2.y();
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Vector2<T>& v) {
    os << v.x() << ", " << v.y();
    return os;
}

// 3D Vector class
template <typename T>
class Vector3 {
public:
    // Member types
    using value_type = T;
    using size_type = int;
    using reference = value_type&;
    using const_reference = const value_type&;

    // Constructors
    CXX_CONSTEXPR Vector3() CXX_NOEXCEPT
            : Vector3(0) {}

    CXX_CONSTEXPR explicit Vector3(const value_type& v) CXX_NOEXCEPT
            : Vector3(v, v, v) {}

    CXX_CONSTEXPR Vector3(const Vector2<T>& v, const value_type& z) CXX_NOEXCEPT
            : Vector3(v.x(), v.y(), z) {}

    CXX_CONSTEXPR Vector3(const value_type& x, const value_type& y, const value_type& z) CXX_NOEXCEPT
            : elements({x, y, z}) {}

    // Access operator
    CXX14_CONSTEXPR value_type operator[](size_type i) const CXX_NOEXCEPT {
        assert(i >= 0 && i < 3);
        return elements[i];
    }

    CXX17_CONSTEXPR reference operator[](size_type i) CXX_NOEXCEPT {
        assert(i >= 0 && i < 3);
        return elements[i];
    }

    // Access elements by name
    CXX_CONSTEXPR value_type x() const CXX_NOEXCEPT {
        return elements[0];
    }

    CXX_CONSTEXPR value_type y() const CXX_NOEXCEPT {
        return elements[1];
    }

    CXX_CONSTEXPR value_type z() const CXX_NOEXCEPT {
        return elements[2];
    }

    CXX_CONSTEXPR value_type s() const CXX_NOEXCEPT {
        return elements[0];
    }

    CXX_CONSTEXPR value_type t() const CXX_NOEXCEPT {
        return elements[1];
    }

    CXX_CONSTEXPR value_type p() const CXX_NOEXCEPT {
        return elements[2];
    }

    CXX17_CONSTEXPR reference x() CXX_NOEXCEPT {
        return elements[0];
    }

    CXX17_CONSTEXPR reference y() CXX_NOEXCEPT {
        return elements[1];
    }

    CXX17_CONSTEXPR reference z() CXX_NOEXCEPT {
        return elements[2];
    }

    CXX17_CONSTEXPR reference s() CXX_NOEXCEPT {
        return elements[0];
    }

    CXX17_CONSTEXPR reference t() CXX_NOEXCEPT {
        return elements[1];
    }

    CXX17_CONSTEXPR reference p() CXX_NOEXCEPT {
        return elements[2];
    }

    // Math operators
    CXX_CONSTEXPR Vector3 operator+(const Vector3& v) const CXX_NOEXCEPT {
        return {x() + v.x(), y() + v.y(), z() + v.z()};
    }

    CXX14_CONSTEXPR Vector3& operator+=(const Vector3& v) CXX_NOEXCEPT {
        x() += v.x();
        y() += v.y();
        z() += v.z();
        return *this;
    }

    CXX_CONSTEXPR Vector3 operator-() const CXX_NOEXCEPT {
        return {-x(), -y(), -z()};
    }

    CXX_CONSTEXPR Vector3 operator-(const Vector3& v) const CXX_NOEXCEPT {
        return {x() - v.x(), y() - v.y(), z() - v.z()};
    }

    CXX14_CONSTEXPR Vector3& operator-=(const Vector3& v) CXX_NOEXCEPT {
        x() -= v.x();
        y() -= v.y();
        z() -= v.z();
        return *this;
    }

    CXX_CONSTEXPR Vector3 operator*(const Vector3& v) const CXX_NOEXCEPT {
        return {x() * v.x(), y() * v.y(), z() * v.z()};
    }

    CXX_CONSTEXPR Vector3 operator*(const value_type& t) const CXX_NOEXCEPT {
        return {t * x(), t * y(), t * z()};
    }

    CXX14_CONSTEXPR Vector3& operator*=(const value_type& t) CXX_NOEXCEPT {
        x() *= t;
        y() *= t;
        z() *= t;
        return *this;
    }

    CXX_CONSTEXPR Vector3 operator/(const Vector3& v) const CXX_NOEXCEPT {
        return {x() / v.x(), y() / v.y(), z() / v.z()};
    }

    CXX14_CONSTEXPR Vector3 operator/(const value_type& t) const CXX_NOEXCEPT {
        const value_type inv_t = value_type(1) / t;
        return *this * inv_t;
    }

    CXX14_CONSTEXPR Vector3& operator/=(const value_type& t) CXX_NOEXCEPT {
        const value_type inv_t = value_type(1) / t;
        return (*this *= inv_t);
    }

private:
    // Elements
    std::array<value_type, 3> elements;
};

template <typename T>
CXX_CONSTEXPR Vector3<T>
operator*(const typename Vector3<T>::value_type& t, const Vector3<T>& v) CXX_NOEXCEPT {
    return {t * v.x(), t * v.y(), t * v.z()};
}

template <typename T>
CXX14_CONSTEXPR Vector3<T> Min(const Vector3<T>& v1, const Vector3<T>& v2) CXX_NOEXCEPT {
    return {std::min(v1.x(), v2.x()), std::min(v1.y(), v2.y()), std::min(v1.z(), v2.z())};
}

template <typename T>
CXX14_CONSTEXPR T MinComponent(const Vector3<T>& v) CXX_NOEXCEPT {
    return std::min(v.x(), std::min(v.y(), v.z()));
}

template <typename T>
CXX14_CONSTEXPR Vector3<T> Max(const Vector3<T>& v1, const Vector3<T>& v2) CXX_NOEXCEPT {
    return {std::max(v1.x(), v2.x()), std::max(v1.y(), v2.y()), std::max(v1.z(), v2.z())};
}

template <typename T>
CXX14_CONSTEXPR T MaxComponent(const Vector3<T>& v) CXX_NOEXCEPT {
    return std::max(v.x(), std::max(v.y(), v.z()));
}

template <typename T>
CXX_INLINE Vector3<T> Abs(const Vector3<T>& v) CXX_NOEXCEPT {
    return {std::abs(v.x()), std::abs(v.y()), std::abs(v.z())};
}

template <typename T>
CXX14_CONSTEXPR Axis MaxDimension(const Vector3<T>& v) CXX_NOEXCEPT {
    if (v.x() > v.y() && v.x() > v.z()) {
        return Axis::X;
    } else if (v.y() > v.z()) {
        return Axis::Y;
    } else {
        return Axis::Z;
    }
}

template <typename T>
CXX_CONSTEXPR typename Vector3<T>::value_type
Dot(const Vector3<T>& v1, const Vector3<T>& v2) CXX_NOEXCEPT {
    return v1.x() * v2.x() + v1.y() * v2.y() + v1.z() * v2.z();
}

template <typename T>
CXX_INLINE typename Vector3<T>::value_type
AbsDot(const Vector3<T>& v1, const Vector3<T>& v2) CXX_NOEXCEPT {
    return std::abs(Dot(v1, v2));
}

template <typename T>
CXX_CONSTEXPR Vector3<T> Cross(const Vector3<T>& v1, const Vector3<T>& v2) CXX_NOEXCEPT {
    return {v1.y() * v2.z() - v1.z() * v2.y(),
            v1.z() * v2.x() - v1.x() * v2.z(),
            v1.x() * v2.y() - v1.y() * v2.x()};
}

template <typename T>
CXX_CONSTEXPR typename Vector3<T>::value_type LengthSquared(const Vector3<T>& v) CXX_NOEXCEPT {
    return Dot(v, v);
}

template <typename T>
CXX_CONSTEXPR typename Vector3<T>::value_type
DistanceSquared(const Vector3<T>& v, const Vector3<Real>& w) CXX_NOEXCEPT {
    return LengthSquared(w - v);
}

template <typename T>
CXX_INLINE typename Vector3<T>::value_type Distance(const Vector3<T>& v, const Vector3<T>& w) CXX_NOEXCEPT {
    return std::sqrt(DistanceSquared(v, w));
}

template <typename T>
CXX_INLINE typename Vector3<T>::value_type Length(const Vector3<T>& v) CXX_NOEXCEPT {
    return std::sqrt(LengthSquared((v)));
}

template <typename T>
CXX_INLINE Vector3<T> Normalize(const Vector3<T>& v) CXX_NOEXCEPT {
    return v / Length(v);
}

template <typename T>
CXX_INLINE Vector3<T>
Permute(const Vector3<T>& v,
        typename Vector3<T>::size_type x,
        typename Vector3<T>::size_type y,
        typename Vector3<T>::size_type z) CXX_NOEXCEPT {
    return {v[x], v[y], v[z]};
}

// Compute vector facing in the same direction as the given one
template <typename T>
CXX_CONSTEXPR Vector3<T> FaceForward(const Vector3<T>& n, const Vector3<T>& v) CXX_NOEXCEPT {
    return Dot(n, v) > 0 ? n : -n;
}

// Check if two vectors are in the same hemisphere
template <typename T>
CXX_CONSTEXPR bool SameHemisphere(const Vector3<T>& v, const Vector3<T>& w) CXX_NOEXCEPT {
    return Dot(v, w) > 0;
}

// Compute the theta angle of a given direction
template <typename T>
CXX_INLINE typename Vector3<T>::value_type SphericalTheta(const Vector3<T>& v) CXX_NOEXCEPT {
    return std::acos(v.y() / Length(v));
}

// Compute the phi angle of a given direction
template <typename T>
CXX_INLINE typename Vector3<T>::value_type SphericalPhi(const Vector3<T>& v) CXX_NOEXCEPT {
    return std::atan2(v.z(), v.x());
}

// Reflect v along n
template <typename T>
CXX_CONSTEXPR Vector3<T> Reflect(const Vector3<T>& v, const Vector3<T>& n) CXX_NOEXCEPT {
    return 2 * Dot(v, n) * n - v;
}

// Compute refraction direction
template <typename T>
CXX_INLINE bool Refract(const Vector3<T>& w, const Vector3<T>& n, T eta, Vector3<T>& wt) CXX_NOEXCEPT {
    const T cos_theta_i = Dot(n, w);
    const T sin2_theta_i = std::max(T(0), 1 - cos_theta_i * cos_theta_i);
    const T sin2_theta_t = eta * eta * sin2_theta_i;
    // Handle possible total internal reflection
    if (sin2_theta_t >= 1) { return false; }
    const T cos_theta_t = std::sqrt(1 - sin2_theta_t);
    // Compute transmission direction
    wt = -eta * w + (eta * cos_theta_i - cos_theta_t) * n;

    return true;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Vector3<T>& v) {
    os << v.x() << ", " << v.y() << ", " << v.z();
    return os;
}

// Matrix class
template <typename T>
class Matrix4x4 {
public:
    // Member types
    using value_type = T;
    using size_type = int;
    using reference = value_type&;
    using const_reference = const value_type&;
    using row_reference = std::array<value_type, 4>&;
    using const_row_reference = const std::array<value_type, 4>&;

    // Constructors
    CXX_INLINE Matrix4x4() CXX_NOEXCEPT {
        for (auto& row : elements) {
            for (auto& c : row) {
                c = value_type(0);
            }
        }
    }

    CXX_INLINE explicit Matrix4x4(const value_type& d) CXX_NOEXCEPT {
        for (size_type i = 0; i < 4; ++i) {
            for (size_type j = 0; j < 4; ++j) {
                if (i == j) {
                    elements[i][j] = d;
                } else {
                    elements[i][j] = value_type(0);
                }
            }
        }
    }

    // Create identity matrix
    static Matrix4x4 Identity() CXX_NOEXCEPT {
        return Matrix4x4(value_type(1));
    }

    CXX_INLINE explicit Matrix4x4(const std::array<std::array<value_type, 4>, 4>& data) CXX_NOEXCEPT
            : elements(data) {}

    CXX17_CONSTEXPR
    Matrix4x4(const value_type& m00, const value_type& m01, const value_type& m02, const value_type& m03,
              const value_type& m10, const value_type& m11, const value_type& m12, const value_type& m13,
              const value_type& m20, const value_type& m21, const value_type& m22, const value_type& m23,
              const value_type& m30, const value_type& m31, const value_type& m32, const value_type& m33) CXX_NOEXCEPT {
        elements[0][0] = m00;
        elements[0][1] = m01;
        elements[0][2] = m02;
        elements[0][3] = m03;
        elements[1][0] = m10;
        elements[1][1] = m11;
        elements[1][2] = m12;
        elements[1][3] = m13;
        elements[2][0] = m20;
        elements[2][1] = m21;
        elements[2][2] = m22;
        elements[2][3] = m23;
        elements[3][0] = m30;
        elements[3][1] = m31;
        elements[3][2] = m32;
        elements[3][3] = m33;
    }

    // Index operator on rows
    CXX17_CONSTEXPR row_reference operator[](size_type i) CXX_NOEXCEPT {
        assert(i >= 0 && i < 4);
        return elements[i];
    }

    CXX17_CONSTEXPR const_row_reference operator[](size_type i) const CXX_NOEXCEPT {
        assert(i >= 0 && i < 4);
        return elements[i];
    }

    CXX17_CONSTEXPR Vector3<T> TransformDirection(const Vector3<T>& d) const CXX_NOEXCEPT {
        return {elements[0][0] * d.x() + elements[0][1] * d.y() + elements[0][2] * d.z(),
                elements[1][0] * d.x() + elements[1][1] * d.y() + elements[1][2] * d.z(),
                elements[2][0] * d.x() + elements[2][1] * d.y() + elements[2][2] * d.z()};
    }

    CXX17_CONSTEXPR Vector3<T> TransformPosition(const Vector3<T>& p) const CXX_NOEXCEPT {
        const value_type x = elements[0][0] * p.x() + elements[0][1] * p.y() + elements[0][2] * p.z() + elements[0][3];
        const value_type y = elements[1][0] * p.x() + elements[1][1] * p.y() + elements[1][2] * p.z() + elements[1][3];
        const value_type z = elements[2][0] * p.x() + elements[2][1] * p.y() + elements[2][2] * p.z() + elements[2][3];
        const value_type w = elements[3][0] * p.x() + elements[3][1] * p.y() + elements[3][2] * p.z() + elements[3][3];

        if (ApproxEqual(T(1), w)) {
            return {x, y, z};
        } else {
            return Vector3<T>(x, y, z) / w;
        }
    }

    // Transform normal vector, this assumes that the matrix is already the inverse and multiplies by the transpose
    CXX17_CONSTEXPR Vector3<T> TransformNormal(const Vector3<T>& n) const CXX_NOEXCEPT {
        return {elements[0][0] * n.x() + elements[1][0] * n.y() + elements[2][0] * n.z(),
                elements[0][1] * n.x() + elements[1][1] * n.y() + elements[2][1] * n.z(),
                elements[0][2] * n.x() + elements[1][2] * n.y() + elements[2][2] * n.z()};
    }

    // Multiply matrices together
    CXX14_CONSTEXPR Matrix4x4 operator*(const Matrix4x4& m) const CXX_NOEXCEPT {
        Matrix4x4 result;
        for (size_type i = 0; i < 4; ++i) {
            for (size_type j = 0; j < 4; ++j) {
                result[i][j] = elements[i][0] * m[0][j] +
                               elements[i][1] * m[1][j] +
                               elements[i][2] * m[2][j] +
                               elements[i][3] * m[3][j];
            }
        }

        return result;
    }

    // Compute inverse matrix
    Matrix4x4 Inverse() const CXX_NOEXCEPT;

    // Check if the matrix contains a scaling factor
    CXX_INLINE bool HasScale() const CXX_NOEXCEPT {
        const Real n0 = LengthSquared(this->TransformDirection(Vector3<T>(1, 0, 0)));
        const Real n1 = LengthSquared(this->TransformDirection(Vector3<T>(0, 1, 0)));
        const Real n2 = LengthSquared(this->TransformDirection(Vector3<T>(0, 0, 1)));

        return !ApproxEqual(n0, Real(1), Real(0.001)) ||
               !ApproxEqual(n1, Real(1), Real(0.001)) ||
               !ApproxEqual(n2, Real(1), Real(0.001));
    }

private:
    // Matrix elements, stored as rows
    std::array<std::array<value_type, 4>, 4> elements;
};

template <typename T>
Matrix4x4<T> Matrix4x4<T>::Inverse() const CXX_NOEXCEPT {
    // Matrix inversion based on the Gauss method
    std::array<size_type, 4> indxc, indxr;
    std::array<size_type, 4> ipiv = {0, 0, 0, 0};
    std::array<std::array<value_type, 4>, 4> minv = elements;

    for (size_type i = 0; i < 4; i++) {
        size_type irow = 0, icol = 0;
        value_type big = 0;
        // Choose pivot
        for (size_type j = 0; j < 4; j++) {
            if (ipiv[j] != 1) {
                for (size_type k = 0; k < 4; k++) {
                    if (ipiv[k] == 0) {
                        if (std::abs(minv[j][k]) >= big) {
                            big = std::abs(minv[j][k]);
                            irow = j;
                            icol = k;
                        }
                    } else if (ipiv[k] > 1) {
                        std::cerr << "Singular matrix given to inversion procedure" << std::endl;
                        exit(EXIT_FAILURE);
                    }
                }
            }
        }
        ++ipiv[icol];
        // Swap rows irow and icol for pivot
        if (irow != icol) {
            for (size_type k = 0; k < 4; ++k) {
                std::swap(minv[irow][k], minv[icol][k]);
            }
        }
        indxr[i] = irow;
        indxc[i] = icol;
        if (ApproxEqual(minv[icol][icol], value_type(0))) {
            std::cerr << "Singular matrix given to inversion procedure" << std::endl;
            exit(EXIT_FAILURE);
        }

        // Set m[icol][icol] to one by scaling row icol appropriately
        value_type pivinv = value_type(1) / minv[icol][icol];
        minv[icol][icol] = value_type(1);
        for (size_type j = 0; j < 4; j++) {
            minv[icol][j] *= pivinv;
        }

        // Subtract this row from others to zero out their columns
        for (size_type j = 0; j < 4; j++) {
            if (j != icol) {
                value_type save = minv[j][icol];
                minv[j][icol] = 0;
                for (size_type k = 0; k < 4; k++) {
                    minv[j][k] -= minv[icol][k] * save;
                }
            }
        }
    }
    // Swap columns to reflect permutation
    for (int j = 3; j >= 0; j--) {
        if (indxr[j] != indxc[j]) {
            for (size_type k = 0; k < 4; k++) {
                std::swap(minv[k][indxr[j]], minv[k][indxc[j]]);
            }
        }
    }

    return Matrix4x4(minv);
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix4x4<T>& m) {
    for (typename Matrix4x4<T>::size_type i = 0; i < 4; ++i) {
        for (typename Matrix4x4<T>::size_type j = 0; j < 4; ++j) {
            os << m[i][j] << " ";
        }
        if (i != 3) {
            os << "\n";
        }
    }

    return os;
}

// Define common types
using Vector2f = Vector2<float>;
using Vector2d = Vector2<double>;
using Vector2i = Vector2<int>;
using Vector2st = Vector2<std::size_t>;

using Vector3f = Vector3<float>;
using Vector3d = Vector3<double>;

using Matrix4x4f = Matrix4x4<float>;
using Matrix4x4d = Matrix4x4<double>;

#endif // INTERSECTION_KERNEL_LIN_ALGEBRA_HPP
