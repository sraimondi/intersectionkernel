#ifndef INTERSECTION_KERNEL_BVH_HPP
#define INTERSECTION_KERNEL_BVH_HPP

#include "shape.hpp"

#include <vector>
#include <memory>
#include <exception>

// Forward declarations
struct BVHBuildNode {
    BVHBuildNode() = default;

    // Initialize Node as a leaf node, which means that the children pointer are null and num_shapes != 0
    void InitLeaf(std::size_t first_shape, std::size_t n, const Bounds& b);

    // Initialize Node as an interior node, which means that che children pointer are not null and num_triangles == 0. 
    // Children are passed by value and moved to the struct pointers
    void InitInterior(std::size_t axis, std::unique_ptr<BVHBuildNode> c0, std::unique_ptr<BVHBuildNode> c1);

    // Bounds of all the shapes in this node
    Bounds bounds;
    // Children nodes
    std::array<std::unique_ptr<BVHBuildNode>, 2> children;
    // Axis that was used to split along for the sub-nodes
    std::size_t spit_axis;
    // Store the shapes in the node, from first_shape_offset to first_shape_offset + num_shapes NOT included
    std::size_t first_shape_offset, num_shapes;
};

struct BVHShapeInfo {
    BVHShapeInfo(std::size_t index, const Bounds& b);

    // Index of the shape in the local_shapes list
    std::size_t shape_index;
    // Bounds of the shape
    Bounds bounds;
    // Centroid of the Bounds
    Vector3<Real> centroid;
};

struct CXX_ALIGNAS(32) LinearBVHNode {
    // Bounds of the node
    Bounds bounds;
    union {
        // Offset of the first shape for leaf
        uint32_t shape_offset;
        // Second child offset for interior nodes
        uint32_t second_child_offset;
    };
    // Number of shapes in the node
    uint8_t num_shapes;
    // Split axis
    uint8_t axis;
    // Padding to ensure alignment
    uint8_t padding[2];
};

// Custom exception for BVH
class BVHException : public std::runtime_error {
public:
    explicit BVHException(const std::string& err);
};

// Type of heuristic to use to split primitives
enum class BVHSplitMethod {
    SplitEqual,
    SplitSAH
};

// Policy for the computation of the delta during the BVH split, defaults to sqrt
enum class BVHDeltaPolicy {
    One,
    Sqrt,
    Buckets
};

// Compute delta for the given number of triangles to check
class BVHDelta {
public:
    explicit BVHDelta(BVHDeltaPolicy policy = BVHDeltaPolicy::One, std::size_t n_b = 15);

    // Compute delta for given start and end
    std::size_t ComputeDelta(std::size_t start, std::size_t end) const CXX_NOEXCEPT;

private:
    const BVHDeltaPolicy policy;
    // Used with Buckets policy
    const std::size_t num_buckets;
};

// BVH configuration class BVHConfig
class BVHConfig {
public:
    // Constructor with all default values
    explicit BVHConfig(const BVHSplitMethod& split = BVHSplitMethod::SplitSAH,
                       const BVHDelta& delta = BVHDelta(),
                       std::size_t max_s_leaf = 1,
                       Real tr_cost = 1, Real b_cost = 0.125,
                       std::size_t min_tr_p_sah = 1000);

    // Triangles split heuristic
    const BVHSplitMethod split_method;
    // Delta computation policy
    const BVHDelta delta_policy;
    // Maximum number of shapes in a leaf node
    const std::size_t max_shapes_in_leaf;
    // Shape intersection cost
    Real shape_cost;
    // Bounds intersection cost
    Real bounds_cost;
    // Minimum number of shapes to split SAH evaluation to multiple threads
    const std::size_t min_shapes_parallel_sah;
};

class BVH {
public:
    // Constructor expects a list of triangles that will the stored in the acceleration structure
    BVH(const std::vector<ShapeInstance>& shapes, const BVHConfig& config);

    BVH(std::vector<ShapeInstance>&& shapes, const BVHConfig& config);

    ~BVH();

    // Check for intersection between ray and triangles in BVH
    void Intersect(const Ray& ray, Intersection& isect) const CXX_NOEXCEPT;

    // Check if ray hits a triangle in the BVH
    bool IntersectP(const Ray& ray, Real max_t) const CXX_NOEXCEPT;

private:
    // Start BVH build process
    void Build();

    // Recursively build a part of the tree, in the interval [start, end) in the information and returns a pointer to the node built. Reorders the triangles such that they are close for the leafs
    std::unique_ptr<BVHBuildNode>
    RecursiveBuild(std::vector<BVHShapeInfo>& shapes_info,
                   std::size_t start, std::size_t end,
                   std::size_t& total_nodes,
                   std::vector<ShapeInstance>& ordered_shapes);

    // Compute best split using SAH, returns the nest cost and the split index
    std::pair<Real, std::size_t>
    FindBestSplitSAH(const std::vector<BVHShapeInfo>& shapes_info,
                     std::size_t start, std::size_t end,
                     Real one_over_bounds_area) const;

    // Same as previous function but in parallel
    std::pair<Real, std::size_t>
    FindBestSplitSAHParallel(const std::vector<BVHShapeInfo>& shapes_info,
                             std::size_t start, std::size_t end,
                             Real one_over_bounds_area) const;

    // Compress pointer version to linear array version
    std::size_t FlattenTree(const BVHBuildNode* node, std::size_t& offset);

    // BVH configuration
    const BVHConfig configuration;
    // Number of available threads
    const std::size_t num_threads;
    // Local shapes
    std::vector<ShapeInstance> local_shapes;
    // Linear nodes of the tree
    LinearBVHNode* flat_nodes;
    std::size_t total_nodes;
};

#endif // INTERSECTION_KERNEL_BVH_HPP
