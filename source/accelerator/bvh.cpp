#include "bvh.hpp"
#include "memory.hpp"

#include <thread>
#include <iostream>

void BVHBuildNode::InitLeaf(std::size_t first_shape, std::size_t n, const Bounds& b) {
    bounds = b;
    first_shape_offset = first_shape;
    num_shapes = n;
}

void BVHBuildNode::InitInterior(std::size_t axis, std::unique_ptr<BVHBuildNode> c0, std::unique_ptr<BVHBuildNode> c1) {
    children[0] = std::move(c0);
    children[1] = std::move(c1);
    // Compute bounds from children
    bounds = Bounds(children[0]->bounds, children[1]->bounds);
    // Store the split axis for the children
    spit_axis = axis;
}

BVHShapeInfo::BVHShapeInfo(std::size_t index, const Bounds& b)
        : shape_index(index), bounds(b), centroid(bounds.Centroid()) {}

BVHException::BVHException(const std::string& err)
        : std::runtime_error(err) {}

BVHDelta::BVHDelta(BVHDeltaPolicy policy, size_t n_b)
        : policy(policy), num_buckets(n_b) {}

std::size_t BVHDelta::ComputeDelta(std::size_t start, std::size_t end) const CXX_NOEXCEPT {
    switch (policy) {
        case BVHDeltaPolicy::One: {
            return 1;
        }
        case BVHDeltaPolicy::Sqrt: {
            return std::max(static_cast<std::size_t>(std::sqrt(end - start)), std::size_t(1));
        }
        case BVHDeltaPolicy::Buckets: {
            return std::max(DivideUp(end - start, num_buckets), std::size_t(1));
        }
        default: {
            std::cerr << "Invalid configuration given to compute step delta for SAH heuristic";
            return 0;
        }
    }
}

BVHConfig::BVHConfig(const BVHSplitMethod& split,
                     const BVHDelta& delta,
                     std::size_t max_s_leaf,
                     Real tr_cost, Real b_cost,
                     std::size_t min_tr_p_sah)
        : split_method(split), delta_policy(delta),
          max_shapes_in_leaf(std::min(static_cast<std::size_t>(255), max_s_leaf)),
          shape_cost(tr_cost), bounds_cost(b_cost),
          min_shapes_parallel_sah(min_tr_p_sah) {}

BVH::BVH(const std::vector<ShapeInstance>& shapes, const BVHConfig& config)
        : configuration(config), num_threads(std::thread::hardware_concurrency()),
          local_shapes(shapes),
          total_nodes(0) {
    Build();
}

BVH::BVH(std::vector<ShapeInstance>&& shapes, const BVHConfig& config)
        : configuration(config), num_threads(std::thread::hardware_concurrency()),
          local_shapes(std::move(shapes)),
          total_nodes(0) {
    Build();
}

BVH::~BVH() {
    if (flat_nodes) {
        FreeAligned(flat_nodes);
    }
}

void BVH::Intersect(const Ray& ray, Intersection& isect) const noexcept {
    // If there are no shapes, return
    if (local_shapes.empty()) { return; }
    // Precomputed local variables for ray
    const Vector3<Real> inv_dir = Vector3<Real>(1) / ray.Direction();
    const std::array<bool, 3> dir_is_neg({inv_dir.x() < 0, inv_dir.y() < 0, inv_dir.z() < 0});
    // Stack of nodes to travers
    uint32_t nodes_to_visit[64];
    uint32_t to_visit_offset = 0, current_node = 0;
    while (true) {
        const LinearBVHNode& node = flat_nodes[current_node];
        // Check if ray intersect the bounds of the node
        if (node.bounds.Intersect(ray.Origin(), inv_dir, dir_is_neg, isect.t)) {
            if (node.num_shapes > 0) {
                // Intersect ray with all shapes in node
                for (uint8_t i = 0; i < node.num_shapes; ++i) {
                    local_shapes[node.shape_offset + i].Intersect(ray, isect);
                }
                // Check if we don't have any more node to visit
                if (to_visit_offset == 0) { break; }
                current_node = nodes_to_visit[--to_visit_offset];
            } else {
                if (dir_is_neg[node.axis]) {
                    // Process second child first
                    nodes_to_visit[to_visit_offset++] = current_node + 1;
                    current_node = node.second_child_offset;
                } else {
                    nodes_to_visit[to_visit_offset++] = node.second_child_offset;
                    current_node = current_node + 1;
                }
            }
        } else {
            // Check if we don't have any more node to visit
            if (to_visit_offset == 0) { break; }
            current_node = nodes_to_visit[--to_visit_offset];
        }
    }
}

bool BVH::IntersectP(const Ray& ray, Real max_t) const noexcept {
    // If there are no triangles, return 
    if (local_shapes.empty()) { return false; }
    // Precomputed local variables for ray
    const auto inv_dir = Vector3<Real>(1) / ray.Direction();
    const std::array<bool, 3> dir_is_neg({inv_dir.x() < 0, inv_dir.y() < 0, inv_dir.z() < 0});
    // Stack of nodes to travers
    uint32_t nodes_to_visit[64];
    uint32_t to_visit_offset = 0, current_node = 0;
    while (true) {
        const auto& node = flat_nodes[current_node];
        // Check if ray intersect the bounds of the node
        if (node.bounds.Intersect(ray.Origin(), inv_dir, dir_is_neg, max_t)) {
            if (node.num_shapes > 0) {
                // Intersect ray with all triangles in node
                for (uint8_t i = 0; i < node.num_shapes; ++i) {
                    if (local_shapes[node.shape_offset + i].IntersectP(ray, max_t)) {
                        return true;
                    }
                }
                // Check if we don't have any more node to visit
                if (to_visit_offset == 0) { break; }
                current_node = nodes_to_visit[--to_visit_offset];
            } else {
                if (dir_is_neg[node.axis]) {
                    // Process second child first
                    nodes_to_visit[to_visit_offset++] = current_node + 1;
                    current_node = node.second_child_offset;
                } else {
                    nodes_to_visit[to_visit_offset++] = node.second_child_offset;
                    current_node = current_node + 1;
                }
            }
        } else {
            // Check if we don't have any more node to visit
            if (to_visit_offset == 0) { break; }
            current_node = nodes_to_visit[--to_visit_offset];
        }
    }

    return false;
}

void BVH::Build() {
    // Handle case where the list of triangles is empty
    if (local_shapes.empty()) {
        throw BVHException("Triangle list for BVH construction is empty");
    }

    // Initialize information about triangles for building
    std::vector<BVHShapeInfo> shapes_info;
    shapes_info.reserve(local_shapes.size());
    for (std::size_t i = 0; i < local_shapes.size(); ++i) {
        shapes_info.emplace_back(i, local_shapes[i].GetBounds());
    }

    // Reordered triangles
    std::vector<ShapeInstance> ordered_shapes;
    ordered_shapes.reserve(local_shapes.size());
    std::cout << "Starting BVH building...\n";
    auto start = std::chrono::high_resolution_clock::now();
    auto root = RecursiveBuild(shapes_info, 0, local_shapes.size(), total_nodes, ordered_shapes);
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    std::cout << "BVH building took " << duration.count() / 1000.f << " seconds\n";
    // After building, swap the content of the local_triangles with the sorted ones
    local_shapes = std::move(ordered_shapes);

    // Now put tree in flat representation
    flat_nodes = AllocateAligned<LinearBVHNode>(total_nodes);
    if (!flat_nodes) {
        std::cerr << "Error allocating memory for flat BVH\n";
        throw std::bad_alloc();
    }

    // Put tree in flat representation
    std::size_t offset = 0;
    FlattenTree(root.get(), offset);
    assert(total_nodes == offset);

    std::cout << "Built BVH acceleration structure with " << local_shapes.size() << " shapes and " << total_nodes
              << " nodes\n";
#ifndef NDEBUG
    std::cout << "Size of the tree is " << total_nodes * sizeof(LinearBVHNode) << " bytes\n";
#endif
}

std::unique_ptr<BVHBuildNode> BVH::RecursiveBuild(std::vector<BVHShapeInfo>& shapes_info,
                                                  std::size_t start, std::size_t end,
                                                  std::size_t& total_nodes,
                                                  std::vector<ShapeInstance>& ordered_shapes) {
    // Add one node to the total
    ++total_nodes;
    // Create local node
#if CXX_VERSION >= CXX_14
    auto node = std::make_unique<BVHBuildNode>();
#else
    auto node = std::unique_ptr<BVHBuildNode>(new BVHBuildNode());
#endif
    // Compute Bounds for all triangles in the node
    Bounds node_bounds;
    for (std::size_t i = start; i < end; ++i) {
        node_bounds.Extend(shapes_info[i].bounds);
    }

    // Compute number of triangles to work on
    const std::size_t num_triangles = end - start;
    if (num_triangles <= configuration.max_shapes_in_leaf) {
        // Create leaf node
        // Store the offset in ordered triangles where we start to insert the triangles for this node
        const std::size_t first_triangle_offset = ordered_shapes.size();
        for (std::size_t i = start; i < end; ++i) {
            ordered_shapes.push_back(local_shapes[shapes_info[i].shape_index]);
        }
        // Initialise node as leaf
        node->InitLeaf(first_triangle_offset, num_triangles, node_bounds);
        return node;
    } else {
        // Compute bounds of centroids
        Bounds centroids_bounds;
        for (std::size_t i = start; i < end; ++i) {
            centroids_bounds.Extend(shapes_info[i].centroid);
        }
        const auto split_dim = static_cast<std::size_t>(centroids_bounds.MaxExtent());

        // Partition triangles in two sets and build children
        std::size_t mid = (start + end) / 2;
        // Check that the bounds are not degenerated
        if (centroids_bounds[0][split_dim] == centroids_bounds[1][split_dim]) {
            // Create leaf node
            // Store the offset in ordered triangles where we start to insert the triangles for this node
            const std::size_t first_triangle_offset = ordered_shapes.size();
            for (std::size_t i = start; i < end; ++i) {
                ordered_shapes.push_back(local_shapes[shapes_info[i].shape_index]);
            }
            // Initialise node as leaf
            node->InitLeaf(first_triangle_offset, num_triangles, node_bounds);
            return node;
        } else {
            switch (configuration.split_method) {
                case BVHSplitMethod::SplitEqual: {
                    // Partition triangles into two equal count partitions
                    std::nth_element(shapes_info.begin() + start, shapes_info.begin() + mid,
                                     shapes_info.begin() + end,
                                     [split_dim](const BVHShapeInfo& a, const BVHShapeInfo& b) {
                                         return a.centroid[split_dim] < b.centroid[split_dim];
                                     });
                    break;
                }
                case BVHSplitMethod::SplitSAH: {
                    // First sort the primitives along the split axis
                    std::sort(shapes_info.begin() + start, shapes_info.begin() + end,
                              [split_dim](const BVHShapeInfo& a, const BVHShapeInfo& b) {
                                  return a.centroid[split_dim] < b.centroid[split_dim];
                              });

                    // Const of creating a leaf with all triangles in it
                    const Real leaf_cost = num_triangles * configuration.shape_cost;

                    // Find best split using SAH
                    std::pair<Real, std::size_t> sh_result;
                    const Real inv_bounds_area = 1 / node_bounds.area();
                    if (num_triangles > configuration.min_shapes_parallel_sah) {
                        sh_result = FindBestSplitSAHParallel(shapes_info, start, end, inv_bounds_area);
                    } else {
                        sh_result = FindBestSplitSAH(shapes_info, start, end, inv_bounds_area);
                    }

                    // Check if it's worth to split or create a leaf here
                    if (sh_result.first < leaf_cost) {
                        // Set the new mid point based on the index we found for the best split
                        mid = sh_result.second;
                    } else {
                        // Create leaf node
                        // Store the offset in ordered triangles where we start to insert the triangles for this node
                        const std::size_t first_triangle_offset = ordered_shapes.size();
                        for (std::size_t i = start; i < end; ++i) {
                            ordered_shapes.push_back(local_shapes[shapes_info[i].shape_index]);
                        }
                        // Initialise node as leaf
                        node->InitLeaf(first_triangle_offset, num_triangles, node_bounds);
                        return node;
                    }
                }
            }

            // Initialise node by recursively calling the build procedure on the sub nodes
            node->InitInterior(split_dim,
                               RecursiveBuild(shapes_info, start, mid, total_nodes, ordered_shapes),
                               RecursiveBuild(shapes_info, mid, end, total_nodes, ordered_shapes));
        }
    }

    return node;
}

std::pair<Real, std::size_t> BVH::FindBestSplitSAH(const std::vector<BVHShapeInfo>& shapes_info,
                                                   std::size_t start, std::size_t end,
                                                   Real one_over_bounds_area) const {
    // Store the current best SAH result with the cost and the index
    auto result = std::pair<Real, std::size_t>(INF, 1);

    for (std::size_t split_index = start + 1; split_index < end; ++split_index) {
        Bounds c0_bounds, c1_bounds;
        // Compute child 0 bounds
        for (std::size_t c0_index = start; c0_index < split_index; ++c0_index) {
            c0_bounds.Extend(shapes_info[c0_index].bounds);
        }
        for (std::size_t c1_index = split_index; c1_index < end; ++c1_index) {
            c1_bounds.Extend(shapes_info[c1_index].bounds);
        }
        // Evaluate SH cost
        const std::size_t count_0 = split_index - start;
        const std::size_t count_1 = end - split_index;

        const Real sh_cost = configuration.bounds_cost +
                             (count_0 * c0_bounds.area() + count_1 * c1_bounds.area()) * configuration.shape_cost *
                             one_over_bounds_area;
        // Check if this split has a better cost then the current one
        if (sh_cost < result.first) {
            result.first = sh_cost;
            result.second = split_index;
        }
    }

    return result;
}

std::pair<Real, std::size_t> BVH::FindBestSplitSAHParallel(const std::vector<BVHShapeInfo>& shapes_info,
                                                           std::size_t start, std::size_t end,
                                                           Real one_over_bounds_area) const {
    // Compute the split delta for the SAH computation based on the policy
    const std::size_t split_delta = configuration.delta_policy.ComputeDelta(start, end);

    // Each thread will store his results and then we filter them out in the main thread
    std::vector<std::pair<Real, std::size_t>> threads_result(num_threads);

    // Launch threads
    std::vector<std::thread> threads;

    const std::size_t num_indices = DivideUp(end - start, num_threads);
    for (std::size_t id = 0; id < num_threads; ++id) {
        // Compute range for thread
        const std::size_t split_start = start + id * num_indices;
        const std::size_t split_end = std::min(end, split_start + num_indices);

        // Launch thread
        threads.emplace_back(
                [start, end, split_start, split_end, split_delta, one_over_bounds_area](
                        const std::vector<BVHShapeInfo>& shapes_info,
                        Real bounds_cost, Real shape_cost,
                        std::pair<Real, std::size_t>& result) -> void {
                    // Initialise result
                    result = std::pair<Real, std::size_t>(INF, 1);

                    // Loop over the range of split indices that the thread must try
                    for (std::size_t split_index = split_start + 1;
                         split_index < split_end; split_index += split_delta) {
                        // Compute children bounds
                        Bounds c0_bounds, c1_bounds;
                        for (std::size_t c0_index = start; c0_index < split_index; ++c0_index) {
                            c0_bounds.Extend(shapes_info[c0_index].bounds);
                        }
                        for (std::size_t c1_index = split_index; c1_index < end; ++c1_index) {
                            c1_bounds.Extend(shapes_info[c1_index].bounds);
                        }

                        // Evaluate SH cost
                        const std::size_t count_0 = split_index - start;
                        const std::size_t count_1 = end - split_index;
                        const Real sh_cost = bounds_cost + (count_0 * c0_bounds.area() + count_1 * c1_bounds.area()) *
                                                           shape_cost * one_over_bounds_area;

                        // Check if this split has a better cost then the current one
                        if (sh_cost < result.first) {
                            result.first = sh_cost;
                            result.second = split_index;
                        }
                    }
                },
                std::cref(shapes_info),
                configuration.bounds_cost, configuration.shape_cost,
                std::ref(threads_result[id])
        );
    }

    // Synchronize
    for (auto& th : threads) {
        th.join();
    }

    // Find best one and return
    std::size_t best_split_index = 0;
    for (std::size_t i = 1; i < threads_result.size(); ++i) {
        if (threads_result[i].first < threads_result[best_split_index].first) {
            best_split_index = i;
        }
    }

    return threads_result[best_split_index];
}

std::size_t BVH::FlattenTree(const BVHBuildNode* node, std::size_t& offset) {
    // Get reference to the next free node in the linear representation
    LinearBVHNode& linear_node = flat_nodes[offset];
    linear_node.bounds = node->bounds;
    std::size_t this_offset = offset++;
    // Check if this is an interior node
    if (node->num_shapes > 0) {
        linear_node.shape_offset = static_cast<uint32_t>(node->first_shape_offset);
        linear_node.num_shapes = static_cast<uint8_t>(node->num_shapes);
    } else {
        linear_node.axis = static_cast<uint8_t>(node->spit_axis);
        linear_node.num_shapes = 0;
        FlattenTree(node->children[0].get(), offset);
        linear_node.second_child_offset = static_cast<uint32_t>(FlattenTree(node->children[1].get(), offset));
    }

    return this_offset;
}