#include "random_sampler.hpp"

RandomSampler::RandomSampler(std::size_t seed) CXX_NOEXCEPT
        : SamplerInterface(seed) {}

std::unique_ptr<SamplerInterface> RandomSampler::Clone(std::size_t seed) const {
#if CXX_VERSION >= CXX_14
    return std::make_unique<RandomSampler>(seed);
#else
    return std::unique_ptr<RandomSampler>(new RandomSampler(seed));
#endif
}

std::vector<Real> RandomSampler::Request1DArray(std::size_t n) {
    std::vector<Real> samples(n);
    std::generate(samples.begin(), samples.end(), [this]() -> Real {
        return Request1D();
    });
    return samples;
}

std::vector<Vector2<Real>> RandomSampler::Request2DArray(std::size_t n) {
    std::vector<Vector2<Real>> samples(n);
    std::generate(samples.begin(), samples.end(), [this]() -> Vector2<Real> {
        return Request2D();
    });
    return samples;
}

void RandomSampler::Fill1DArray(const std::vector<Real>::iterator& begin, const std::vector<Real>::iterator& end) {
    for (auto it = begin; it != end; ++it) {
        *it = Request1D();
    }
}

void RandomSampler::Fill2DArray(const std::vector<Vector2<Real>>::iterator& begin,
                                const std::vector<Vector2<Real>>::iterator& end) {
    for (auto it = begin; it != end; ++it) {
        *it = Request2D();
    }
}


