#ifndef INTERSECTIONKERNEL_SAMPLER_HPP
#define INTERSECTIONKERNEL_SAMPLER_HPP

#include "lin_algebra.hpp"
#include "rng.hpp"

#include <vector>
#include <memory>

// Base Sampler interface
class SamplerInterface {
public:
    // Create new sampler with a given seed
    explicit SamplerInterface(std::size_t seed) CXX_NOEXCEPT;

    virtual ~SamplerInterface() = default;

    // Clone sampler
    virtual std::unique_ptr<SamplerInterface> Clone(std::size_t seed) const = 0;

    // Round requested number of samples, gives to opportunity to certain samplers to give better distributions
    virtual std::size_t RoundRequest(std::size_t n) const CXX_NOEXCEPT;

    // Return a vector of 1D samples of given size
    virtual std::vector<Real> Request1DArray(std::size_t n) = 0;

    // Return a vector of 2D samples of given size
    virtual std::vector<Vector2<Real>> Request2DArray(std::size_t n) = 0;

    // Fill a given vector with 1D samples
    virtual void Fill1DArray(const std::vector<Real>::iterator& begin, const std::vector<Real>::iterator& end) = 0;

    // Fill a given vector with 2D samples
    virtual void Fill2DArray(const std::vector<Vector2<Real>>::iterator& begin,
                             const std::vector<Vector2<Real>>::iterator& end) = 0;

    // Return a random single value
    CXX_INLINE Real Request1D() CXX_NOEXCEPT {
        return Real(rng.NextFloat());
    }

    // Return a random 2D value
    CXX_INLINE Vector2<Real> Request2D() CXX_NOEXCEPT {
        return {rng.NextFloat(), rng.NextFloat()};
    }

    // Shuffle STL container using the RNG values
    template <typename Iterator>
    void Shuffle(Iterator begin, Iterator end) {
        for (auto it = end - 1; it > begin; --it) {
            std::iter_swap(it, begin + rng.NextUInt32(static_cast<uint32_t>(it - begin + 1)));
        }
    }

protected:
    // Random number generator
    RNG rng;
};

#endif //INTERSECTIONKERNEL_SAMPLER_HPP
