#ifndef INTERSECTIONKERNEL_RNG_HPP
#define INTERSECTIONKERNEL_RNG_HPP

#include <cstdint>
#include "config.hpp"

CXX_CONSTEXPR uint64_t PCG32_DEFAULT_STATE = 0x853c49e6748fea9bULL;
CXX_CONSTEXPR uint64_t PCG32_DEFAULT_STREAM = 0xda3e39cb94b95bdbULL;
CXX_CONSTEXPR uint64_t PCG32_MULT = 6364136223846793005ULL;

class RNG {
public:
    // Create new RNG
    CXX_INLINE RNG() CXX_NOEXCEPT
            : state(PCG32_DEFAULT_STATE), inc(PCG32_DEFAULT_STREAM) {}

    CXX_INLINE explicit RNG(uint64_t init_state, uint64_t init_seq = 1u) CXX_NOEXCEPT {
        Seed(init_state, init_seq);
    }


    // Seed the pseudorandom number generator
    CXX_INLINE void Seed(uint64_t seed, uint64_t init_seq = 1) CXX_NOEXCEPT {
        state = 0u;
        inc = (init_seq << 1u) | 1u;
        NextUInt32();
        state += seed;
        NextUInt32();
    }

    // Compute next uniform uint32_t value
    CXX_INLINE uint32_t NextUInt32() CXX_NOEXCEPT {
        uint64_t old_state = state;
        state = old_state * PCG32_MULT + (inc | 1);
        const auto xorshifted = static_cast<uint32_t>(((old_state >> 18u) ^ old_state) >> 27u);
        const auto rot = static_cast<uint32_t>(old_state >> 59u);

        return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
    }

    // Compute next uniform uint32_t value within a given bound
    CXX_INLINE uint32_t NextUInt32(uint32_t bounds) CXX_NOEXCEPT {
        const uint32_t threshold = (~bounds + 1u) % bounds;
        while (true) {
            uint32_t r = NextUInt32();
            if (r >= threshold) {
                return r % bounds;
            }
        }
    }

    // Generate a single precision floating point value in the [0, 1) interval
    CXX_INLINE float NextFloat() CXX_NOEXCEPT {
        union {
            uint32_t u;
            float f;
        } p;

        p.u = (NextUInt32() >> 9) | 0x3f800000u;
        return p.f - 1.f;
    }

private:
    // Private state
    uint64_t state, inc;
};

#endif //INTERSECTIONKERNEL_RNG_HPP
