#ifndef INTERSECTIONKERNEL_STRATIFIED_SAMPLER_HPP
#define INTERSECTIONKERNEL_STRATIFIED_SAMPLER_HPP

#include "sampler.hpp"

// Stratifed sampler class
class StratifiedSampler : public SamplerInterface {
public:
    explicit StratifiedSampler(std::size_t seed = PCG32_DEFAULT_STATE) CXX_NOEXCEPT;

    std::unique_ptr<SamplerInterface> Clone(std::size_t seed) const override;

    std::size_t RoundRequest(std::size_t n) const CXX_NOEXCEPT override;

    std::vector<Real> Request1DArray(std::size_t n) override;

    std::vector<Vector2<Real>> Request2DArray(std::size_t n) override;

    void Fill1DArray(const std::vector<Real>::iterator& begin, const std::vector<Real>::iterator& end) override;

    void Fill2DArray(const std::vector<Vector2<Real>>::iterator& begin,
                     const std::vector<Vector2<Real>>::iterator& end) override;
};

#endif //INTERSECTIONKERNEL_STRATIFIED_SAMPLER_HPP
