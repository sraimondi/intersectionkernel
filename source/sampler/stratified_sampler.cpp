#include "stratified_sampler.hpp"

StratifiedSampler::StratifiedSampler(size_t seed) CXX_NOEXCEPT
        : SamplerInterface(seed) {}

std::unique_ptr<SamplerInterface> StratifiedSampler::Clone(std::size_t seed) const {
#if CXX_VERSION >= CXX_14
    return std::make_unique<StratifiedSampler>(seed);
#else
    return std::unique_ptr<StratifiedSampler>(new StratifiedSampler(seed));
#endif
}

std::size_t StratifiedSampler::RoundRequest(std::size_t n) const CXX_NOEXCEPT {
    // The StratifiedSampler rounds the request to the closest perfect square
    const auto n_r_sqrt = static_cast<std::size_t>(std::sqrt(static_cast<Real>(n)) + Real(0.5));
    return n_r_sqrt * n_r_sqrt;
}

std::vector<Real> StratifiedSampler::Request1DArray(std::size_t n) {
    // Allocate space for samples and generate them
    std::vector<Real> samples(n);
    const Real inv_samples = 1 / static_cast<Real>(n);
    for (std::size_t s = 0; s < n; ++s) {
        samples[s] = (s + Request1D()) * inv_samples;
    }
    // Shuffle them and return
    Shuffle(samples.begin(), samples.end());

    return samples;
}

std::vector<Vector2<Real>> StratifiedSampler::Request2DArray(std::size_t n) {
    // Compute samples for each dimension
    const auto n_sqrt = static_cast<std::size_t>(std::sqrt(n));
    const Real inv_samples = 1 / static_cast<Real>(n_sqrt);
    // Allocate space for samples and generate them
    std::vector<Vector2<Real>> samples(n);
    for (std::size_t sy = 0; sy < n_sqrt; ++sy) {
        for (std::size_t sx = 0; sx < n_sqrt; ++sx) {
            samples[sy * n_sqrt + sx] = Vector2<Real>((sx + Request1D()) * inv_samples,
                                                      (sy + Request1D()) * inv_samples);
        }
    }

    // Shuffle them and return
    Shuffle(samples.begin(), samples.end());

    return samples;
}

void StratifiedSampler::Fill1DArray(const std::vector<Real>::iterator& begin, const std::vector<Real>::iterator& end) {
    // Compute how many samples we need to generate
    const auto n = static_cast<std::size_t>(std::distance(begin, end));
    const Real inv_samples = 1 / static_cast<Real>(n);
    for (std::size_t s = 0; s < n; ++s) {
        *(begin + s) = (s + Request1D()) * inv_samples;
    }
    // Shuffle samples
    Shuffle(begin, end);
}

void StratifiedSampler::Fill2DArray(const std::vector<Vector2<Real>>::iterator& begin,
                                    const std::vector<Vector2<Real>>::iterator& end) {
    // Compute how many samples we need to generate
    const auto n = std::distance(begin, end);
    const auto n_sqrt = static_cast<std::size_t>(std::sqrt(n));
    const Real inv_samples = 1 / static_cast<Real>(n_sqrt);
    // Loop over the two dimensions and generate samples
    for (std::size_t sy = 0; sy < n_sqrt; ++sy) {
        for (std::size_t sx = 0; sx < n_sqrt; ++sx) {
            *(begin + sy * n_sqrt + sx) = Vector2<Real>((sx + Request1D()) * inv_samples,
                                                        (sy + Request1D()) * inv_samples);
        }
    }
    Shuffle(begin, end);
}
