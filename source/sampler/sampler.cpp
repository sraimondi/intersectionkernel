#include "sampler.hpp"

SamplerInterface::SamplerInterface(std::size_t seed) CXX_NOEXCEPT
        : rng(seed) {}

std::size_t SamplerInterface::RoundRequest(std::size_t n) const CXX_NOEXCEPT {
    return n;
}
