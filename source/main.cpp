#include <iostream>
#include <chrono>
#include <filter/box_filter.hpp>
#include <filter/gaussian_filter.hpp>
#include <filter/mitchell_filter.hpp>
#include <texture/constant_texture.hpp>
#include <integrator/path_trace_integrator.hpp>
#include <shape/sphere.hpp>
#include <light/area_light.hpp>
#include "stratified_sampler.hpp"
#include "ray_direct_light_integrator.hpp"
#include "random_sampler.hpp"

#include "mesh_loader.hpp"
#include "image.hpp"
#include "camera.hpp"
#include "bvh.hpp"
#include "image_writer.hpp"
#include "transform.hpp"
#include "ray_whitted_integrator.hpp"
#include "diffuse_material.hpp"
#include "specular_material.hpp"
#include "point_light.hpp"
#include "infinite_light.hpp"
#include "emitting_material.hpp"

#include "image_integrator.hpp"

int main(int argc, const char* argv[]) {
    // Create film
    constexpr std::size_t WIDTH = 800, HEIGHT = 600;
    Image<Spectrum<Real>> film(WIDTH, HEIGHT);

    // Load meshes
    Mesh mesh = MeshLoader::LoadFromFile("../models/dragon.ply");
    Mesh plane = MeshLoader::LoadFromFile("../models/plane.ply");
    // Mesh sphere = MeshLoader::LoadFromFile("../models/sphere.ply");

    // Create mesh transformation matrices
    auto l2w_1 = Translate<Real>(0, 0, 0) * Scale<Real>(0.5) * RotateY<Real>(-40);
    auto w2l_1 = l2w_1.Inverse();

    auto l2w_2 = Translate<Real>(-150, 0, 0) * Scale<Real>(0.5) * RotateY<Real>(-40);
    auto w2l_2 = l2w_2.Inverse();

    auto l2w_3 = Translate<Real>(150, 0, 0) * Scale<Real>(0.5) * RotateY<Real>(-40);
    auto w2l_3 = l2w_3.Inverse();

    auto l2w_p = Translate<Real>(0, -59.4132 / 2, 0);
    auto w2l_p = l2w_p.Inverse();

//    auto I = Matrix4x4<Real>::Identity();

    auto S = Translate(Vector3<Real>(0, 120, 0));
    auto S_i = S.Inverse();

    // Test material
    const ConstantTexture<Spectrum<Real>> black_texture(Spectrum<Real>(0));
    const ConstantTexture<Spectrum<Real>> white_texture(Spectrum<Real>(0.9));
    DiffuseLambertMaterial diffuse_material(white_texture);

    ConstantTexture<Real> roughness(20);
    DiffuseOrenNayarMaterial diffuse_ON(white_texture, roughness);

    // Glass material
    const ConstantTexture<Real> ref_index_glass(1.3);
    const ConstantTexture<Real> ref_index_air(1);
    SpecularReflectionMaterial mirror(white_texture);
//    SpecularTransmissionMaterial transmission(white_texture, ref_index_air, ref_index_glass);
    DielectricMaterial glass(white_texture, white_texture, ref_index_air, ref_index_glass);

    // Create list of shapes
    std::vector<ShapeInstance> shapes;

    // Add the two dragons
    mesh.GenerateTrianglesInstances(l2w_3, w2l_3, diffuse_ON, shapes);
    mesh.GenerateTrianglesInstances(l2w_2, w2l_2, diffuse_ON, shapes);
    mesh.GenerateTrianglesInstances(l2w_1, w2l_1, glass, shapes);
    // Add plane
    plane.GenerateTrianglesInstances(l2w_p, w2l_p, diffuse_material, shapes);

    ConstantTexture<Spectrum<Real>> emission_texture(Spectrum<Real>(30));
    DiffuseEmittingMaterial emitting_material(emission_texture);

    // Add sphere
    Sphere sphere(40);
    ShapeInstance sphere_i = ShapeInstance(S, S_i, sphere, emitting_material);
    shapes.push_back(sphere_i);
//    sphere.GenerateTrianglesInstances(S, S_i, glass_material, triangles);

    // Create BVH
    BVHConfig bvh_config(BVHSplitMethod::SplitSAH, BVHDelta(BVHDeltaPolicy::Buckets));
    // Move triangles into BVH
    BVH bvh(shapes, bvh_config);

    // Create Scene
    Scene scene(bvh);

#if CXX_VERSION >= CXX_14

    scene.AddLight(std::make_unique<const AreaLight>(sphere_i, 25));
//    scene.AddLight(std::make_unique<const InfiniteLight>([](const Vector3<Real>& w) -> Spectrum<Real> {
//        return Clamp(w.y(), Real(0), Real(1)) * Spectrum<Real>(0.7);
//    }, 9));
//    scene.AddLight(std::make_unique<const PointLight>(Vector3<Real>(0, 100, 40), Spectrum<Real>(20000)));
//    scene.AddLight(std::make_unique<const PointLight>(Vector3<Real>(-100, 100, 0), Spectrum<Real>(10000)));
#else
    scene.AddLight(std::unique_ptr<const InfiniteLight>(new InfiniteLight([](const Vector3<Real>& w) -> Spectrum<Real> {
        return Clamp(w.y(), Real(0), Real(1)) * Spectrum<Real>(0.5);
    }, 16)));
    scene.AddLight(std::unique_ptr<const PointLight>(new PointLight(Vector3<Real>(0, 100, 40), Spectrum<Real>(20000))));
#endif


    // Create integrator
    //BaseIntegrator integrator(bvh);
//    NormalMapIntegrator integrator(bvh);


    // Create camera for lucy
//    Camera camera(Vector(500.f, 600.f, 1700.f), Vector(0.f, 100.f, 0.f),
//                  Vector(0.f, 1.f, 0.f), 50.f, WIDTH, HEIGHT);

    // Create camera for bunny
//    Camera camera(Vector3(-5.f, 6.f, 25.f), Vector3(0.f),
//                  Vector3(0.f, 1.f, 0.f), 50.f, WIDTH, HEIGHT);

    // Create camera for dragons
    Camera camera(Vector3<Real>(30.f, 170.f, 300.f),
                  Vector3<Real>(0.f),
                  Vector3<Real>(0.f, 1.f, 0.f),
                  45.f,
                  WIDTH, HEIGHT);

//    Camera camera(Vector3<Real>(0.f, 10.f, 0.f),
//                  Vector3<Real>(0.f),
//                  Vector3<Real>(0.f, 0.f, -1.f),
//                  45.f,
//                  WIDTH, HEIGHT);

//    // Create camera for buddha
//    // Camera camera(Vector(5.f, 10.f, 30.f), Vector(0.f),
//    //         Vector(0.f, 1.f, 0.f), 50.f, WIDTH, HEIGHT)

    // Create image integrator
    CXX_CONSTEXPR std::size_t SAMPLES_PER_PIXEL = 64;
    ImageIntegrator image_integrator(std::make_shared<const RayPathTraceIntegrator>(12),
                                     std::make_shared<StratifiedSampler>(),
                                     std::make_shared<const BoxFilter>(),
                                     SAMPLES_PER_PIXEL);

    // Render image
    const auto start = std::chrono::high_resolution_clock::now();

    image_integrator.Render(film, camera, scene);

    const auto end = std::chrono::high_resolution_clock::now();
    const auto duration = std::chrono::duration_cast<std::chrono::seconds>(end - start);

    std::cout << "Rendered image in " << duration.count() << " s.\n";

    GeneratePNG("test.png", film);

    return 0;
}
