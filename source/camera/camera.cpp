#include "camera.hpp"
#include "common.hpp"
#include "transform.hpp"

Camera::Camera(const Vector3<Real>& e, const Vector3<Real>& at, const Vector3<Real>& up, Real fov, std::size_t width,
               std::size_t height) CXX_NOEXCEPT
        : width(width), height(height), look_at(LookAt(e, at, up)), eye(look_at.TransformPosition(Vector3<Real>(0))) {
    // Compute fov
    t = std::tan(DegToRad(fov) / 2);
    r = t * width / static_cast<Real>(height);
}

Ray Camera::GenerateRay(std::size_t row, std::size_t col, const Vector2<Real>& u) const CXX_NOEXCEPT {
    const Real vp_x = -r + 2 * r * (col + Real(0.5) + u.x()) / static_cast<Real>(width);
    const Real vp_y = t - 2 * t * (row + Real(0.5) + u.y()) / static_cast<Real>(height);

    return {eye, Normalize(look_at.TransformDirection(Vector3<Real>(vp_x, vp_y, -1)))};
}
