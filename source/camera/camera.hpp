#ifndef INTERSECTION_KERNEL_CAMERA_HPP
#define INTERSECTION_KERNEL_CAMERA_HPP

#include "ray.hpp"

class Camera {
public:
    Camera(const Vector3<Real>& e, const Vector3<Real>& at, const Vector3<Real>& up, Real fov, std::size_t width,
           std::size_t height) CXX_NOEXCEPT;

    // Generate ray given row and column in the image + sample position from the pixel center
    Ray GenerateRay(std::size_t row, std::size_t col, const Vector2<Real>& u) const CXX_NOEXCEPT;

private:
    // Film size
    std::size_t width, height;
    // Look at matrix
    Matrix4x4<Real> look_at;
    // Eye position
    Vector3<Real> eye;
    // FOV
    Real t, r;
};

#endif // INTERSECTION_KERNEL_CAMERA_HPP

