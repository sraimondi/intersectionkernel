#include "ray_whitted_integrator.hpp"

std::unique_ptr<RayIntegratorInterface> RayWhittedIntegrator::Clone() const {
#if CXX_VERSION >= CXX_14
    return std::make_unique<RayWhittedIntegrator>(max_depth);
#else
    return std::unique_ptr<RayWhittedIntegrator>(new RayWhittedIntegrator(max_depth));
#endif
}

void RayWhittedIntegrator::Preprocess(const Scene&, SamplerInterface&) CXX_NOEXCEPT {
    // Nothing here
}

void RayWhittedIntegrator::NewRay(SamplerInterface&) CXX_NOEXCEPT {
    // Nothing here
}

Spectrum<Real> RayWhittedIntegrator::Li(const Ray& ray, const Scene& scene,
                                        SamplerInterface& sampler, unsigned int depth) const CXX_NOEXCEPT {
    // Output radiance
    Spectrum<Real> L(0);

    // Find closest ray hit, if any
    Intersection isect;
    scene.Intersect(ray, isect);

    // If we don't hit anything add contribution of infinite lights
    if (!isect.Hit()) {
        for (const auto& light : scene.GetLights()) {
            L += light->Le(ray);
        }
    } else {
        // Get pointer to material
        const auto& material = isect.hit_shape->GetMaterial();

        // If material is emitting, add contribution
        if (material.IsEmitting()) {
            L += material.Emission(isect, isect.wo);
        }

        // Loop over all lights and add contribution
        for (const auto& light : scene.GetLights()) {
            LightSample light_sample;
            OcclusionTester occ_tester;
            const Spectrum<Real> Li = light->SampleLi(isect, sampler.Request2D(), light_sample, &occ_tester);
            // Check for light contribution
            if (light_sample.pdf > 0 && !Li.IsBlack()) {
                // Evaluate BRDF for the sampled direction
                const Spectrum<Real> f = material.F(isect, light_sample.wi);
                // If brdf is not black and ray is not occluded, add contribution
                if (!f.IsBlack() && !occ_tester.Occluded(scene)) {
                    L += f * Li * Dot(isect.geometry_frame.n(), light_sample.wi) / light_sample.pdf;
                }
            }
        }

        // If we are not at max depth, check for light scattering
        if (depth + 1 < max_depth) {
            L += SpecularLi(isect, scene, sampler, depth);
        }
    }

    return L;
}

