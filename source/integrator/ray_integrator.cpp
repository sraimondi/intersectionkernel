#include "ray_integrator.hpp"

Spectrum<Real> RayIntegratorInterface::DirectLiAllLights(const Intersection& isect,
                                                         const std::vector<std::size_t>& light_num_samples,
                                                         const std::vector<Vector2<Real>>& material_samples,
                                                         const std::vector<Vector2<Real>>& light_samples,
                                                         const Scene& scene) const CXX_NOEXCEPT {
    // Total direct light
    Spectrum<Real> Ld(0);

    // Loop over all lights and add contribution
    std::size_t light_samples_offset = 0;
    for (std::size_t l = 0; l < scene.GetLights().size(); ++l) {
        // Direct light contribution for single source
        Spectrum<Real> Ld_light(0);
        // Sample the light uniformly
        for (std::size_t light_sample = 0; light_sample < light_num_samples[l]; ++light_sample) {
            Ld_light += DirectLi(isect, material_samples[light_samples_offset + light_sample],
                                 *scene.GetLights()[l], light_samples[light_samples_offset + light_sample],
                                 scene);
        }
        light_samples_offset += light_num_samples[l];
        // Add direct light contributions scaled by number of samples
        Ld += Ld_light / static_cast<Real>(light_num_samples[l]);
    }

    return Ld;
}

// Power heuristic for MIS
CXX14_CONSTEXPR Real PowerHeuristic(int nf, Real f_pdf, int ng, Real g_pdf) CXX_NOEXCEPT {
    const Real f = nf * f_pdf;
    const Real f2 = f * f;
    const Real g = ng * g_pdf;

    return f2 / (f2 + g * g);
}

Spectrum<Real> RayIntegratorInterface::DirectLi(const Intersection& isect, const Vector2<Real>& u_material,
                                                const LightInterface& light, const Vector2<Real>& u_light,
                                                const Scene& scene) const CXX_NOEXCEPT {
    // Final direct light contribution
    Spectrum<Real> Ld(0);

    // Get reference to intersection material
    const auto& material = isect.hit_shape->GetMaterial();

    // Direct illumination is computed only for materials that are not described as perfect specular lobes
    if (!material.MatchesType(M_SPECULAR)) {
        // Sample light source with MIS
        LightSample light_sample;
        OcclusionTester occ_tester;
        const Spectrum<Real> Li = light.SampleLi(isect, u_light, light_sample, &occ_tester);
        // Check for light contribution
        if (light_sample.pdf > 0 && !Li.IsBlack()) {
            // Evaluate BRDF for the sampled direction
            const Spectrum<Real> f = material.F(isect, light_sample.wi);
            const Real f_pdf = material.Pdf(isect, light_sample.wi);
            // If brdf is not black, incoming li and ray is not occluded, add contribution
            if (!f.IsBlack() && !occ_tester.Occluded(scene)) {
                if (light.IsDeltaLight()) {
                    Ld += f * Li * Dot(isect.geometry_frame.n(), light_sample.wi) / light_sample.pdf;
                } else {
                    const Real weight = PowerHeuristic(1, light_sample.pdf, 1, f_pdf);
                    Ld += f * Li * Dot(isect.geometry_frame.n(), light_sample.wi) * weight / light_sample.pdf;
                }
            }
        }

        // Sample material with MIS, unless light is described by delta distribution or material is specular
        if (!light.IsDeltaLight()) {
            // Sample material
            MaterialSample material_sample;
            const Spectrum<Real> f = material.SampleF(isect, u_material, material_sample);

            // Check if we have contribution from the material
            if (material_sample.pdf > 0 && !f.IsBlack()) {
                // Compute PDF for the light
                const Real light_pdf = light.PdfLi(isect, material_sample.wi);
                // If the pdf of the light is zero, we can return directly
                if (ApproxEqual(light_pdf, Real(0))) {
                    return Ld;
                }
                const Real mis_weight = PowerHeuristic(1, material_sample.pdf, 1, light_pdf);

                // Try to intersect ray with light
                Intersection light_isect;
                const Ray light_ray = isect.SpawnRay(light_sample.wi);
                scene.Intersect(light_ray, light_isect);

                // Add light contribution from material sampling
                Spectrum<Real> Li_sampled(0);
                if (light_isect.Hit()) {
                    // Check if the light we are sampling is the same as the intersected
                    if (light_isect.hit_shape == light.GetRepresentingShape()) {
                        Li_sampled = light_isect.hit_shape->GetMaterial().Emission(light_isect, -material_sample.wi);
                    }
                } else {
                    // This can happen if the light is an infinite light
                    Li_sampled = light.Le(light_ray);
                }

                // Add contribution if light is not black
                if (!Li_sampled.IsBlack()) {
                    Ld += f * Li_sampled * Dot(isect.geometry_frame.n(), material_sample.wi) * mis_weight /
                          material_sample.pdf;
                }
            }
        }
    }

    return Ld;
}

Spectrum<Real> RayIntegratorInterface::SpecularLi(const Intersection& isect, const Scene& scene,
                                                  SamplerInterface& sampler, unsigned int depth) const CXX_NOEXCEPT {
    // Get pointer to material
    const auto& material = isect.hit_shape->GetMaterial();
    // Check if material matches specular reflection
    if (material.MatchesType(M_SPECULAR)) {
        // Sample the BRDF
        MaterialSample material_sample;
        const Spectrum<Real> f = material.SampleF(isect, sampler.Request2D(), material_sample);
        // Check if we need to trace next ray, here we omit the cosine term since the material is specular
        // and there is no division in the BRDF to compensate for that
        if (material_sample.pdf > 0 &&
            !f.IsBlack() &&
            !ApproxEqual(Dot(material_sample.wi, isect.geometry_frame.n()), Real(0))) {
            return f * Li(isect.SpawnRay(material_sample.wi), scene, sampler, depth + 1) / material_sample.pdf;
        }
    }

    return Spectrum<Real>(0);
}

