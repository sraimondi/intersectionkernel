#include "path_trace_integrator.hpp"

RayPathTraceIntegrator::RayPathTraceIntegrator(unsigned int max_depth) CXX_NOEXCEPT
        : max_depth(max_depth) {}

std::unique_ptr<RayIntegratorInterface> RayPathTraceIntegrator::Clone() const {
#if CXX_VERSION >= CXX_14
    return std::make_unique<RayPathTraceIntegrator>(max_depth);
#else
    return std::unique_ptr<RayPathTraceIntegrator>(new RayPathTraceIntegrator(max_depth));
#endif
}

void RayPathTraceIntegrator::Preprocess(const Scene& scene, SamplerInterface& sampler) CXX_NOEXCEPT {
    // Allocate enough space for the light samples
    // light_num_samples stores for each light the number of samples that better fit the sampler
    // light_samples_v is a vector of dimension max_depth that holds a vector where the slices, whose size is given by
    // light_num_samples, hold the samples for each light at a certain depth

    std::size_t num_samples_for_lights = 0;
    for (const auto& light :scene.GetLights()) {
        light_num_samples.emplace_back(sampler.RoundRequest(light->GetNumSamples()));
        num_samples_for_lights += light_num_samples.back();
    }

    // Allocate space
    for (unsigned int depth = 0; depth < max_depth; ++depth) {
        light_samples_v.emplace_back(num_samples_for_lights);
        material_samples_v.emplace_back(num_samples_for_lights);
    }
}

void RayPathTraceIntegrator::NewRay(SamplerInterface& sampler) CXX_NOEXCEPT {
    // Fill the samples for each light at each depth
    for (unsigned int depth = 0; depth < max_depth; ++depth) {
        std::size_t offset = 0;
        for (std::size_t light_num_sample : light_num_samples) {
            // Fill light samples
            sampler.Fill2DArray(light_samples_v[depth].begin() + offset,
                                light_samples_v[depth].begin() + offset + light_num_sample);
            // Fill material samples
            sampler.Fill2DArray(material_samples_v[depth].begin() + offset,
                                material_samples_v[depth].begin() + offset + light_num_sample);
            offset += light_num_sample;
        }
    }
}

Spectrum<Real> RayPathTraceIntegrator::Li(const Ray& ray, const Scene& scene,
                                          SamplerInterface& sampler, unsigned int) const CXX_NOEXCEPT {
    // Initialise values for path tracing
    Spectrum<Real> L(0), beta(1);
    // Set current ray to the given one
    Ray current_ray = ray;
    bool specular_bounce = false;

    // Start looping over all allowed bounces for the path
    for (unsigned int bounce = 0; bounce < max_depth; ++bounce) {
        // Intersect ray with the scene
        Intersection isect;
        scene.Intersect(current_ray, isect);

        // Possibly add emitted light at intersection if we are at the first bounce or the bounce is specular
        if (bounce == 0 || specular_bounce) {
            if (isect.Hit()) {
                const auto& material = isect.hit_shape->GetMaterial();
                if (material.IsEmitting()) {
                    L += beta * material.Emission(isect, -current_ray.Direction());
                }
            } else {
                // Add contribution from infinite Lights
                for (const auto& light : scene.GetLights()) {
                    L += beta * light->Le(current_ray);
                }
            }
        }

        // Terminate path if ray escaped or we are at max depth
        if (!isect.Hit()) {
            break;
        }

        // Compute direct illumination
        L += beta *
             DirectLiAllLights(isect, light_num_samples, material_samples_v[bounce], light_samples_v[bounce], scene);

        // Get pointer to material and sample for new direction
        const auto& material = isect.hit_shape->GetMaterial();
        MaterialSample material_sample;
        const Spectrum<Real> f = material.SampleF(isect, sampler.Request2D(), material_sample);
        // Check if we need to stop
        if (ApproxEqual(material_sample.pdf, Real(0)) || f.IsBlack()) {
            break;
        }
        // Check if bounce is specular
        specular_bounce = material.MatchesType(M_SPECULAR);
        // Update beta
        const Real n_dot_wi = specular_bounce ? 1 : Dot(isect.geometry_frame.n(), material_sample.wi);
        beta *= f * n_dot_wi / material_sample.pdf;

        // Spawn new ray
        current_ray = isect.SpawnRay(material_sample.wi);

        // Possibly terminate the path with russian roulette
        const Real beta_avg = (beta.r() + beta.g() + beta.b()) / 3;
        if (bounce > 3) {
            Real q = std::max(Real(0.05), 1 - beta_avg);
            if (sampler.Request1D() < q) {
                break;
            }
            // Scale by probability to continue
            beta /= 1 - q;
        }
    }

    return L;
}
