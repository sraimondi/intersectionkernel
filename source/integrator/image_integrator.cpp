#include "image_integrator.hpp"

#include <thread>

// Pixel helper structure
struct Pixel {
    CXX_CONSTEXPR Pixel() CXX_NOEXCEPT
            : filter_sum(0) {}

    // Pixel sum of all samples
    Spectrum<Real> samples_sum;
    // Sum of the filter weights
    Real filter_sum;
};

ImageIntegrator::ImageIntegrator(const std::shared_ptr<const RayIntegratorInterface>& ray_i,
                                 const std::shared_ptr<SamplerInterface>& s,
                                 const std::shared_ptr<const FilterInterface>& f,
                                 std::size_t sample_per_pixel)
        : ray_integrator(ray_i), sampler(s), filter(f), samples_per_pixel(sample_per_pixel) {}

std::vector<Tile> ImageIntegrator::GenerateRenderTiles(const Image<Spectrum<Real>>& target_image) const CXX_NOEXCEPT {
    CXX_CONSTEXPR std::size_t TILE_W = 16;
    CXX_CONSTEXPR std::size_t TILE_H = 16;

    // Compute number of tiles
    const std::size_t num_tiles_width = DivideUp(target_image.GetWidth(), TILE_W);
    const std::size_t num_tiles_height = DivideUp(target_image.GetHeight(), TILE_H);

    std::vector<Tile> tiles;

    // Compute tiles start and end, sorted in column order
    for (std::size_t j = 0; j < num_tiles_width; ++j) {
        for (std::size_t i = 0; i < num_tiles_height; ++i) {
            const auto start_row = i * TILE_H;
            const auto start_col = j * TILE_W;
            tiles.emplace_back(start_row,
                               start_col,
                               std::min(start_row + TILE_H, target_image.GetHeight()),
                               std::min(start_col + TILE_W, target_image.GetWidth()));
        }
    }

    return tiles;
}

void ImageIntegrator::PrintProgressBar(const std::atomic<std::size_t>& pixels_done,
                                       std::size_t total_pixels) const CXX_NOEXCEPT {
    // Print progress bar
    CXX_CONSTEXPR std::size_t BAR_WIDTH = 60;
    Real progress = 0;
    do {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        std::cout << "Rendering progress: ";
        progress = pixels_done / static_cast<Real>(total_pixels - 1);
        for (std::size_t i = 0; i < static_cast<std::size_t>(progress * BAR_WIDTH); ++i) {
            std::cout << "=";
        }
        std::cout << " " << static_cast<std::size_t>(100 * progress) << "%\r";
        std::cout.flush();
    } while (pixels_done < total_pixels);

    std::cout << "Rendering progress: ";
    for (std::size_t i = 0; i < BAR_WIDTH; ++i) {
        std::cout << "=";
    }
    std::cout << " 100%\r";
    std::cout << std::endl;
}

void ImageIntegrator::Render(Image<Spectrum<Real>>& target_image, const Camera& camera,
                             const Scene& scene) const CXX_NOEXCEPT {
    // Generate tiles to render
    const auto tiles = GenerateRenderTiles(target_image);
    // Atomic tile counter
    std::atomic<std::size_t> tile_id(0);

    // Number of pixels done
    std::atomic<std::size_t> pixels_done(0);

    // Launched threads list
    std::vector<std::thread> threads;

    // Launch threads
    const auto num_threads = static_cast<std::size_t>(std::thread::hardware_concurrency());
    const std::size_t threads_to_launch = std::min(num_threads, tiles.size());
    std::cout << "Launching " << threads_to_launch << " thread/s\n";

    // Clone integrator and sampler such that each thread has his own
    std::vector<std::unique_ptr<SamplerInterface>> samplers(threads_to_launch);
    std::vector<std::unique_ptr<RayIntegratorInterface>> integrators(threads_to_launch);

    for (std::size_t tid = 0; tid < threads_to_launch; ++tid) {
        // Create integrator and sampler for the thread
        integrators[tid] = ray_integrator->Clone();
        samplers[tid] = sampler->Clone(tid * PCG32_DEFAULT_STATE);

        // Add thread
        threads.emplace_back(
                [&target_image, &camera, &scene, &tile_id, &tiles](
                        std::atomic<std::size_t>& pixels_done,
                        RayIntegratorInterface* ray_integrator,
                        SamplerInterface* sampler,
                        const FilterInterface* filter,
                        std::size_t samples_per_pixel) {

                    // Before rendering the tiles, give the possibility to the integrator to preprocess the scene
                    ray_integrator->Preprocess(scene, *sampler);

                    // FIXME Allocate space for the pixel samples
                    std::vector<Vector2<Real>> pixel_samples(samples_per_pixel);

                    // Local tile to render
                    std::size_t local_tile_id = tile_id++;
                    while (local_tile_id < tiles.size()) {
                        // Get boundaries of the tile to render
                        const auto& current_tile = tiles[local_tile_id];

                        // Loop over the pixels for the tile
                        for (std::size_t c = current_tile.start_pixel.s(); c < current_tile.end_pixel.s(); ++c) {
                            for (std::size_t r = current_tile.start_pixel.t(); r < current_tile.end_pixel.t(); ++r) {
                                // Generate samples for the pixel
                                sampler->Fill2DArray(pixel_samples.begin(), pixel_samples.end());

                                // Map samples to filter extent
                                for (auto& sample : pixel_samples) {
                                    sample = (sample - Vector2<Real>(0.5)) * filter->GetRadius();
                                }

                                // Store pixel sum and filter weight
                                Pixel current_pixel;

                                // Loop over all samples and compute incoming radiance
                                for (const auto& sample : pixel_samples) {
                                    // Generate ray from camera
                                    const Ray ray = camera.GenerateRay(r, c, sample);
                                    // tell integrator we are starting a new ray
                                    ray_integrator->NewRay(*sampler);
                                    // Integrate ray and add incoming radiance
                                    const Real filter_weight = filter->Evaluate(sample);
                                    current_pixel.samples_sum +=
                                            filter_weight * ray_integrator->Li(ray, scene, *sampler, 0);
                                    current_pixel.filter_sum += filter_weight;
                                }

                                // Store value in the image
                                target_image(r, c) = current_pixel.samples_sum / current_pixel.filter_sum;

                                ++pixels_done;
                            }
                        }

                        // Get next tile to process
                        local_tile_id = tile_id++;
                    }

                }, std::ref(pixels_done),
                integrators[tid].get(), samplers[tid].get(),
                filter.get(),
                sampler->RoundRequest(samples_per_pixel));
    }

    PrintProgressBar(pixels_done, target_image.GetWidth() * target_image.GetHeight());

    // Synchronize all threads
    for (auto& thread : threads) {
        thread.join();
    }
}
