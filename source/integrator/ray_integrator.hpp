#ifndef INTERSECTIONKERNEL_RAY_INTEGRATOR_HPP
#define INTERSECTIONKERNEL_RAY_INTEGRATOR_HPP

#include "scene.hpp"
#include "sampler.hpp"

// Base ray integrator interface
class RayIntegratorInterface {
public:
    virtual ~RayIntegratorInterface() = default;

    // Clone the integrator
    virtual std::unique_ptr<RayIntegratorInterface> Clone() const = 0;

    // Preprocess the scene we are going to use
    virtual void Preprocess(const Scene& scene, SamplerInterface& sampler) CXX_NOEXCEPT = 0;

    // Tell the Ray integrator that we are about to start a new Ray integration
    virtual void NewRay(SamplerInterface& sampler) CXX_NOEXCEPT = 0;

    // Compute incoming radiance
    virtual Spectrum<Real> Li(const Ray& ray,               // Ray to integrate
                              const Scene& scene,           // Scene where the ray is shoot
                              SamplerInterface& sampler,    // Sampler providing the 1D / 2D random values
                              unsigned int depth            // Current ray depth
    ) const CXX_NOEXCEPT = 0;

    // Uniformly sample all lights in the scene
    Spectrum<Real> DirectLiAllLights(const Intersection& isect,
                                     const std::vector<std::size_t>& light_num_samples,
                                     const std::vector<Vector2<Real>>& material_samples,
                                     const std::vector<Vector2<Real>>& light_samples,
                                     const Scene& scene) const CXX_NOEXCEPT;

    // Compute direct light at given intersection
    Spectrum<Real> DirectLi(const Intersection& isect, const Vector2<Real>& u_material,
                            const LightInterface& light, const Vector2<Real>& u_light,
                            const Scene& scene) const CXX_NOEXCEPT;

    // Compute incoming specular radiance
    Spectrum<Real> SpecularLi(const Intersection& isect, const Scene& scene,
                              SamplerInterface& sampler, unsigned int depth) const CXX_NOEXCEPT;
};

#endif //INTERSECTIONKERNEL_RAY_INTEGRATOR_HPP
