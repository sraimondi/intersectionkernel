#include "ray_direct_light_integrator.hpp"

RayDirectLightIntegrator::RayDirectLightIntegrator(unsigned int max_depth) CXX_NOEXCEPT
        : max_depth(max_depth) {}

std::unique_ptr<RayIntegratorInterface> RayDirectLightIntegrator::Clone() const {
#if CXX_VERSION >= CXX_14
    return std::make_unique<RayDirectLightIntegrator>(max_depth);
#else
    return std::unique_ptr<RayDirectLightIntegrator>(new RayDirectLightIntegrator(max_depth));
#endif
}

void RayDirectLightIntegrator::Preprocess(const Scene& scene, SamplerInterface& sampler) CXX_NOEXCEPT {
    // Allocate enough space for the light samples
    // light_num_samples stores for each light the number of samples that better fit the sampler
    // light_samples_v is a vector of dimension max_depth that holds a vector where the slices, whose size is given by
    // light_num_samples, hold the samples for each light at a certain depth

    std::size_t num_samples_for_lights = 0;
    for (const auto& light :scene.GetLights()) {
        light_num_samples.emplace_back(sampler.RoundRequest(light->GetNumSamples()));
        num_samples_for_lights += light_num_samples.back();
    }

    // Allocate space
    for (unsigned int depth = 0; depth < max_depth; ++depth) {
        light_samples_v.emplace_back(num_samples_for_lights);
        material_samples_v.emplace_back(num_samples_for_lights);
    }
}

void RayDirectLightIntegrator::NewRay(SamplerInterface& sampler) CXX_NOEXCEPT {
    // Fill the samples for each light at each depth
    for (unsigned int depth = 0; depth < max_depth; ++depth) {
        std::size_t offset = 0;
        for (std::size_t light_num_sample : light_num_samples) {
            // Fill light samples
            sampler.Fill2DArray(light_samples_v[depth].begin() + offset,
                                light_samples_v[depth].begin() + offset + light_num_sample);
            // Fill material samples
            sampler.Fill2DArray(material_samples_v[depth].begin() + offset,
                                material_samples_v[depth].begin() + offset + light_num_sample);
            offset += light_num_sample;
        }
    }
}

Spectrum<Real> RayDirectLightIntegrator::Li(const Ray& ray, const Scene& scene,
                                            SamplerInterface& sampler, unsigned int depth) const CXX_NOEXCEPT {
    // Output radiance
    Spectrum<Real> L(0);

    // Find closest ray hit, if any
    Intersection isect;
    scene.Intersect(ray, isect);

    // If we don't hit anything add contribution of infinite lights
    if (!isect.Hit()) {
        for (const auto& light : scene.GetLights()) {
            L += light->Le(ray);
        }
    } else {
        // If material is emitting, add contribution
        const auto& material = isect.hit_shape->GetMaterial();
        if (material.IsEmitting()) {
            L += material.Emission(isect, isect.wo);
        }

        // Add direct light contribution from all lights in the scene
        L += DirectLiAllLights(isect, light_num_samples, light_samples_v[depth], material_samples_v[depth], scene);

        // If we are not at max depth, check for light scattering
        if (depth + 1 < max_depth) {
            L += SpecularLi(isect, scene, sampler, depth);
        }
    }

    return L;
}


