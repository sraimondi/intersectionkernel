#ifndef INTERSECTIONKERNEL_RAY_WHITTED_INTEGRATOR_HPP
#define INTERSECTIONKERNEL_RAY_WHITTED_INTEGRATOR_HPP

#include "ray_integrator.hpp"

// Whitted integrator
class RayWhittedIntegrator : public RayIntegratorInterface {
public:
    CXX_CONSTEXPR explicit RayWhittedIntegrator(unsigned int max_depth) CXX_NOEXCEPT
            : max_depth(max_depth) {}

    std::unique_ptr<RayIntegratorInterface> Clone() const override;

    void Preprocess(const Scene& scene, SamplerInterface& sampler) CXX_NOEXCEPT override;

    void NewRay(SamplerInterface& sampler) CXX_NOEXCEPT override;

    Spectrum<Real> Li(const Ray& ray, const Scene& scene,
                      SamplerInterface& sampler, unsigned int depth) const CXX_NOEXCEPT override;

private:
    // Maximum recursion depth
    const unsigned int max_depth;
};


#endif //INTERSECTIONKERNEL_RAY_WHITTED_INTEGRATOR_HPP
