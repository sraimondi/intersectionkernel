#ifndef INTERSECTIONKERNEL_IMAGE_INTEGRATOR_HPP
#define INTERSECTIONKERNEL_IMAGE_INTEGRATOR_HPP

#include "camera.hpp"
#include "image.hpp"
#include "ray_integrator.hpp"
#include "filter.hpp"

#include <atomic>

// Tile helper structure
struct Tile {
    CXX_INLINE Tile(typename Vector2st::value_type start_row, typename Vector2st::value_type start_col,
                    typename Vector2st::value_type end_row, typename Vector2st::value_type end_col)
            : start_pixel(start_col, start_row), end_pixel(end_col, end_row) {}

    Vector2st start_pixel, end_pixel;
};

class ImageIntegrator {
public:
    ImageIntegrator(const std::shared_ptr<const RayIntegratorInterface>& ray_i,
                    const std::shared_ptr<SamplerInterface>& s,
                    const std::shared_ptr<const FilterInterface>& f,
                    std::size_t sample_per_pixel);

    // Render the given scene on a given image
    void Render(Image<Spectrum<Real>>& target_image, const Camera& camera, const Scene& scene) const CXX_NOEXCEPT;

private:
    // Setup tiles to render
    std::vector<Tile> GenerateRenderTiles(const Image<Spectrum<Real>>& target_image) const CXX_NOEXCEPT;

    // Print progress bar
    void PrintProgressBar(const std::atomic<std::size_t>& pixels_done, std::size_t total_pixels) const CXX_NOEXCEPT;

    // Ray integrator used
    const std::shared_ptr<const RayIntegratorInterface> ray_integrator;
    // Sampler
    const std::shared_ptr<SamplerInterface> sampler;
    // Filter used in reconstruction
    const std::shared_ptr<const FilterInterface> filter;
    // Samples per pixel
    const std::size_t samples_per_pixel;
};

#endif //INTERSECTIONKERNEL_IMAGE_INTEGRATOR_HPP
