#ifndef INTERSECTIONKERNEL_PATH_TRACE_INTEGRATOR_HPP
#define INTERSECTIONKERNEL_PATH_TRACE_INTEGRATOR_HPP

#include "ray_integrator.hpp"

// Path tracing integrator
class RayPathTraceIntegrator : public RayIntegratorInterface {
public:
    explicit RayPathTraceIntegrator(unsigned int max_depth) CXX_NOEXCEPT;

    std::unique_ptr<RayIntegratorInterface> Clone() const override;

    void Preprocess(const Scene& scene, SamplerInterface& sampler) CXX_NOEXCEPT override;

    void NewRay(SamplerInterface& sampler) CXX_NOEXCEPT override;

    Spectrum<Real> Li(const Ray& ray, const Scene& scene,
                      SamplerInterface& sampler, unsigned int depth) const CXX_NOEXCEPT override;

private:
    // Maximum depth to trace, set to high value to use Russian roulette
    const unsigned int max_depth;
    // Vectors to store the samples final number of samples for each light and the samples themselves
    std::vector<std::size_t> light_num_samples;
    std::vector<std::vector<Vector2<Real>>> light_samples_v;
    // The same number of samples as for the light is also generated for the material
    std::vector<std::vector<Vector2<Real>>> material_samples_v;
};

#endif //INTERSECTIONKERNEL_PATH_TRACE_INTEGRATOR_HPP
