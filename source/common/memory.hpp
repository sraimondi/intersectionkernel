#ifndef INTERSECTION_KERNEL_MEMORY_HPP
#define INTERSECTION_KERNEL_MEMORY_HPP

#include "common.hpp"

// Define utility function to allocate aligned memory
template <typename T, std::size_t Alignment = CACHE_LINE_SIZE>
T* AllocateAligned(std::size_t num_elements) {
    const std::size_t size = DivideUp(num_elements * sizeof(T), Alignment) * Alignment;
    T* ptr = nullptr;
    if (posix_memalign(reinterpret_cast<void**>(&ptr), Alignment, size) != 0 || !ptr) {
        return nullptr;
    }
    return ptr;
}

// Define utility function to free aligned memory
template <typename T>
void FreeAligned(T* mem) {
    std::free(mem);
}

#endif // INTERSECTION_KERNEL_MEMORY_HPP