#ifndef INTERSECTION_KERNEL_COMMON_HPP
#define INTERSECTION_KERNEL_COMMON_HPP

#include "config.hpp"

#include <limits>
#include <cmath>
#include <type_traits>

// Type to use in the program for floating point
using Real = float;

// Constants
CXX_CONSTEXPR Real INF = std::numeric_limits<Real>::max();
CXX_CONSTEXPR Real EPS = Real(0.001);
CXX_CONSTEXPR Real PI = Real(3.141592653);
CXX_CONSTEXPR Real TWO_PI = 2 * PI;
CXX_CONSTEXPR Real INV_PI = 1 / PI;
CXX_CONSTEXPR Real INV_2PI = 1 / (2 * PI);
CXX_CONSTEXPR Real INV_4PI = 1 / (4 * PI);
CXX_CONSTEXPR Real PI_OVER_2 = PI / 2;
CXX_CONSTEXPR Real PI_OVER_4 = PI / 4;

// Divide two integer values to the closest upper value
template <typename T>
CXX_CONSTEXPR
#if CXX_VERSION >= CXX_17
std::enable_if_t<std::is_integral_v<T>, T>
#elif CXX_VERSION >= CXX_14
std::enable_if_t<std::is_integral<T>::value, T>
#else
typename std::enable_if<std::is_integral<T>::value, T>::type
#endif
DivideUp(const T& a, const T& b) CXX_NOEXCEPT {
    return (a + b - 1) / b;
}

// Divide two integer values to the closes lower value
template <typename T>
CXX_CONSTEXPR
#if CXX_VERSION >= CXX_17
std::enable_if_t<std::is_integral_v<T>, T>
#elif CXX_VERSION >= CXX_14
std::enable_if_t<std::is_integral<T>::value, T>
#else
typename std::enable_if<std::is_integral<T>::value, T>::type
#endif
DivideDown(const T& a, const T& b) CXX_NOEXCEPT {
    return a / b;
}

// Clamp value
template <typename T>
CXX_CONSTEXPR const T& Clamp(const T& val, const T& min, const T& max) CXX_NOEXCEPT {
    return val < min ? min : (val > max ? max : val);
}

// Lerp
template <typename T, typename Q>
CXX_CONSTEXPR Q Lerp(const T& t, const Q& a, const Q& b) CXX_NOEXCEPT {
    return (T(1) - t) * a + t * b;
}

// Convert degrees to radians and viceversa
template <typename T>
CXX_CONSTEXPR
#if CXX_VERSION >= CXX_17
std::enable_if_t<std::is_floating_point_v<T>, T>
#elif CXX_VERSION >= CXX_14
std::enable_if_t<std::is_floating_point<T>::value, T>
#else
typename std::enable_if<std::is_floating_point<T>::value, T>::type
#endif
DegToRad(const T& deg) CXX_NOEXCEPT {
    return (PI / 180) * deg;
}

template <typename T>
CXX_CONSTEXPR
#if CXX_VERSION >= CXX_17
std::enable_if_t<std::is_floating_point_v<T>, T>
#elif CXX_VERSION >= CXX_14
std::enable_if_t<std::is_floating_point<T>::value, T>
#else
typename std::enable_if<std::is_floating_point<T>::value, T>::type
#endif
RadToDeg(const T& rad) CXX_NOEXCEPT {
    return (180 * INV_PI) * rad;
}

// Check for equality of two floating prevision value
template <typename T>
CXX14_CONSTEXPR
#if CXX_VERSION >= CXX_17
std::enable_if_t<std::is_floating_point_v<T>, bool>
#elif CXX_VERSION >= CXX_14
std::enable_if_t<std::is_floating_point<T>::value, bool>
#else
typename std::enable_if<std::is_floating_point<T>::value, bool>::type
#endif
ApproxEqual(const T& a, const T& b, T tollerance = 32 * std::numeric_limits<T>::epsilon()) {
    return std::abs(a - b) < tollerance;
}

// Solve quadratic system, the type used in the solving process is double by default
// The two solutions are always return in increasing order
template <typename T, typename SolveT = double>
CXX_INLINE bool SolveQuadratic(T a, T b, T c, T& x0, T& x1) CXX_NOEXCEPT {
    // Compute discriminant and check if it's negative
    const SolveT discr = SolveT(b) * SolveT(b) - 4 * SolveT(a) * SolveT(c);
    if (discr < 0) {
        return false;
    }
    const SolveT root = std::sqrt(discr);
    // Compute quadratic values
    SolveT q;
    if (b < 0) {
        q = -0.5 * (b - root);
    } else {
        q = -0.5 * (b + root);
    }
    x0 = q / a;
    x1 = c / q;
    if (x0 > x1) {
        std::swap(x0, x1);
    }

    return true;
}

#endif //INTERSECTION_KERNEL_COMMON_HPP
