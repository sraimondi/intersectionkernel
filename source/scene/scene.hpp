#ifndef INTERSECTIONKERNEL_SCENE_HPP
#define INTERSECTIONKERNEL_SCENE_HPP

#include "bvh.hpp"
#include "light.hpp"

// Simple storage class that hold the scene data
class Scene {
public:
    // Create new scene
    explicit Scene(const BVH& bvh);

    // Intersect ray with scene
    CXX_INLINE void Intersect(const Ray& ray, Intersection& isect) const CXX_NOEXCEPT {
        bvh.Intersect(ray, isect);
    }

    // Check for ray intersection
    CXX_INLINE bool IntersectP(const Ray& ray, Real max_t = INF) const CXX_NOEXCEPT {
        return bvh.IntersectP(ray, max_t);
    }

    // Get list of light
    CXX_CONSTEXPR const std::vector<std::unique_ptr<const LightInterface>>& GetLights() const CXX_NOEXCEPT {
        return lights;
    }

    // Add light
    CXX_INLINE void AddLight(std::unique_ptr<const LightInterface> light) {
        lights.push_back(std::move(light));
    }

private:
    // Acceleration structure
    const BVH& bvh;
    // Lights in the scene
    std::vector<std::unique_ptr<const LightInterface>> lights;
};

#endif //INTERSECTIONKERNEL_SCENE_HPP
