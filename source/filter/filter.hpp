#ifndef INTERSECTIONKERNEL_FILTER_HPP
#define INTERSECTIONKERNEL_FILTER_HPP

#include "lin_algebra.hpp"

// Base filter interface
class FilterInterface {
public:
    CXX_CONSTEXPR explicit FilterInterface(const Vector2<Real>& r) CXX_NOEXCEPT
            : radius(r), inv_radius(1 / radius.x(), 1 / radius.y()) {}

    // Access filter values
    CXX17_CONSTEXPR const Vector2<Real>& GetRadius() const CXX_NOEXCEPT {
        return radius;
    }

    CXX17_CONSTEXPR const Vector2<Real>& GetInvRadius() const CXX_NOEXCEPT {
        return inv_radius;
    }

    // Check if a point is in the Filter extent
    CXX_CONSTEXPR bool Inside(const Vector2<Real>& p) const CXX_NOEXCEPT {
        return -radius < p && p < radius;
    }

    // Evaluate filter at a given position
    virtual Real Evaluate(const Vector2<Real>& p) const CXX_NOEXCEPT = 0;

protected:
    // Extent of the filter in the plane space
    const Vector2<Real> radius, inv_radius;
};

#endif //INTERSECTIONKERNEL_FILTER_HPP
