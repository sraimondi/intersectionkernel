#ifndef INTERSECTIONKERNEL_MITCHELL_FILTER_HPP
#define INTERSECTIONKERNEL_MITCHELL_FILTER_HPP

#include "filter.hpp"

// Mitchell-Netravali filter class
class MitchellFilter : public FilterInterface {
public:
    // B and C values should be on the line B + 2 * C = 1
    CXX_CONSTEXPR explicit MitchellFilter(const Vector2<Real>& r,
                                          Real B = 1 / 3.f, Real C = 1 / 3.f) CXX_NOEXCEPT
            : FilterInterface(r), B(B), C(C) {}

    Real Evaluate(const Vector2<Real>& p) const CXX_NOEXCEPT override;

private:
    // Evaluate 1D Mitchell function
    Real Mitchell(Real x) const CXX_NOEXCEPT;

    // Stored filter values
    const Real B, C;
};

#endif //INTERSECTIONKERNEL_MITCHELL_FILTER_HPP
