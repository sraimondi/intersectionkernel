#ifndef INTERSECTIONKERNEL_TRIANGLE_FILTER_HPP
#define INTERSECTIONKERNEL_TRIANGLE_FILTER_HPP

#include "filter.hpp"

class TriangleFilter : public FilterInterface {
public:
    CXX_CONSTEXPR explicit TriangleFilter(const Vector2<Real>& r) CXX_NOEXCEPT
            : FilterInterface(r) {}

    Real Evaluate(const Vector2<Real>& p) const CXX_NOEXCEPT override;
};

#endif //INTERSECTIONKERNEL_TRIANGLE_FILTER_HPP
