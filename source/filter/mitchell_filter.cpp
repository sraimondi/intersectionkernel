#include "mitchell_filter.hpp"

Real MitchellFilter::Evaluate(const Vector2<Real>& p) const CXX_NOEXCEPT {
    if (Inside(p)) {
        return Mitchell(p.x() * inv_radius.x()) * Mitchell(p.y() * inv_radius.y());
    }

    return 0;
}

Real MitchellFilter::Mitchell(Real x) const CXX_NOEXCEPT {
    // Map [0, 1) value to Mitchell function domain [0, 2]
    x = std::abs(2 * x);
    const Real x2 = x * x;
    Real w = 0;
    if (x >= 1) {
        w = (-B - 6 * C) * x * x2 + (6 * B + 30 * C) * x2 + (-12 * B - 48 * C) * x + (8 * B + 24 * C);
    } else {
        w = (12 - 9 * B - 6 * C) * x * x2 + (-18 + 12 * B + 6 * C) * x2 + (6 - 2 * B);
    }

    return (1 / Real(6)) * w;
}
