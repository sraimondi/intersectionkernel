#ifndef INTERSECTIONKERNEL_BOX_FILTER_HPP
#define INTERSECTIONKERNEL_BOX_FILTER_HPP

#include "filter.hpp"

// Box filter class
class BoxFilter : public FilterInterface {
public:
    CXX_CONSTEXPR explicit BoxFilter(const Vector2<Real>& r = Vector2<Real>(0.5)) CXX_NOEXCEPT
            : FilterInterface(r) {}

    Real Evaluate(const Vector2<Real>& p) const CXX_NOEXCEPT override;
};

#endif //INTERSECTIONKERNEL_BOX_FILTER_HPP
