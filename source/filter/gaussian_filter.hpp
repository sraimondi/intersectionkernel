#ifndef INTERSECTIONKERNEL_GAUSSIAN_FILTER_HPP
#define INTERSECTIONKERNEL_GAUSSIAN_FILTER_HPP

#include "filter.hpp"

// Gaussian filter class
class GaussianFilter : public FilterInterface {
public:
    GaussianFilter(const Vector2<Real>& r, Real alpha) CXX_NOEXCEPT;

    Real Evaluate(const Vector2<Real>& p) const CXX_NOEXCEPT override;

private:
    // Evaluate Gaussian function
    Real Gaussian(Real d, Real exp_v) const CXX_NOEXCEPT;

    // Alpha value for the gaussian curve
    const Real alpha;
    // Precompute exponential values for evaluating the exponential function
    const Real exp_x, exp_y;
};

#endif //INTERSECTIONKERNEL_GAUSSIAN_FILTER_HPP
