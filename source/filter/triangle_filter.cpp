#include "triangle_filter.hpp"

Real TriangleFilter::Evaluate(const Vector2<Real>& p) const CXX_NOEXCEPT {
    if (Inside(p)) {
        return std::max(Real(0), radius.x() - std::abs(p.x())) *
               std::max(Real(0), radius.y() - std::abs(p.y()));
    }

    return 0;
}
