#include "box_filter.hpp"

Real BoxFilter::Evaluate(const Vector2<Real>& p) const CXX_NOEXCEPT {
    // Box filter returns one if the point is within the filter extent and 0 otherwise
    if (Inside(p)) {
        return 1;
    }

    return 0;
}
