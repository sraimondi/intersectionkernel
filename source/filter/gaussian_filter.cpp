#include "gaussian_filter.hpp"

GaussianFilter::GaussianFilter(const Vector2<Real>& r, Real alpha) CXX_NOEXCEPT
        : FilterInterface(r), alpha(alpha),
          exp_x(std::exp(-alpha * GetRadius().x() * GetRadius().x())),
          exp_y(std::exp(-alpha * GetRadius().y() * GetRadius().y())) {}

Real GaussianFilter::Evaluate(const Vector2<Real>& p) const CXX_NOEXCEPT {
    if (Inside(p)) {
        return Gaussian(p.x(), exp_x) * Gaussian(p.y(), exp_y);
    }

    return 0;
}

Real GaussianFilter::Gaussian(Real d, Real exp_v) const CXX_NOEXCEPT {
    return std::max(Real(0), std::exp(-alpha * d * d) - exp_v);
}
